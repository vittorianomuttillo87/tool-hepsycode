package hepsycode.design;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.sirius.common.ui.tools.api.util.EclipseUIUtil;
import org.eclipse.sirius.diagram.AbstractDNode;
import org.eclipse.sirius.diagram.business.api.refresh.DiagramCreationUtil;
import org.eclipse.sirius.diagram.ui.business.api.view.SiriusLayoutDataManager;
import org.eclipse.sirius.diagram.ui.business.internal.view.RootLayoutData;
import org.eclipse.sirius.diagram.ui.tools.api.util.GMFNotationHelper;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;


import hepsycode.impl.ProcessImpl;


@SuppressWarnings("restriction")
public class Services {
	
	public static final List<String> DATA_TYPES = Arrays.asList("Integer", "String", "Boolean", "BigDecimal", "Double", "Float", "Long", "Short");
    
	public EcoreEMap<String, String> addElToMap(EObject self, String id, String type, EcoreEMap<String, String> map) 
	{
      map.put(id, type);
      return map;
    }
    
	@SuppressWarnings({ "unchecked"})
    public List<String> mapAsList(EObject self, EcoreEMap<String, String> map) {
    	List<String> list = new ArrayList<String>();
    	if((map != null) && !map.isEmpty()) 
    	{
    		for(Object el : map.entrySet()) 
    		{
    			Map.Entry<String, String> entry = (Map.Entry<String, String>)el;
    			list.add(entry.getValue() + ": " + entry.getKey());
    		}
    	}
        return list;
    }
    
	@SuppressWarnings({"unlikely-arg-type" })
	public EcoreEMap<String, String> deleteFromMap(EObject self, ArrayList<String> selected, EcoreEMap<String, String> map) 
    {
      if((map!=null) && !map.isEmpty()) 
      {
    	  if((selected != null) && !selected.isEmpty()) 
    	  {
    		  for(String sel : selected) {
    			  String[] array = sel.split(": ");
    			  map.remove(array[1]);
    		  }
    	  }
      }
      return map;
    }
    
    public EList<String> getType(EObject aClass) {
    	EList<String> listT = new BasicEList<String>();
    	listT.addAll(DATA_TYPES);
		return listT;
	}
	

    public EObject setLocation(EObject obj, AbstractDNode packageView) 
    {
    	// recupera la posizione del puntatore
    	 IEditorPart editor = EclipseUIUtil.getActiveEditor();
         if (editor instanceof DiagramEditor) {
             DiagramEditor diagramEditor = (DiagramEditor) editor;
             DiagramEditPart diagramEditPart = diagramEditor.getDiagramEditPart();
             EditPartViewer viewer = diagramEditPart.getViewer();

             org.eclipse.swt.graphics.Point cursorLocation = Display.getCurrent().getCursorLocation();
             final FigureCanvas control = (FigureCanvas) viewer.getControl();
             final org.eclipse.swt.graphics.Point screenSWTPoint = control.toControl(cursorLocation);

             final Point screenPoint = new Point((screenSWTPoint.x - 50), (screenSWTPoint.y - 50));
             
             // imposta la posizione della vista
             SiriusLayoutDataManager.INSTANCE.addData(new RootLayoutData(packageView, screenPoint, null));
             
             
         }
        return obj;
    }
    
    public EObject setLocationWithPosition(EObject obj, AbstractDNode packageView, int x, int y) 
    {
    	final Point position = new Point(x, y);
         
    	// imposta la posizione della vista
    	SiriusLayoutDataManager.INSTANCE.addData(new RootLayoutData(packageView, position, null));
            
    	return obj;
    }
	
	
	public int getWidth(EObject obj, AbstractDNode nodeView) 
    {
		DiagramCreationUtil util = new DiagramCreationUtil(nodeView.getParentDiagram());
	    if ( util.findAssociatedGMFDiagram()) 
	    {
	        Diagram diag = util.getAssociatedGMFDiagram();
	        Node node = GMFNotationHelper.findGMFNode(diag, nodeView);
	        return GMFNotationHelper.getWidth(node);
	    }
	    return 1;
    }
	
	public int getHeight(EObject obj, AbstractDNode nodeView) 
    {
		DiagramCreationUtil util = new DiagramCreationUtil(nodeView.getParentDiagram());
	    if ( util.findAssociatedGMFDiagram()) 
	    {
	        Diagram diag = util.getAssociatedGMFDiagram();
	        Node node = GMFNotationHelper.findGMFNode(diag, nodeView);
	        return GMFNotationHelper.getHeight(node);
	    }
	    return 1;
    }
	
	public AbstractDNode setX(AbstractDNode nodeView) 
    {
		DiagramCreationUtil util = new DiagramCreationUtil(nodeView.getParentDiagram());
	    if ( util.findAssociatedGMFDiagram()) 
	    {
	        Diagram diag = util.getAssociatedGMFDiagram();
	        Node node = GMFNotationHelper.findGMFNode(diag, nodeView);
	        int x = GMFNotationHelper.getX(node);
	        ProcessImpl proc = (ProcessImpl)nodeView.getTarget();
	        proc.setX(x);
	    }
	    return nodeView;
    }
	
	public AbstractDNode setY(AbstractDNode nodeView) 
    {
		DiagramCreationUtil util = new DiagramCreationUtil(nodeView.getParentDiagram());
	    if ( util.findAssociatedGMFDiagram()) 
	    {
	        Diagram diag = util.getAssociatedGMFDiagram();
	        Node node = GMFNotationHelper.findGMFNode(diag, nodeView);
	        int y = GMFNotationHelper.getY(node);
	        ProcessImpl proc = (ProcessImpl)nodeView.getTarget();
	        proc.setY(y);
	    }
	    return nodeView;
    }
	
}
