/**
 */
package hepsycode;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Package</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link hepsycode.Package#getProcesses <em>Processes</em>}</li>
 *   <li>{@link hepsycode.Package#isZipped <em>Zipped</em>}</li>
 *   <li>{@link hepsycode.Package#getWidth <em>Width</em>}</li>
 *   <li>{@link hepsycode.Package#getHeight <em>Height</em>}</li>
 * </ul>
 *
 * @see hepsycode.HepsycodePackage#getPackage()
 * @model
 * @generated
 */
public interface Package extends Node {
	/**
	 * Returns the value of the '<em><b>Processes</b></em>' containment reference list.
	 * The list contents are of type {@link hepsycode.Process}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processes</em>' containment reference list.
	 * @see hepsycode.HepsycodePackage#getPackage_Processes()
	 * @model containment="true"
	 * @generated
	 */
	EList<hepsycode.Process> getProcesses();

	/**
	 * Returns the value of the '<em><b>Zipped</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Zipped</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Zipped</em>' attribute.
	 * @see #setZipped(boolean)
	 * @see hepsycode.HepsycodePackage#getPackage_Zipped()
	 * @model default="false" unique="false" required="true"
	 * @generated
	 */
	boolean isZipped();

	/**
	 * Sets the value of the '{@link hepsycode.Package#isZipped <em>Zipped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Zipped</em>' attribute.
	 * @see #isZipped()
	 * @generated
	 */
	void setZipped(boolean value);

	/**
	 * Returns the value of the '<em><b>Width</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width</em>' attribute.
	 * @see #setWidth(int)
	 * @see hepsycode.HepsycodePackage#getPackage_Width()
	 * @model default="1" unique="false" dataType="org.eclipse.emf.ecore.xml.type.Int" required="true" ordered="false"
	 * @generated
	 */
	int getWidth();

	/**
	 * Sets the value of the '{@link hepsycode.Package#getWidth <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Width</em>' attribute.
	 * @see #getWidth()
	 * @generated
	 */
	void setWidth(int value);

	/**
	 * Returns the value of the '<em><b>Height</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Height</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Height</em>' attribute.
	 * @see #setHeight(int)
	 * @see hepsycode.HepsycodePackage#getPackage_Height()
	 * @model default="1" unique="false" dataType="org.eclipse.emf.ecore.xml.type.Int" required="true" ordered="false"
	 * @generated
	 */
	int getHeight();

	/**
	 * Sets the value of the '{@link hepsycode.Package#getHeight <em>Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Height</em>' attribute.
	 * @see #getHeight()
	 * @generated
	 */
	void setHeight(int value);

} // Package
