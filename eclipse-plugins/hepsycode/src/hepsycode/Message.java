/**
 */
package hepsycode;

import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link hepsycode.Message#getDataTypes <em>Data Types</em>}</li>
 * </ul>
 *
 * @see hepsycode.HepsycodePackage#getMessage()
 * @model
 * @generated
 */
public interface Message extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Data Types</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Types</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Types</em>' map.
	 * @see hepsycode.HepsycodePackage#getMessage_DataTypes()
	 * @model mapType="hepsycode.StringToStringMap&lt;org.eclipse.emf.ecore.xml.type.String, org.eclipse.emf.ecore.xml.type.String&gt;"
	 * @generated
	 */
	EMap<String, String> getDataTypes();

} // Message
