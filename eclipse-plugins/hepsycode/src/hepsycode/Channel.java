/**
 */
package hepsycode;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Channel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link hepsycode.Channel#getMsg <em>Msg</em>}</li>
 *   <li>{@link hepsycode.Channel#getStart_n <em>Start n</em>}</li>
 *   <li>{@link hepsycode.Channel#getEnd_n <em>End n</em>}</li>
 *   <li>{@link hepsycode.Channel#getApp_end_n <em>App end n</em>}</li>
 *   <li>{@link hepsycode.Channel#getApp_start_n <em>App start n</em>}</li>
 *   <li>{@link hepsycode.Channel#getWidth <em>Width</em>}</li>
 * </ul>
 *
 * @see hepsycode.HepsycodePackage#getChannel()
 * @model
 * @generated
 */
public interface Channel extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Msg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Msg</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Msg</em>' containment reference.
	 * @see #setMsg(Message)
	 * @see hepsycode.HepsycodePackage#getChannel_Msg()
	 * @model containment="true"
	 * @generated
	 */
	Message getMsg();

	/**
	 * Sets the value of the '{@link hepsycode.Channel#getMsg <em>Msg</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Msg</em>' containment reference.
	 * @see #getMsg()
	 * @generated
	 */
	void setMsg(Message value);

	/**
	 * Returns the value of the '<em><b>Start n</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start n</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start n</em>' reference.
	 * @see #setStart_n(Node)
	 * @see hepsycode.HepsycodePackage#getChannel_Start_n()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Node getStart_n();

	/**
	 * Sets the value of the '{@link hepsycode.Channel#getStart_n <em>Start n</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start n</em>' reference.
	 * @see #getStart_n()
	 * @generated
	 */
	void setStart_n(Node value);

	/**
	 * Returns the value of the '<em><b>End n</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End n</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End n</em>' reference.
	 * @see #setEnd_n(Node)
	 * @see hepsycode.HepsycodePackage#getChannel_End_n()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Node getEnd_n();

	/**
	 * Sets the value of the '{@link hepsycode.Channel#getEnd_n <em>End n</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End n</em>' reference.
	 * @see #getEnd_n()
	 * @generated
	 */
	void setEnd_n(Node value);

	/**
	 * Returns the value of the '<em><b>App end n</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>App end n</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>App end n</em>' reference.
	 * @see #setApp_end_n(Node)
	 * @see hepsycode.HepsycodePackage#getChannel_App_end_n()
	 * @model ordered="false"
	 * @generated
	 */
	Node getApp_end_n();

	/**
	 * Sets the value of the '{@link hepsycode.Channel#getApp_end_n <em>App end n</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>App end n</em>' reference.
	 * @see #getApp_end_n()
	 * @generated
	 */
	void setApp_end_n(Node value);

	/**
	 * Returns the value of the '<em><b>App start n</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>App start n</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>App start n</em>' reference.
	 * @see #setApp_start_n(Node)
	 * @see hepsycode.HepsycodePackage#getChannel_App_start_n()
	 * @model ordered="false"
	 * @generated
	 */
	Node getApp_start_n();

	/**
	 * Sets the value of the '{@link hepsycode.Channel#getApp_start_n <em>App start n</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>App start n</em>' reference.
	 * @see #getApp_start_n()
	 * @generated
	 */
	void setApp_start_n(Node value);

	/**
	 * Returns the value of the '<em><b>Width</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width</em>' attribute.
	 * @see #setWidth(int)
	 * @see hepsycode.HepsycodePackage#getChannel_Width()
	 * @model default="0" unique="false"
	 * @generated
	 */
	int getWidth();

	/**
	 * Sets the value of the '{@link hepsycode.Channel#getWidth <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Width</em>' attribute.
	 * @see #getWidth()
	 * @generated
	 */
	void setWidth(int value);

} // Channel
