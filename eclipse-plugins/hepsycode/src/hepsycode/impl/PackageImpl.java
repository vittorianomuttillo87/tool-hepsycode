/**
 */
package hepsycode.impl;

import hepsycode.HepsycodePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Package</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link hepsycode.impl.PackageImpl#getProcesses <em>Processes</em>}</li>
 *   <li>{@link hepsycode.impl.PackageImpl#isZipped <em>Zipped</em>}</li>
 *   <li>{@link hepsycode.impl.PackageImpl#getWidth <em>Width</em>}</li>
 *   <li>{@link hepsycode.impl.PackageImpl#getHeight <em>Height</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PackageImpl extends NodeImpl implements hepsycode.Package {
	/**
	 * The cached value of the '{@link #getProcesses() <em>Processes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcesses()
	 * @generated
	 * @ordered
	 */
	protected EList<hepsycode.Process> processes;

	/**
	 * The default value of the '{@link #isZipped() <em>Zipped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isZipped()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ZIPPED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isZipped() <em>Zipped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isZipped()
	 * @generated
	 * @ordered
	 */
	protected boolean zipped = ZIPPED_EDEFAULT;

	/**
	 * The default value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int WIDTH_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected int width = WIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getHeight() <em>Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeight()
	 * @generated
	 * @ordered
	 */
	protected static final int HEIGHT_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getHeight() <em>Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeight()
	 * @generated
	 * @ordered
	 */
	protected int height = HEIGHT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PackageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HepsycodePackage.Literals.PACKAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<hepsycode.Process> getProcesses() {
		if (processes == null) {
			processes = new EObjectContainmentEList<hepsycode.Process>(hepsycode.Process.class, this, HepsycodePackage.PACKAGE__PROCESSES);
		}
		return processes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isZipped() {
		return zipped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setZipped(boolean newZipped) {
		boolean oldZipped = zipped;
		zipped = newZipped;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HepsycodePackage.PACKAGE__ZIPPED, oldZipped, zipped));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWidth(int newWidth) {
		int oldWidth = width;
		width = newWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HepsycodePackage.PACKAGE__WIDTH, oldWidth, width));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeight(int newHeight) {
		int oldHeight = height;
		height = newHeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HepsycodePackage.PACKAGE__HEIGHT, oldHeight, height));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HepsycodePackage.PACKAGE__PROCESSES:
				return ((InternalEList<?>)getProcesses()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HepsycodePackage.PACKAGE__PROCESSES:
				return getProcesses();
			case HepsycodePackage.PACKAGE__ZIPPED:
				return isZipped();
			case HepsycodePackage.PACKAGE__WIDTH:
				return getWidth();
			case HepsycodePackage.PACKAGE__HEIGHT:
				return getHeight();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HepsycodePackage.PACKAGE__PROCESSES:
				getProcesses().clear();
				getProcesses().addAll((Collection<? extends hepsycode.Process>)newValue);
				return;
			case HepsycodePackage.PACKAGE__ZIPPED:
				setZipped((Boolean)newValue);
				return;
			case HepsycodePackage.PACKAGE__WIDTH:
				setWidth((Integer)newValue);
				return;
			case HepsycodePackage.PACKAGE__HEIGHT:
				setHeight((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HepsycodePackage.PACKAGE__PROCESSES:
				getProcesses().clear();
				return;
			case HepsycodePackage.PACKAGE__ZIPPED:
				setZipped(ZIPPED_EDEFAULT);
				return;
			case HepsycodePackage.PACKAGE__WIDTH:
				setWidth(WIDTH_EDEFAULT);
				return;
			case HepsycodePackage.PACKAGE__HEIGHT:
				setHeight(HEIGHT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HepsycodePackage.PACKAGE__PROCESSES:
				return processes != null && !processes.isEmpty();
			case HepsycodePackage.PACKAGE__ZIPPED:
				return zipped != ZIPPED_EDEFAULT;
			case HepsycodePackage.PACKAGE__WIDTH:
				return width != WIDTH_EDEFAULT;
			case HepsycodePackage.PACKAGE__HEIGHT:
				return height != HEIGHT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (zipped: ");
		result.append(zipped);
		result.append(", width: ");
		result.append(width);
		result.append(", height: ");
		result.append(height);
		result.append(')');
		return result.toString();
	}

} //PackageImpl
