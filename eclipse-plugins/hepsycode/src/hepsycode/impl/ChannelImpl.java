/**
 */
package hepsycode.impl;

import hepsycode.Channel;
import hepsycode.HepsycodePackage;
import hepsycode.Message;
import hepsycode.Node;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Channel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link hepsycode.impl.ChannelImpl#getMsg <em>Msg</em>}</li>
 *   <li>{@link hepsycode.impl.ChannelImpl#getStart_n <em>Start n</em>}</li>
 *   <li>{@link hepsycode.impl.ChannelImpl#getEnd_n <em>End n</em>}</li>
 *   <li>{@link hepsycode.impl.ChannelImpl#getApp_end_n <em>App end n</em>}</li>
 *   <li>{@link hepsycode.impl.ChannelImpl#getApp_start_n <em>App start n</em>}</li>
 *   <li>{@link hepsycode.impl.ChannelImpl#getWidth <em>Width</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ChannelImpl extends NamedElementImpl implements Channel {
	/**
	 * The cached value of the '{@link #getMsg() <em>Msg</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMsg()
	 * @generated
	 * @ordered
	 */
	protected Message msg;

	/**
	 * The cached value of the '{@link #getStart_n() <em>Start n</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStart_n()
	 * @generated
	 * @ordered
	 */
	protected Node start_n;

	/**
	 * The cached value of the '{@link #getEnd_n() <em>End n</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd_n()
	 * @generated
	 * @ordered
	 */
	protected Node end_n;

	/**
	 * The cached value of the '{@link #getApp_end_n() <em>App end n</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApp_end_n()
	 * @generated
	 * @ordered
	 */
	protected Node app_end_n;

	/**
	 * The cached value of the '{@link #getApp_start_n() <em>App start n</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApp_start_n()
	 * @generated
	 * @ordered
	 */
	protected Node app_start_n;

	/**
	 * The default value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int WIDTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected int width = WIDTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChannelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HepsycodePackage.Literals.CHANNEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getMsg() {
		return msg;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMsg(Message newMsg, NotificationChain msgs) {
		Message oldMsg = msg;
		msg = newMsg;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, HepsycodePackage.CHANNEL__MSG, oldMsg, newMsg);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMsg(Message newMsg) {
		if (newMsg != msg) {
			NotificationChain msgs = null;
			if (msg != null)
				msgs = ((InternalEObject)msg).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - HepsycodePackage.CHANNEL__MSG, null, msgs);
			if (newMsg != null)
				msgs = ((InternalEObject)newMsg).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - HepsycodePackage.CHANNEL__MSG, null, msgs);
			msgs = basicSetMsg(newMsg, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HepsycodePackage.CHANNEL__MSG, newMsg, newMsg));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getStart_n() {
		if (start_n != null && start_n.eIsProxy()) {
			InternalEObject oldStart_n = (InternalEObject)start_n;
			start_n = (Node)eResolveProxy(oldStart_n);
			if (start_n != oldStart_n) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, HepsycodePackage.CHANNEL__START_N, oldStart_n, start_n));
			}
		}
		return start_n;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node basicGetStart_n() {
		return start_n;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStart_n(Node newStart_n) {
		Node oldStart_n = start_n;
		start_n = newStart_n;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HepsycodePackage.CHANNEL__START_N, oldStart_n, start_n));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getEnd_n() {
		if (end_n != null && end_n.eIsProxy()) {
			InternalEObject oldEnd_n = (InternalEObject)end_n;
			end_n = (Node)eResolveProxy(oldEnd_n);
			if (end_n != oldEnd_n) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, HepsycodePackage.CHANNEL__END_N, oldEnd_n, end_n));
			}
		}
		return end_n;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node basicGetEnd_n() {
		return end_n;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnd_n(Node newEnd_n) {
		Node oldEnd_n = end_n;
		end_n = newEnd_n;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HepsycodePackage.CHANNEL__END_N, oldEnd_n, end_n));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getApp_end_n() {
		if (app_end_n != null && app_end_n.eIsProxy()) {
			InternalEObject oldApp_end_n = (InternalEObject)app_end_n;
			app_end_n = (Node)eResolveProxy(oldApp_end_n);
			if (app_end_n != oldApp_end_n) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, HepsycodePackage.CHANNEL__APP_END_N, oldApp_end_n, app_end_n));
			}
		}
		return app_end_n;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node basicGetApp_end_n() {
		return app_end_n;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApp_end_n(Node newApp_end_n) {
		Node oldApp_end_n = app_end_n;
		app_end_n = newApp_end_n;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HepsycodePackage.CHANNEL__APP_END_N, oldApp_end_n, app_end_n));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getApp_start_n() {
		if (app_start_n != null && app_start_n.eIsProxy()) {
			InternalEObject oldApp_start_n = (InternalEObject)app_start_n;
			app_start_n = (Node)eResolveProxy(oldApp_start_n);
			if (app_start_n != oldApp_start_n) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, HepsycodePackage.CHANNEL__APP_START_N, oldApp_start_n, app_start_n));
			}
		}
		return app_start_n;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node basicGetApp_start_n() {
		return app_start_n;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApp_start_n(Node newApp_start_n) {
		Node oldApp_start_n = app_start_n;
		app_start_n = newApp_start_n;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HepsycodePackage.CHANNEL__APP_START_N, oldApp_start_n, app_start_n));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWidth(int newWidth) {
		int oldWidth = width;
		width = newWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HepsycodePackage.CHANNEL__WIDTH, oldWidth, width));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HepsycodePackage.CHANNEL__MSG:
				return basicSetMsg(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HepsycodePackage.CHANNEL__MSG:
				return getMsg();
			case HepsycodePackage.CHANNEL__START_N:
				if (resolve) return getStart_n();
				return basicGetStart_n();
			case HepsycodePackage.CHANNEL__END_N:
				if (resolve) return getEnd_n();
				return basicGetEnd_n();
			case HepsycodePackage.CHANNEL__APP_END_N:
				if (resolve) return getApp_end_n();
				return basicGetApp_end_n();
			case HepsycodePackage.CHANNEL__APP_START_N:
				if (resolve) return getApp_start_n();
				return basicGetApp_start_n();
			case HepsycodePackage.CHANNEL__WIDTH:
				return getWidth();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HepsycodePackage.CHANNEL__MSG:
				setMsg((Message)newValue);
				return;
			case HepsycodePackage.CHANNEL__START_N:
				setStart_n((Node)newValue);
				return;
			case HepsycodePackage.CHANNEL__END_N:
				setEnd_n((Node)newValue);
				return;
			case HepsycodePackage.CHANNEL__APP_END_N:
				setApp_end_n((Node)newValue);
				return;
			case HepsycodePackage.CHANNEL__APP_START_N:
				setApp_start_n((Node)newValue);
				return;
			case HepsycodePackage.CHANNEL__WIDTH:
				setWidth((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HepsycodePackage.CHANNEL__MSG:
				setMsg((Message)null);
				return;
			case HepsycodePackage.CHANNEL__START_N:
				setStart_n((Node)null);
				return;
			case HepsycodePackage.CHANNEL__END_N:
				setEnd_n((Node)null);
				return;
			case HepsycodePackage.CHANNEL__APP_END_N:
				setApp_end_n((Node)null);
				return;
			case HepsycodePackage.CHANNEL__APP_START_N:
				setApp_start_n((Node)null);
				return;
			case HepsycodePackage.CHANNEL__WIDTH:
				setWidth(WIDTH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HepsycodePackage.CHANNEL__MSG:
				return msg != null;
			case HepsycodePackage.CHANNEL__START_N:
				return start_n != null;
			case HepsycodePackage.CHANNEL__END_N:
				return end_n != null;
			case HepsycodePackage.CHANNEL__APP_END_N:
				return app_end_n != null;
			case HepsycodePackage.CHANNEL__APP_START_N:
				return app_start_n != null;
			case HepsycodePackage.CHANNEL__WIDTH:
				return width != WIDTH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (width: ");
		result.append(width);
		result.append(')');
		return result.toString();
	}

} //ChannelImpl
