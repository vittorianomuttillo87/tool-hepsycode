/**
 */
package hepsycode.impl;

import hepsycode.HepsycodePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Process</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link hepsycode.impl.ProcessImpl#getImplementations <em>Implementations</em>}</li>
 *   <li>{@link hepsycode.impl.ProcessImpl#getPriority <em>Priority</em>}</li>
 *   <li>{@link hepsycode.impl.ProcessImpl#getCriticality <em>Criticality</em>}</li>
 *   <li>{@link hepsycode.impl.ProcessImpl#getPackages <em>Packages</em>}</li>
 *   <li>{@link hepsycode.impl.ProcessImpl#getType <em>Type</em>}</li>
 *   <li>{@link hepsycode.impl.ProcessImpl#getX <em>X</em>}</li>
 *   <li>{@link hepsycode.impl.ProcessImpl#getY <em>Y</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcessImpl extends NodeImpl implements hepsycode.Process {
	/**
	 * The cached value of the '{@link #getImplementations() <em>Implementations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplementations()
	 * @generated
	 * @ordered
	 */
	protected EList<hepsycode.Process> implementations;

	/**
	 * The default value of the '{@link #getPriority() <em>Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriority()
	 * @generated
	 * @ordered
	 */
	protected static final int PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPriority() <em>Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriority()
	 * @generated
	 * @ordered
	 */
	protected int priority = PRIORITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getCriticality() <em>Criticality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCriticality()
	 * @generated
	 * @ordered
	 */
	protected static final int CRITICALITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCriticality() <em>Criticality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCriticality()
	 * @generated
	 * @ordered
	 */
	protected int criticality = CRITICALITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPackages() <em>Packages</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackages()
	 * @generated
	 * @ordered
	 */
	protected EList<hepsycode.Package> packages;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = "Process";

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getX() <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX()
	 * @generated
	 * @ordered
	 */
	protected static final int X_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getX() <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX()
	 * @generated
	 * @ordered
	 */
	protected int x = X_EDEFAULT;

	/**
	 * The default value of the '{@link #getY() <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getY()
	 * @generated
	 * @ordered
	 */
	protected static final int Y_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getY() <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getY()
	 * @generated
	 * @ordered
	 */
	protected int y = Y_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcessImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HepsycodePackage.Literals.PROCESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<hepsycode.Process> getImplementations() {
		if (implementations == null) {
			implementations = new EObjectContainmentEList<hepsycode.Process>(hepsycode.Process.class, this, HepsycodePackage.PROCESS__IMPLEMENTATIONS);
		}
		return implementations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriority(int newPriority) {
		int oldPriority = priority;
		priority = newPriority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HepsycodePackage.PROCESS__PRIORITY, oldPriority, priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCriticality() {
		return criticality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCriticality(int newCriticality) {
		int oldCriticality = criticality;
		criticality = newCriticality;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HepsycodePackage.PROCESS__CRITICALITY, oldCriticality, criticality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<hepsycode.Package> getPackages() {
		if (packages == null) {
			packages = new EObjectContainmentEList<hepsycode.Package>(hepsycode.Package.class, this, HepsycodePackage.PROCESS__PACKAGES);
		}
		return packages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HepsycodePackage.PROCESS__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getX() {
		return x;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setX(int newX) {
		int oldX = x;
		x = newX;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HepsycodePackage.PROCESS__X, oldX, x));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getY() {
		return y;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setY(int newY) {
		int oldY = y;
		y = newY;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HepsycodePackage.PROCESS__Y, oldY, y));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case HepsycodePackage.PROCESS__IMPLEMENTATIONS:
				return ((InternalEList<?>)getImplementations()).basicRemove(otherEnd, msgs);
			case HepsycodePackage.PROCESS__PACKAGES:
				return ((InternalEList<?>)getPackages()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HepsycodePackage.PROCESS__IMPLEMENTATIONS:
				return getImplementations();
			case HepsycodePackage.PROCESS__PRIORITY:
				return getPriority();
			case HepsycodePackage.PROCESS__CRITICALITY:
				return getCriticality();
			case HepsycodePackage.PROCESS__PACKAGES:
				return getPackages();
			case HepsycodePackage.PROCESS__TYPE:
				return getType();
			case HepsycodePackage.PROCESS__X:
				return getX();
			case HepsycodePackage.PROCESS__Y:
				return getY();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HepsycodePackage.PROCESS__IMPLEMENTATIONS:
				getImplementations().clear();
				getImplementations().addAll((Collection<? extends hepsycode.Process>)newValue);
				return;
			case HepsycodePackage.PROCESS__PRIORITY:
				setPriority((Integer)newValue);
				return;
			case HepsycodePackage.PROCESS__CRITICALITY:
				setCriticality((Integer)newValue);
				return;
			case HepsycodePackage.PROCESS__PACKAGES:
				getPackages().clear();
				getPackages().addAll((Collection<? extends hepsycode.Package>)newValue);
				return;
			case HepsycodePackage.PROCESS__TYPE:
				setType((String)newValue);
				return;
			case HepsycodePackage.PROCESS__X:
				setX((Integer)newValue);
				return;
			case HepsycodePackage.PROCESS__Y:
				setY((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HepsycodePackage.PROCESS__IMPLEMENTATIONS:
				getImplementations().clear();
				return;
			case HepsycodePackage.PROCESS__PRIORITY:
				setPriority(PRIORITY_EDEFAULT);
				return;
			case HepsycodePackage.PROCESS__CRITICALITY:
				setCriticality(CRITICALITY_EDEFAULT);
				return;
			case HepsycodePackage.PROCESS__PACKAGES:
				getPackages().clear();
				return;
			case HepsycodePackage.PROCESS__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case HepsycodePackage.PROCESS__X:
				setX(X_EDEFAULT);
				return;
			case HepsycodePackage.PROCESS__Y:
				setY(Y_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HepsycodePackage.PROCESS__IMPLEMENTATIONS:
				return implementations != null && !implementations.isEmpty();
			case HepsycodePackage.PROCESS__PRIORITY:
				return priority != PRIORITY_EDEFAULT;
			case HepsycodePackage.PROCESS__CRITICALITY:
				return criticality != CRITICALITY_EDEFAULT;
			case HepsycodePackage.PROCESS__PACKAGES:
				return packages != null && !packages.isEmpty();
			case HepsycodePackage.PROCESS__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
			case HepsycodePackage.PROCESS__X:
				return x != X_EDEFAULT;
			case HepsycodePackage.PROCESS__Y:
				return y != Y_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (priority: ");
		result.append(priority);
		result.append(", criticality: ");
		result.append(criticality);
		result.append(", type: ");
		result.append(type);
		result.append(", x: ");
		result.append(x);
		result.append(", y: ");
		result.append(y);
		result.append(')');
		return result.toString();
	}

} //ProcessImpl
