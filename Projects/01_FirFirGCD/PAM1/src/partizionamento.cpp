//============================================================================
// Name        : cpp.cpp
// Author      : Vittoriano Muttillo, Luigi Pomante
// Version     :
// Copyright   : Your copyright notice
// Description : Partitioning
//============================================================================

#include "partizionamento.h"
#include <stdlib.h>

#include <iostream>

using namespace std;

void partizionamento::inizializza( specifica *s )
{
      int j;

      //int sum_BB=0;
      //int i = 0;

      spec=s; // Si aggancia alla specifica
      
      // Alloca la memoria
      Ptype   = new int[spec->numProc];
      istanza = new int[spec->numProc];
      salta   = new bool[spec->numProc];

      //maxBBArray = new int[spec->instanceBB];

      //localBBArray = new int[spec->instanceMaxBB];

      //for( j=0; j < spec->numBB; j++ ) maxBBArray[j] = 0;

      //for( j=0; j < spec->instanceMaxBB; j++ ) localBBArray[j] = 0;

      // Inizializza i pesi      
      wTDA=s->aff;  // Affinity
      wNTCC=s->comm;  // Comunication
      wEP=s->par_w;  // Parallelism
      wL=s->load;  // Load
      wC=s->cost;  // Cost
      wKB=s->size_SW;  // Size SW
      wGeq=s->size_HW;  // Size HW
      wCrit=s->crit; // Criticality
      
      wFattibilita=s->fatt;  // indice di fattibilità

      // Inizializza al MAX le funzioni di merito
      CF=1;
      TDA=1;
      NTCC=1;
      EP=1;
      L=1;
      C=1;
      KB=1;
      Geq=1;
      Crit=1;

      fattibilita=0;
      
      // Appena creato trattasi di neonato...
      // ovvero la CF non e' stata gia' calcolata
      neonato = true;

      // Creazione di un partizionamento casuale
      for( j=0; j < spec->numProc; j++ )
      {
           // Per ogni procedura sceglie un tipo e una istanza di esecutore
           // a caso ma entro i limiti imposti

          int loc =(rand()%spec->instanceBB);
          istanza[j] = spec->architettura[loc].id;

          if(spec->architettura[loc].processorType == "SPP" ){
       	   Ptype[j] = 2;  // SPP
          }else if (spec->architettura[loc].processorType == "DSP" ){
       	   Ptype[j] = 1;  // DSP
          }else if (spec->architettura[loc].processorType == "GPP" ){
       	   Ptype[j] = 0; // GPP
          }

    	  // Inserisco numero massimo istanze di BB che voglio (Evoluzione futura)

           /* if(sum_BB < spec->instanceMaxBB){

        	   int loc =(rand()%spec->instanceBB);

        	   if(maxBBArray[loc]==0){

        		   localBBArray[i]=loc;
        		   sum_BB++;
        		   maxBBArray[loc]++;

				   istanza[j] = spec->architettura[loc].id;

				   if(spec->architettura[loc].processorType == "SPP" ){
					   Ptype[j] = 2;  // SPP
				   }else if (spec->architettura[loc].processorType == "DSP" ){
					   Ptype[j] = 1;  // DSP
				   }else if (spec->architettura[loc].processorType == "GPP" ){
					   Ptype[j] = 0; // GPP
				   }
				   i++;

        	   }else{
        		   istanza[j] = spec->architettura[loc].id;

				   if(spec->architettura[loc].processorType == "SPP" ){
					   Ptype[j] = 2;  // SPP
				   }else if (spec->architettura[loc].processorType == "DSP" ){
					   Ptype[j] = 1;  // DSP
				   }else if (spec->architettura[loc].processorType == "GPP" ){
					   Ptype[j] = 0; // GPP
				   }
        	   }
           }else{

        	   int temp = (rand()%spec->instanceMaxBB);

        	   istanza[j] = spec->architettura[localBBArray[temp]].id;

			   if(spec->architettura[localBBArray[temp]].processorType == "SPP" ){
				   Ptype[j] = 2;  // SPP
			   }else if (spec->architettura[localBBArray[temp]].processorType == "DSP" ){
				   Ptype[j] = 1;  // DSP
			   }else if (spec->architettura[localBBArray[temp]].processorType == "GPP" ){
				   Ptype[j] = 0; // GPP
			   }

           }  */

           //istanza[j] = 0;
           //Ptype[j] = 0;

      }

      // elimino individui infattibili

}

partizionamento::~partizionamento()
{
      // delete[] allocataIn;
      delete[] istanza;
      delete[] salta;
}

// Valuta la funzione di costo complessiva
void partizionamento::valutaCF( float bestCF )
{
     // Calcola le varie funzioni di costo che costituiscono quella complesisva
     // L'ordine di valutazione dovrebbe essere a complessita' computazionale crescente
     // cosi' se esce prima si risparmia tempo
      CF=0;

      // Affinita'
      valutaTDA();
      CF+=wTDA*TDA;

      // Parallelismo
      valutaEP();
      CF+=wEP*EP;

      // Costo comunicazione
      valutaNTCC();
      CF+=wNTCC*NTCC;

      // Costo monetario
      valutaC();
      CF+=wC*C;

      // Carico
      valutaL();
      CF+=wL*L;

      // SIZE
     // valutaSize();
      CF+=wKB*KB;
      CF+=wGeq*Geq;

      // Criticità
      valutaCrit();
      CF+=wCrit*Crit;

      CF+=wFattibilita*fattibilita;

//      // Calcoli e confronti parziali (individui graziati?)
//
//      // Affinita'
//      valutaTDA();
//      CF+=wTDA*TDA;
//      if (CF>bestCF) return; //Se gia' maggiore del meglio (min) esce...
//
//      // Costo monetario
//      valutaC();
//      CF+=wC*C;      
//      if (CF>bestCF) return;
//
//      // Parallelismo
//      valutaEP();
//      CF+=wEP*EP;
//      if (CF>bestCF) return;
//
//      // Costo comunicazione
//      valutaNTCC();
//      CF+=wNTCC*NTCC;
//      if (CF>bestCF) return;
//
//      // Carico
//      valutaL();
//      CF+=wL*L;

}

// Valuta l'affinita' (Total Degree of Affinity)
void partizionamento::valutaTDA()
{
      int i;
      float sumDA=0;

      // Calcola il TDA
      // Media delle affinità derivanti dalle allocazioni effettive
      // E' una media di valori tra 0 e 1, quindi e' essa stessa tra 0 e 1
      // Piu' e' alta piu' le procedure sono allocate sugli esecutori piu' adatti

      for(i=0; i < spec->numProc; i++) sumDA += spec->progetto[i].DA[Ptype[i]];
      TDA= 1 - (sumDA/spec->numProc); // Costo da minimizzare

}

// Valuta i costi di comunicazione                  
void partizionamento::valutaNTCC()
{
      int i, j;
      
      // Inter Cluster Communication Cost
      long ICCC=0;
            
      // Il costo va valutato una sola volta per ogni coppia di procedure (triang bassa)
      // se e solo se queste sono allocate su esecutori differenti
     /* for(i=1; i<spec->numProc; i++)
            for(j=0; j<i; j++){
                if (istanza[i] != istanza[j])  ICCC += spec->comMat[i*spec->numProc+j];
            } */

      for(i=0; i<spec->numProc; i++)
		  for(j=0; j<spec->numProc; j++){
			  if (istanza[i] != istanza[j])  ICCC += spec->comMat[i*spec->numProc+j];
		  }
      
      // Normalized Total Communication Cost (rapporto con il max)
      NTCC = ( (float) ICCC)/spec->maxTCC;

}

// Valuta il parallelismo potenziale
void partizionamento::valutaEP()
{
      int i, j;
      
      // Inter Cluster Exploited Parallelism
      float ICEP=0;

      // Se procedure potenzialmente concorrenti sono su esecutori diversi
      // il grado di parallelismo potenziale offerto dal partizionamento aumenta
      // Il controllo si fa solo sulla triang bassa
      for(i=0; i < spec->numProc; i++)
            for(j=0; j < spec->numProc; j++){
                if (istanza[i] != istanza[j])  ICEP += spec->parMat[i*spec->numProc+j];
            }

      // Normalized parallelism: rapporto con il max e inversione
      EP = 1 - ( (float) ICEP)/spec->maxEP;

}

//Valuta il carico SW e HW
void partizionamento::valutaL()
{

    float loadP=0, loadT=0; // Carico parziale e totale SW
    float ave1=0; // Medie dei carichi di supporto per il calcolo finale
    fattibilita=0;

    int i,j;
    int num=0; //Numero di raggruppamenti (ovvero di esecurori SW)

    // Inizializza array per tenere traccia delle procedure gia' considerate
    for(i=0; i<spec->numProc; i++) salta[i]=false;
        
    // Si identificano raggruppamenti di procedure sugli esecutori
    // e si calcola il carico su ognuno di essi
    for(i=0; i<spec->numProc; i++)
    {
        // Calcola il carico SW
        if (Ptype[i] != 2)
        {

            if (salta[i]==false) // Se non gia' considerata->nuovo raggruppamento
            {
                    num++; //Aumenta il numero di raggrupamenti
                    loadP=spec->progetto[i].load[istanza[i]]; //Carico della procedura corrente  //aggiungere idproc
                   // cout << "Process: " << i << "LOAD: " << spec->progetto[i].load[istanza[i]] << " LOAD_P: " << loadP<< endl;
    
                    // Calcola il carico imposto all'esecutore dal raggruppamento
                    // dalle altre eventuali procedure allocate sullo stesso
                    for(j=i+1; j<spec->numProc; j++)
                    {
                            if (salta[j]==false)
                            {

                                if (istanza[i]==istanza[j])
                                {
                                	loadP+=spec->progetto[j].load[istanza[j]];
                                	//cout << "Process: " << j << "LOAD: " << spec->progetto[j].load[istanza[j]] << " LOAD_P: " << loadP<< endl;
                                    salta[j]=true; //Segna la procedura come gia' considerata
                                }
                            }
                    }        
                    salta[i]=true;//Segna la procedura come gia' considerata
    
                    // Penalizza se infattibile (carico>1)
                    if (loadP>spec->loadMax)
                    {
						L=1;
						fattibilita=1;
						return;
                    }
    
                    // Carico totale: somma dei carichi parziali
                    // Varia in funzione di quante procedure sono in HW
                    loadT+=loadP;
                    loadP=0;
            }
        }
    }

    // 1-Media del carico medio dei raggruppamenti SW e HW (piu' e' 0 meglio e'!) 
   if (num!=0) {
	   ave1=loadT/num;
	   L = 1 - (ave1);
   }
}

// Valuta il costo monetario (raggruppare con calcolo carico?)
void partizionamento::valutaC()
{
    float cost=0;
    int i,j;
 
    // Inizializza array per tenere traccia delle procedure gia' considerate
    for(i=0; i<spec->numProc; i++) salta[i]=false;
        
    for(i=0; (i<spec->numProc); i++)
    {
        // Per ogni esecutore nuovo viene aggiunto il suo costo
        if (salta[i]==false) // Se non gia' considerata...
        {

            // Calcola il costo (creare array dei costi per velocizzare?)

        	cost+= spec->architettura[istanza[i]].cost;
     
            // Segna eventuali altre presenze dell'esecutore
            for(j=i+1; (j<spec->numProc); j++)
            {
                    if (salta[j]==false)
                    {
                        if ((istanza[i]==istanza[j])) salta[j]=true;  // (allocataIn[i]==allocataIn[j])&&
                    }
            }        
            salta[i]=true;

        }
    }
   
    COSTO_INDIVIDUO = cost;
    C= cost/(spec->numBB*spec->maxCOST);// Rapporto con il max costo possibile
}

// Valuta il parallelismo potenziale
void partizionamento::valutaCrit()
{
      int i, j;
      int threeshold = 0;

      // Se procedure con criticità uguali sono su esecutori diversi
      // oppure procedure con criticità diverse sono su esecutori uguali
      // Pongo a 1 il parametro di Crit
      Crit = 0;
      for(i=0; i < spec->numProc; i++){
            for(j=0; j < spec->numProc; j++)
            {
				if (istanza[i] == istanza[j]){
					if( abs(spec->progetto[i].criticality - spec->progetto[j].criticality) > threeshold ){
						Crit = 1;
					}
				}
				/*if (istanza[i] != istanza[j] && abs(spec->progetto[i].criticality - spec->progetto[j].criticality) == 0 ){
					Crit = 1;
				} */
            }
       }
}

// Valuta il parallelismo potenziale
void partizionamento::valutaSize()
{
	float sizeSW_RAM=0;
	float sizeSW_ROM=0;
	float sizeSW_Max=0;
	float sizeHW=0;
	float sizeHWMax=0;
	int i, j;

    // Inizializza array per tenere traccia delle procedure gia' considerate
    for(i=0; i<spec->numProc; i++) salta[i]=false;

    for(i=0; (i<spec->numProc); i++)
    {
        // Per ogni esecutore nuovo viene aggiunto la sua size
        if (salta[i]==false) // Se non gia' considerata...
        {
        	// esecutori SW
        	if(spec->architettura[istanza[i]].processorType == "GPP" || spec->architettura[istanza[i]].processorType == "DSP"){
        		sizeSW_RAM += spec->progetto[i].size_SW_RAM[istanza[i]];
				sizeSW_ROM += spec->progetto[i].size_SW_ROM[istanza[i]];

        		for(j=i+1; (j<spec->numProc); j++)
				{
						if (salta[j]==false)
						{
							if (istanza[i]==istanza[j])
							{
								sizeSW_RAM += spec->progetto[j].size_SW_RAM[istanza[j]];
								sizeSW_ROM += spec->progetto[j].size_SW_ROM[istanza[j]];
								salta[j]=true; //Segna la procedura come gia' considerata
							}
						}
				}
				salta[i]=true;//Segna la procedura come gia' considerata

				 // Penalizza se infattibile (size>1)
				if ( sizeSW_RAM > (spec->architettura[istanza[i]].dataSize) )
				{
					KB=1;
					return;
				}else if( sizeSW_ROM > (spec->architettura[istanza[i]].codeSize) ){
					KB=1;
					return;
				}else{
					sizeSW_Max += (spec->architettura[istanza[i]].dataSize) + (spec->architettura[istanza[i]].dataSize);
				}

        	}else if(spec->architettura[istanza[i]].processorType == "SPP"){ // Esecutore HW

        		sizeHW += spec->progetto[i].eqg;

        		for(j=i+1; (j<spec->numProc); j++)
				{
						if (salta[j]==false)
						{
							if (istanza[i]==istanza[j])
							{
								sizeHW += spec->progetto[j].eqg;
								salta[j]=true; //Segna la procedura come gia' considerata
							}
						}
				}
				salta[i]=true;//Segna la procedura come gia' considerata

				 // Penalizza se infattibile (carico>1)
				if ( sizeHW > (spec->architettura[istanza[i]].eqG) )
				{
					Geq=1;
					return;
				}else{
					sizeHWMax += (spec->architettura[istanza[i]].eqG);
				}
        	}

            // Segna eventuali altre presenze dell'esecutore
            for(j=i+1; (j<spec->numProc); j++)
            {
                    if (salta[j]==false)
                    {
                        if ((istanza[i]==istanza[j])) salta[j]=true;
                    }
            }
            salta[i]=true;

        }
    }

    if(( (sizeSW_ROM + sizeSW_RAM) - sizeSW_Max) > 0 && sizeSW_Max > 0){
    	KB = ( (sizeSW_ROM + sizeSW_RAM) - sizeSW_Max)/sizeSW_Max;
    }else if(sizeSW_Max > 0){
    	KB = (-( (sizeSW_ROM + sizeSW_RAM) - sizeSW_Max)) / sizeSW_Max;
    }

    if((sizeHW - sizeHWMax) > 0 && sizeHWMax > 0){
    	Geq = (sizeHW - sizeHWMax)/sizeHWMax;
    }else if(sizeHWMax > 0){
    	Geq = (-(sizeHW - sizeHWMax))/sizeHWMax;
    }

}

// Valuta criticità
bool partizionamento::criticalityCheck()
{
      int i, j;
      bool criticality;
      int threeshold = 0;

      // Se procedure potenzialmente concorrenti sono su esecutori diversi
      // il grado di parallelismo potenziale offerto dal partizionamento aumenta
      // Il controllo si fa solo sulla triang bassa
      criticality = false;
      for(i=0; i < spec->numProc; i++){
            for(j=0; j < spec->numProc; j++)
            {
				if (istanza[i] == istanza[j]){
					if( abs(spec->progetto[i].criticality - spec->progetto[j].criticality) > threeshold ){
						criticality = true;
					}
				}
				if (istanza[i] != istanza[j] && abs(spec->progetto[i].criticality - spec->progetto[j].criticality) == 0 ){
					criticality = true;
				}
            }
       }
      return criticality;
}

