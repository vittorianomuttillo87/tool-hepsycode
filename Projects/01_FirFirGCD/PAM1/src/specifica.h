#ifndef __SPECIFICA__
#define __SPECIFICA__

#include "procedura.h"
#include "basicBlock.h"

// Specifica: insieme di procedure che la compongono piu' varie info

class specifica
{      
      public:

			// Genetic Algorithm parameters
			unsigned long generazioni;
			unsigned long initialPop;
			unsigned long nPart;
			unsigned long numMaxPart;
			float crip;
			float crad;
			float tmort;

			// Cost function weight
			float aff;
			float comm;
			float par_w;
			float load;
			float cost;

			float size_SW;
			float size_HW;
			float crit;
			float fatt;

			// Constraints

			float TTC;
			unsigned int numberProcesses;
			int instanceBB;
			int instanceMaxBB;
			float loadMax;

            // Procedure che compongono la specifica (puntatore ad array)
            procedura *progetto;

            // Procedure che compongono la specifica (puntatore ad array)
            basicblock *architettura;


            // Numero di procedure che compongono la specifica
            int numProc;
            
            // Numero di BB che compongono la specifica
            int numBB;

            // Matrice delle comunicazioni tra le procedure
            // matrice simmetrica le cui righe e colonne rappresentano le procedure
            // e il contenuto i byte scambiati tra le coppie di procedure
            // Es.
            //      P1  P2  P3
            //   P1 0   10  30
            //   P2 10  0   15
            //   P3 30  15  0
            float *comMat;
            
            // Max costo possibile delle comunicazioni tra procedure
            // calcolato ipotizzando che tutte siano su esecutori diversi
            float maxTCC;

            //Matrice del parallelismo
            // matrice simmetrica le cui righe e colonne rappresentano le procedure
            // e il contenuto se c'e' (1) o meno (0) possibilita' di parallelismo
            // tra le coppie di procedure
            // Es.
            //      P1  P2  P3
            //   P1 0   1   1
            //   P2 1   0   0
            //   P3 1   0   0
            float *parMat;

            // Max parallelismo possibile
            // calcolato ipotizzando che tutte le procedure potenzialmente parallele
            // siano su esecutori diversi e quindi possano esserlo davvero
            float maxEP;

            float maxCOST;

            // Costruttore
            specifica(int start);
            
            void load_parameters();
            void load_constraints();
            void create_basicblocks();
            void create_processes();
            void create_basicBlocks();

            // Distruttore
            ~specifica();
            
};

#endif
