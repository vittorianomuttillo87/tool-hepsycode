#include <systemc.h>
#include "sc_csp_channel_ifs.h"
#include "sc_csp_channel.h"
#include "datatype.h"
#include "stim_gen.h"
#include "mainsystem.h"
#include "display.h"
#include <string.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////////////////

int sc_main(int a, char* b[])
{
	// Channels for the connection to the main system

	sc_csp_channel< sc_uint<8> >   stim1_channel (12,0,0,2);
	sc_csp_channel< sc_uint<8> >   stim2_channel (13,0,0,5);
	sc_csp_channel< sc_uint<8> >   result_channel (14,0,8,1);

	// Instantiation and connection of testbench and system

	stim_gen mystimgen("mystimgen");
	mystimgen.stim1_channel_port(stim1_channel); 
	mystimgen.stim2_channel_port(stim2_channel); 

	mainsystem mysystem("mysystem");
	mysystem.stim1_channel_port(stim1_channel);
	mysystem.stim2_channel_port(stim2_channel);
	mysystem.result_channel_port(result_channel);

	display mydisplay("mydisplay");
	mydisplay.result_channel_port(result_channel); 

	sc_start();

	#if _WIN32
	    system("pause");
	#endif
	

	return 0;
}
