#include <systemc.h>
#include "display.h"

void display::main()
{
	int i=1;
	sc_uint<8> tmp1;

	while(1)
	{
		tmp1=result_channel_port->read();
		cout << "Display-" << i <<": \t" << tmp1 << " " << "\t at time \t" << sc_time_stamp() << endl;
		i++;
	}
}
