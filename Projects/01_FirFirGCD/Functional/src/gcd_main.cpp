#include <systemc.h>
#include "mainsystem.h"

//gcdm
void mainsystem::gcd_main()
{

	// datatype for channels
	gcde_parameters gcde_p;
	gcde_results gcde_r;

	// local variables
	sc_uint<8> sample1;
	sc_uint<8> sample2;
	sc_uint<8> result;
	bool err;

	// init
	 err=false;

	while(1)
	{ 

		// main functionality

		// Main input from channel

		 sample1=result8_channel->read();
		 if (sample1==0) err=true;

		 sample2=result16_channel->read();
		 if (sample2==0) err=true;

		 if (err==false)
		{
			// fill datatype
			gcde_p.sample1=sample1;
			gcde_p.sample2=sample2;

			// send parameters and receive results
			 gcde_parameters_channel->write(gcde_p);
 			 gcde_r=gcde_results_channel->read();

			// fill local variables
			result=gcde_r.result;

		}	
		else
		{
			 result=0;
			 err=false;
		}

		 result_channel_port->write(result);

		
	}
}

