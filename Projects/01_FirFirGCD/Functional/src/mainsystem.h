#ifndef __MYSYSTEM__
#define __MYSYSTEM__

#include "sc_csp_channel_ifs.h"
#include "sc_csp_channel.h"
#include "datatype.h"

SC_MODULE(mainsystem)
{
	// Ports for testbench connections
	sc_port< sc_csp_channel_in_if< sc_uint<8> > > stim1_channel_port;
	sc_port< sc_csp_channel_in_if< sc_uint<8> > > stim2_channel_port;
	sc_port< sc_csp_channel_out_if< sc_uint<8> > > result_channel_port;

	// PROCESSES

	// Process
	void fir8_main();
	// Fake processes
	void fir8_evaluation();
	void fir8_shifting();

	// Process
	void fir16_main();
	// Fake processes
	void fir16_evaluation();
	void fir16_shifting();

	// Process
	void gcd_main();
	// Fake process
	void gcd_evaluation();

	// CHANNELS

	// fir8
	sc_csp_channel< fir8e_parameters > *fir8e_parameters_channel;
	sc_csp_channel< fir8e_results > *fir8e_results_channel;
	sc_csp_channel< fir8s_parameters > *fir8s_parameters_channel;
	sc_csp_channel< fir8s_results > *fir8s_results_channel;

	// fir16
	sc_csp_channel< fir16e_parameters > *fir16e_parameters_channel;
	sc_csp_channel< fir16e_results > *fir16e_results_channel;
	sc_csp_channel< fir16s_parameters > *fir16s_parameters_channel;
	sc_csp_channel< fir16s_results > *fir16s_results_channel;

	// gcd
	sc_csp_channel< gcde_parameters > *gcde_parameters_channel;
	sc_csp_channel< gcde_results > *gcde_results_channel;

	// Inter-process connections
	sc_csp_channel< sc_uint<8> > *result8_channel;  	    
	sc_csp_channel< sc_uint<8> >   *result16_channel;  	    

	SC_CTOR(mainsystem)
	{

		fir8e_parameters_channel= new sc_csp_channel< fir8e_parameters > (0,0,2,3,1);
		fir8e_results_channel= new sc_csp_channel< fir8e_results > (1,0,3,2,1);
		fir8s_parameters_channel= new sc_csp_channel< fir8s_parameters > (2,0,2,4,1);
		fir8s_results_channel= new sc_csp_channel< fir8s_results > (3,0,4,2,1);

		fir16e_parameters_channel= new sc_csp_channel< fir16e_parameters > (4,0,5,6,1);
		fir16e_results_channel= new sc_csp_channel< fir16e_results > (5,0,6,5,1);
		fir16s_parameters_channel= new sc_csp_channel< fir16s_parameters > (6,0,5,7,1);
		fir16s_results_channel= new sc_csp_channel< fir16s_results > (7,0,7,5,1);

		gcde_parameters_channel = new sc_csp_channel< gcde_parameters > (10,0,8,9,1);
		gcde_results_channel = new sc_csp_channel< gcde_results > (11,0,9,8,1);

		result8_channel= new sc_csp_channel< sc_uint<8> > (8,0,2,8,1);
		result16_channel= new sc_csp_channel< sc_uint<8> > (9,0,5,8,1); 

		SC_THREAD(fir8_main);
		SC_THREAD(fir8_evaluation);
		SC_THREAD(fir8_shifting);

		SC_THREAD(fir16_main);
		SC_THREAD(fir16_evaluation);
		SC_THREAD(fir16_shifting);

		SC_THREAD(gcd_main);
		SC_THREAD(gcd_evaluation);

	}

};

#endif