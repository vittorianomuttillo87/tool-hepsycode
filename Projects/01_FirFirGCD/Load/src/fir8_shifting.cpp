
#include "mainsystem.h"
#include <math.h>

#define TAP8 8

//f8s
void mainsystem::fir8_shifting()
{
	// datatype for channels
	fir8s_parameters fir8s_p;
	fir8s_results fir8s_r;

	// local variables
	sc_uint<8> sample_tmp;
	sc_uint<8> shift[8];

	while(1)
	{
		// read parameters from channel
		S(4) fir8s_p=fir8s_parameters_channel->read();

		// fill local variables
		S(4) sample_tmp=fir8s_p.sample_tmp;
		S(4) for( unsigned j=0; j<TAP8; j++) shift[j]=fir8s_p.shift[j];

		// process

		S(4) for(int i=TAP8-2; i>=0; i--)
		{S(4)
			S(4) shift[i+1] = shift[i];
		S(4)}
		S(4) shift[0]=sample_tmp;

		// fill datatype
		S(4) for( unsigned j=0; j<TAP8; j++) fir8s_p.shift[j]=shift[j];

		// send results by channel
		S(4) fir8s_results_channel->write(fir8s_r);

		P(4)
	}
}




