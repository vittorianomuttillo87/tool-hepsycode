/*
 * main.cpp
 *
 *  Created on: 30 set 2016
 *      Author: daniele
 */

#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <set>

#include <systemc.h>
#include "sc_csp_channel_ifs.h"
#include "datatype.h"
#include "stim_gen.h"
#include "mainsystem.h"
#include "display.h"
#include <string.h>

#include "sc_csp_channel_timing_L.h"
#include "SchedulingManager_timing_L.h"
#include "SystemManager_timing_L.h"
#include "tl.h"

using namespace std;

SystemManager		*pSystemManager = new SystemManager();
SchedulingManager	*pSchedulingManager = new SchedulingManager("SchedulingManager");;

int sc_main(int a, char* b[])
{

	// ./XML/mapping_MPU8051_0.xml per 8051
	// ./XML/mapping_PIC24_1.xml per PIC24
	// ./XML/mapping_MPULEON3_2.xml per LEON3

	//char* mapping = "./XML/mapping_MPULEON3_2.xml";
	//multimap<int, pair<string,int> > allocat = (pSystemManager->mapping("./XML/mapping_PIC24_1.xml"));
	//pSystemManager->setAllocation(allocat);

	/////////////////////////////////////////////////////////////////////////////////////////
	// Testbech and System
	/////////////////////////////////////////////////////////////////////////////////////////

	// Channels for the connection to the main system

	sc_csp_channel< sc_uint<8> >   stim1_channel(8, 0, 2, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);
	sc_csp_channel< sc_uint<8> >   stim2_channel(8, 0, 5, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);
	sc_csp_channel< sc_uint<8> >   result_channel(8, 8, 1, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);

	// Instantiation and connection of testbench and system

	stim_gen mystimgen("mystimgen");
	mystimgen.stim1_channel_port(stim1_channel);
	mystimgen.stim2_channel_port(stim2_channel);

	mainsystem mysystem("mysystem");
	mysystem.stim1_channel_port(stim1_channel);
	mysystem.stim2_channel_port(stim2_channel);
	mysystem.result_channel_port(result_channel);

	display mydisplay("mydisplay");
	mydisplay.result_channel_port(result_channel);

	/////////////////////////////////////////////////////////////////////////////////////////
	// Simulation management
	/////////////////////////////////////////////////////////////////////////////////////////

	// Time info
	sc_time end, end_NO_OH=sc_time(0, SC_MS), tot=sc_time(0, SC_MS);
	sc_time tot_proc=sc_time(0, SC_MS);
	sc_time tot_comm=sc_time(0, SC_MS);
	double tot_proc_load = 0;

	// Start simulation
	sc_start();

	//  Total simulated time
	end=sc_time_stamp();

	#if _WIN32
		system("pause");
	#endif

	/////////////////////////////////////////////////////////////////////////////////////////
	// Report
	/////////////////////////////////////////////////////////////////////////////////////////

	//// TIMING DATA

	cout << endl << "FINAL SIMULATED TIME (with overhead): " << end << endl;
	end_NO_OH = end;

	cout << endl << "PROCESS TIME PROFILING" << endl;

	cout << endl << "Average NET TIME for each process:" << endl << endl;

	for(unsigned i = 2; i<pSystemManager->getVPS().size(); i++){
		cout << pSystemManager->getVPS()[i].id << "-" << pSystemManager->getVPS()[i].name << " " << (pSystemManager->getVPS()[i].processTime/pSystemManager->getVPS()[i].profiling) << endl;
		cout << endl;
		tot+=pSystemManager->getVPS()[i].processTime;
	}
	cout << "Total NET TIME for all the processes:" << tot.to_seconds() << endl;

	cout << endl << "Schedulers Overhead" << endl;
	for(int i = 0; i< pSystemManager->getmaxBBSoftware(); i++){
		end_NO_OH = end_NO_OH - pSchedulingManager->sched_oh[i];
		cout << pSchedulingManager->sched_oh[i] << endl;
	}

	//pSystemManager->setFRT(end_NO_OH);
	//pSystemManager->setFRT(end);

	cout << endl << "COMMUNICATION PROFILING" << endl << endl;

	cout << "WRITER\t\t\tREADER\t\t\tCOM\t\t\tBIT\t\t\tNUM*BIT\t\t\tTIME (sec)" << endl <<endl;
	tot_comm += stim1_channel.report_profiling_WT();
	tot_comm += stim2_channel.report_profiling_WT();
	tot_comm += result_channel.report_profiling_WT();
	tot_comm += mysystem.result8_channel->report_profiling_WT();
	tot_comm += mysystem.result16_channel->report_profiling_WT();
	tot_comm += mysystem.fir8e_parameters_channel->report_profiling_WT();
	tot_comm += mysystem.fir8e_results_channel->report_profiling_WT();
	tot_comm += mysystem.fir8s_parameters_channel->report_profiling_WT();
	tot_comm += mysystem.fir8s_results_channel->report_profiling_WT();
	tot_comm += mysystem.fir16e_parameters_channel->report_profiling_WT();
	tot_comm += mysystem.fir16e_results_channel->report_profiling_WT();
	tot_comm += mysystem.fir16s_parameters_channel->report_profiling_WT();
	tot_comm += mysystem.fir16s_results_channel->report_profiling_WT();
	tot_comm += mysystem.gcde_parameters_channel->report_profiling_WT();
	tot_comm += mysystem.gcde_results_channel->report_profiling_WT();
	cout << endl;

	tot_proc = tot + tot_comm;

	pSystemManager->setFRT(tot_proc);

	cout << endl << "LOAD ESTIMATION" << endl;

	pSystemManager->loadEst(tot_proc);

	for(unsigned i =2; i<pSystemManager->getVPS().size(); i++){
		cout <<endl<<"FRL:"<<" "<< pSystemManager->getVPS()[i].id << "-" << pSystemManager->getVPS()[i].name;
		cout << " " << (pSystemManager->getFRL()[i])<< endl;
		tot_proc_load += pSystemManager->getFRL()[i];
	}
	cout << endl << "FINAL TOTAL LOAD: " << tot_proc_load << endl;

	cout << endl << "FINAL NET SIMULATED TIME (without overhead): " << end_NO_OH << endl;
	cout << endl << "FINAL NET SIMULATED TIME (sum net process time): " << tot_proc << endl;
	cout << endl << "FINAL NET COMMUNICATION TIME: " << tot_comm << endl;
	cout << endl << "FINAL NET SIMULATED TIME (NET + COMM): " << tot_proc + tot_comm << endl;

	pSystemManager->deleteConcXml();
	pSystemManager->updateXml();

	#if _WIN32
		system("pause");
	#endif

	return 0;

}


