#ifndef __SCHEDULINGMANAGER__

#define __SCHEDULINGMANAGER__

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>

#include <systemc.h>
#include "SystemManager_timing_L.h"
#include "tl.h"

extern SystemManager *pSystemManager;

#include "define.h"

SC_MODULE(SchedulingManager)
{
	public:

	// Data structure to manage scheduling

	bool ready[NPS];
	sc_event schedule[NPS];
	sc_event release[NPS];

	sc_time sched_oh[5];

	SC_CTOR(SchedulingManager)
	{
		SC_THREAD(scheduler_8051_0);  // uno scheduler per ogni istanza di processore
		SC_THREAD(scheduler_8051_1);
		SC_THREAD(scheduler_dspic_0);
		SC_THREAD(scheduler_dspic_1);
		SC_THREAD(scheduler_leon3_0);
		SC_THREAD(scheduler_leon3_1);

		// START
		for(unsigned int j=0; j<pSystemManager->getVPS().size(); j++)
		{
			ready[j]=true;
			sched_oh[j]=sc_time(0, SC_MS);
		}
	}  

	// Schedulers
	void scheduler_8051_0();   // funzioni degli schedulers che vengono assegnati ai threads
	void scheduler_8051_1();
	void scheduler_dspic_0();
	void scheduler_dspic_1();
	void scheduler_leon3_0();
	void scheduler_leon3_1();

	void scheduler_xy_RR(string pname, int pinstance);
	void scheduler_xy_FP(string pname, int pinstance);
	void scheduler_xy_NO_OH(string pname, int pinstance);
};

// Macro for instrumentation

#define S(X) 						\
		pSystemManager->Increase(X);   \
		if(!pSystemManager->checkSPP(X)) wait(pSchedulingManager->schedule[X]); \
		wait(pSystemManager->upSimTime(X));   \
		if(!pSystemManager->checkSPP(X)) pSchedulingManager->release[X].notify(SC_ZERO_TIME);

#endif
