
#include "mainsystem.h"
#include <math.h>

#define TAP8 8

//f8e
void mainsystem::fir8_evaluation()
{
	// datatype for channels
	fir8e_parameters fir8e_p;
	fir8e_results fir8e_r;

	// local variables
	sc_int<17> pro;
	sc_uint<19> acc;
	sc_uint<9> coef[8];
	sc_uint<8>  sample_tmp;
	sc_uint<8> shift[8];

	while(1)
	{
		// read parameters from channel
		S(3) fir8e_p=fir8e_parameters_channel->read();

		// fill local variables
		S(3) sample_tmp=fir8e_p.sample_tmp;
		S(3) for( unsigned j=0; j<TAP8; j++) coef[j]=fir8e_p.coef[j];
		S(3) for( unsigned j=0; j<TAP8; j++) shift[j]=fir8e_p.shift[j];

		// process
		S(3) acc=sample_tmp*coef[0];

		S(3) for(int i=TAP8-2; i>=0; i--)
		{S(3)
			S(3) pro=shift[i]*coef[i+1];
			S(3) acc += pro;
		S(3)}

		// fill datatype
		S(3) fir8e_r.acc=acc;

		// send results by channel
		S(3) fir8e_results_channel->write(fir8e_r);

		P(3)
	}
}



