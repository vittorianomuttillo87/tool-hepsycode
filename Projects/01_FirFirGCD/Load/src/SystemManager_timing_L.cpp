/*
 * SystemManager.cpp
 *
 *  Created on: 07 ott 2016
 *      Author: daniele
 */

#include <systemc.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <set>
#include <string.h>
#include <iostream>
#include <sstream>
#include <string>

#include "tl.h"
#include "pugixml.hpp"

#include "SystemManager_timing_L.h"

using namespace std;
using namespace pugi;

template<class T>
std::string to_string(const T &value) {
    std::ostringstream os;
    os << value;
    return os.str();
}

SystemManager::SystemManager(){
	VP = generateProcessorInstances();
	VPS = generateProcessInstances();
	VBB = generateBBInstances();
	allocation = mapping();
	L = generateLinkInstance();
	VCH = generateChannelInstances();
	//allocationLC = mappingLC();
}

vector<ProcessingUnit> SystemManager:: getVP()
{
	return this->VP;
}

vector<Process> SystemManager:: getVPS()
{
	return this->VPS;
}

vector<BasicBlock> SystemManager:: getVBB()
{
	return this->VBB;
}

multimap<int, pair<string,int> > SystemManager:: getAllocation()
{
	return this->allocation;
}

void SystemManager:: setAllocation(multimap<int, pair<string,int> > allocation)
{
	this->allocation = allocation;
}


vector<BasicBlock> SystemManager:: generateBBInstances(){

	/*****************************
		 *   LOAD BASIC BLOCKS
		*****************************/

	char* temp;
	vector<BasicBlock> vbb;
	// vector<ProcessingUnit> vp;
	maxBBSoftware = 0;

	// parsing xml file
	xml_document myDoc;
	xml_parse_result myResult = myDoc.load_file("./XML/instancesTL.xml");
	xml_node instancesBB = myDoc.child("instancesBB");

	// cout<<endl<<"****** Basic Blocks ******"<<endl;

	for (xml_node_iterator seqBB_it=instancesBB.begin(); seqBB_it!=instancesBB.end(); ++seqBB_it){

		xml_node_iterator BB_it = seqBB_it->begin();
		BasicBlock bb;

		//ID
		int id = atoi(BB_it->child_value());
		bb.setId(id);
		// cout << "id: " << bb.getId() << endl;

		//NAME
		BB_it++;
		string BBname = BB_it->child_value();
		bb.setName(BBname);
		// cout << "Name: " << bb.getName() << endl;

		//TYPE
		BB_it++;
		string type = BB_it->child_value();
		bb.setType(type);
		// cout << "Type: " << bb.getType() << endl;

		// cout << endl;

		// PROCESSING UNIT

		BB_it++;

		xml_node instancesPU = seqBB_it->child("processingUnit");
		xml_node_iterator pu_it = instancesPU.begin();

		// ProcessingUnit pu;

		//NAME
		string puName = pu_it->child_value();
		// pu.setName(puName);
		// cout << "PU name: " << pu.getName() << endl;

		//ID
		pu_it++;
		int idPU = atoi(pu_it->child_value());
		// pu.setId(idPU);
		// cout << "PU id: " << pu.getId() << endl;

		//Processor Type
		pu_it++;
		string puType = pu_it->child_value();
		if(puType == "GPP" || puType == "DSP"){
			maxBBSoftware++;
		}
		// pu.setProcessorType(puType);
		// cout << "PU processor Type: " << pu.getProcessorType() << endl;

		// cost
		pu_it++;
		float idCost = atof(pu_it->child_value());
		// pu.setCost(idCost);
		// cout << "PU Cost: " << pu.getCost() << endl;

		//ISA
		pu_it++;
		string puISA = pu_it->child_value();
		// pu.setISA(puISA);
		// cout << "PU ISA: " << pu.getISA() << endl;

		// Frequency
		pu_it++;
		float idFreq = atof(pu_it->child_value());
		// pu.setFrequency(idFreq);
		// cout << "PU Frequency: " << pu.getFrequency() << endl;

		// CC4CS
		pu_it++;
		float idCC4CS = atof(pu_it->child_value());
		// pu.setCC4S(idCC4CS);
		// cout << "PU CC4CS: " << pu.getCC4S() << endl;

		// overheadCS
		pu_it++;
		float idOver = atof(pu_it->child_value());
		// pu.setOverheadCS(sc_time(idOver, SC_MS));
		// cout << "PU Overhead ContextSwitch: " << pu.getOverheadCS() << endl;

		// cout << endl;

		// LOACL MEMORY

		BB_it++;

		xml_node instancesLM = seqBB_it->child("localMemory");
		xml_node_iterator lm_it = instancesLM.begin();

		//CODE SIZE
		int lmCodeSize = atof(lm_it->child_value());
		bb.setCodeSize(lmCodeSize);
		// cout << "LM Code Size: " << bb.getCodeSize() << endl;

		//DATA SIZE
		lm_it++;
		int lmDataSize= atof(lm_it->child_value());
		bb.setDataSize(lmDataSize);
		// cout << "LM Data Size: " << bb.getDataSize() << endl;

		//eQG
		lm_it++;
		temp = (char*) lm_it->child_value();
		int lmEqG = atof(temp);
		bb.setEqG(lmEqG);
		// cout << "LM eQG: " << bb.getEqG()<< endl;

		// Comunication
		BB_it++;

		xml_node instancesCU = seqBB_it->child("communicationUnit");
		xml_node_iterator cu_it = instancesCU.begin();

		// TO DO

		// Free Running time
		/* BB_it++;

		xml_node instancesFRT = seqBB_it->child("loadEstimation");
		xml_node_iterator frt_it = instancesFRT.begin();

		float lmFreeRunningTime= atof(frt_it->child_value());
		bb.setFRT(lmFreeRunningTime);
		// cout << "Free Running time: " << bb.getFRT() << endl; */


		// cout << endl;

		// vp.push_back(pu);
		vbb.push_back(bb);
	}

	return vbb;
}



vector<ProcessingUnit> SystemManager:: generateProcessorInstances()
{
	/*****************************
		 *   LOAD PROCESSORS
		*****************************/

	char* temp;
	maxBBSoftware = 0;
	// vector<BasicBlock> vbb;
	vector<ProcessingUnit> vp;

	// parsing xml file
	xml_document myDoc;
	xml_parse_result myResult = myDoc.load_file("./XML/instancesTL.xml");
	xml_node instancesBB = myDoc.child("instancesBB");

	// cout<<endl<<"****** Processors ******"<<endl;

	for (xml_node_iterator seqBB_it=instancesBB.begin(); seqBB_it!=instancesBB.end(); ++seqBB_it){

		xml_node_iterator BB_it = seqBB_it->begin();
		// BasicBlock bb;

		//ID
		int id = atoi(BB_it->child_value());
		// bb.setId(id);
		// cout << "id: " << bb.getId() << endl;

		//NAME
		BB_it++;
		string BBname = BB_it->child_value();
		// bb.setName(BBname);
		// cout << "Name: " << bb.getName() << endl;

		//TYPE
		BB_it++;
		string type = BB_it->child_value();
		// bb.setType(type);
		// cout << "Type: " << bb.getType() << endl;

		// cout << endl;

		// PROCESSING UNIT

		BB_it++;

		xml_node instancesPU = seqBB_it->child("processingUnit");
		xml_node_iterator pu_it = instancesPU.begin();

		ProcessingUnit pu;

		//NAME
		string puName = pu_it->child_value();
		pu.setName(puName);
		// cout << "PU name: " << pu.getName() << endl;

		//ID
		pu_it++;
		int idPU = atoi(pu_it->child_value());
		pu.setId(idPU);
		// cout << "PU id: " << pu.getId() << endl;

		//Processor Type
		pu_it++;
		string puType = pu_it->child_value();
		pu.setProcessorType(puType);
		if(puType == "GPP" || puType == "DSP"){
			maxBBSoftware++;
		}
		// cout << "PU processor Type: " << pu.getProcessorType() << endl;

		// cost
		pu_it++;
		float idCost = atof(pu_it->child_value());
		pu.setCost(idCost);
		// cout << "PU Cost: " << pu.getCost() << endl;

		//ISA
		pu_it++;
		string puISA = pu_it->child_value();
		pu.setISA(puISA);
		// cout << "PU ISA: " << pu.getISA() << endl;

		// Frequency
		pu_it++;
		float idFreq = atof(pu_it->child_value());
		pu.setFrequency(idFreq);
		// cout << "PU Frequency: " << pu.getFrequency() << endl;

		// CC4CS
		pu_it++;
		float idCC4CS = atof(pu_it->child_value());
		pu.setCC4S(idCC4CS);
		//cout << "PU CC4CS: " << pu.getCC4S() << endl;

		// overheadCS
		pu_it++;
		float idOver = atof(pu_it->child_value());
		pu.setOverheadCS(sc_time(idOver, SC_MS));
		//cout << "PU Overhead ContextSwitch: " << pu.getOverheadCS() << endl;

		// cout << endl;

		// LOACL MEMORY

		BB_it++;

		xml_node instancesLM = seqBB_it->child("localMemory");
		xml_node_iterator lm_it = instancesLM.begin();

		//CODE SIZE
		int lmCodeSize = atof(lm_it->child_value());
		// bb.setCodeSize(lmCodeSize);
		// cout << "LM Code Size: " << bb.getCodeSize() << endl;

		//DATA SIZE
		lm_it++;
		int lmDataSize= atof(lm_it->child_value());
		// bb.setDataSize(lmDataSize);
		// cout << "LM Data Size: " << bb.getDataSize() << endl;

		//eQG
		lm_it++;
		temp = (char*) lm_it->child_value();
		int lmEqG = atof(temp);
		// bb.setEqG(lmEqG);
		// cout << "LM eQG: " << bb.getEqG()<< endl;

		// Comunication
		BB_it++;

		xml_node instancesCU = seqBB_it->child("communicationUnit");
		xml_node_iterator cu_it = instancesCU.begin();

		// TO DO

		// Free Running time
		/* BB_it++;

		xml_node instancesFRT = seqBB_it->child("loadEstimation");
		xml_node_iterator frt_it = instancesFRT.begin();

		float lmFreeRunningTime= atof(frt_it->child_value());
		// bb.setFRT(lmFreeRunningTime);
		// cout << "Free Running time: " << bb.getFRT() << endl; */

		// cout << endl;

		vp.push_back(pu);
	}

	return vp;
}


vector<Process> SystemManager:: generateProcessInstances()
{
		vector<Process> vps;


		/*****************************
		 *   LOAD PROCESSES
		*****************************/

		int i;
		char* temp;

		/////////////////////////////////////////////////

		pugi::xml_document doc;
		pugi::xml_parse_result result = doc.load_file("./XML/application.xml");
		// cout << "Processes Load result: " << result.description() << endl;

		// cout<<endl<<"****** Processes ******"<<endl;

		//method 2: use object/node structure
		xml_node instancesPS2 = doc.child("instancesPS");
		xml_node processes = instancesPS2.child("process");

		for(i = 0; i < NPS; i++){

			Process pi;

			// Process Name
			string name = processes.child_value("name");
			pi.setName(name);
			// cout << "name: " << pi.getName() << endl;

			// Process id
			temp = (char*) processes.child_value("id");
			pi.setId(atoi(temp));
			// cout << "id: " << pi.getId() << endl;

			// Process Priority
			temp = (char*) processes.child_value("priority");
			pi.setPriority(atoi(temp));
			// cout << "priority: " << pi.getPriority() << endl;

			// Process Criticality
			temp = (char*) processes.child_value("criticality");
			pi.setCriticality(atoi(temp));
			// cout << "criticality: " << pi.getCriticality() << endl;

			// Process eqGate (HW size)
			xml_node eqGate = processes.child("eqGate");
			pi.setEqGate(eqGate.attribute("value").as_int());
			// cout << "eqGate: " << pi.getEqGate() << endl;

			// Process MemSize (SW Size)
			// cout << "MemSize: " << endl;
			xml_node memSize = processes.child("memSize");

			// cout << "CodeSize: " << endl;
			xml_node codeSize = memSize.child("codeSize");
			for (pugi::xml_node processorModel = codeSize.child("processorModel"); processorModel; processorModel = processorModel.next_sibling()) {
				// cout << "ProcessorModel: " << processorModel.attribute("name").as_string()<< " Code Size: " << processorModel.attribute("value").as_int()<<endl;
				pi.setCodeSize( processorModel.attribute("name").as_string(), processorModel.attribute("value").as_int() );
			}

			// cout << "DataSize: " << endl;
			xml_node dataSize = memSize.child("dataSize");
			for (pugi::xml_node processorModel = dataSize.child("processorModel"); processorModel; processorModel = processorModel.next_sibling()) {
				// cout << "Processor Model: " << processorModel.attribute("name").as_string()<< " Data Size: " << processorModel.attribute("value").as_int()<<endl;
				pi.setDataSize( processorModel.attribute("name").as_string(), processorModel.attribute("value").as_int() );
			}

			// Process Affinity

			// cout << "Affinity: " << endl;
			xml_node affinity = processes.child("affinity");
			for (pugi::xml_node processorType = affinity.child("processorType"); processorType; processorType = processorType.next_sibling()) {
				string processorType_name = processorType.attribute("name").as_string();
				float affinity_value = processorType.attribute("value").as_float();
				pi.setAffinity(processorType_name, affinity_value);
				// cout << "Processor Type: " << processorType_name << " Id: 0 " << " Affinity: " << affinity_value <<endl;
				//cout << "Processor Type: " << processorType_name << " Affinity: " << affinity_value <<endl;
			}

			// Process Concurrency

			// cout << "Concurrency: " << endl;
			xml_node concurrency = processes.child("concurrency");
			for (pugi::xml_node processId = concurrency.child("processId"); processId; processId = processId.next_sibling()) {
				unsigned int process_id_n = processId.attribute("id").as_int();
				float process_concurrency_value = processId.attribute("value").as_float();
				pi.setConcurrency(process_id_n, process_concurrency_value);
				// cout << "Process ID: " << i << "Concurrent Process id: " << process_id_n << " Comunication: " << process_concurrency_value << endl;
			}

			// Process Load

			// cout << "Load: " << endl;
			xml_node load = processes.child("load");
			for (pugi::xml_node processorId = load.child("processorId"); processorId; processorId = processorId.next_sibling()) {
				unsigned int processor_id_n = processorId.attribute("id").as_int();
				float process_load_value = processorId.attribute("value").as_float();
				pi.setLoad(processor_id_n, process_load_value);
				// cout << "Basic Block Id: " << processor_id_n << " Load: " << process_load_value << endl;
			}

			// Process Communication
			// TO DO

			vps.push_back(pi);

			// cout << endl;
			processes = processes.next_sibling();
		}

		doc.reset();
		return vps;
}

multimap<int, pair<string,int> > SystemManager:: mapping()
{
	multimap<int, pair<string,int> > allocation;

	// parsing xml file

	xml_document myDoc;
	xml_parse_result myResult = myDoc.load_file(MAPPING);
	xml_node mapping = myDoc.child("mapping");

	//mapping parameters

	xml_node_iterator mapping_it;

	for (mapping_it=mapping.begin(); mapping_it!=mapping.end(); ++mapping_it){

		xml_node_iterator child_mapping_it = mapping_it->begin();

		int processId = child_mapping_it->attribute("PSid").as_int();
		string processorName = child_mapping_it->attribute("PRname").as_string();
		int processorId = child_mapping_it->attribute("value").as_int();
		PAlloc = processorId;
		//cout<< " processID: "<< processId << " processorName: " << processorName << "processorID: " << processorId << endl;
		pair<string,int> pairPRID = make_pair(processorName, processorId);
		allocation.insert(make_pair(processId, pairPRID));
	}

	/*for (multimap<string, pair<string,int> >::iterator i = allocation.begin(); i != allocation.end(); ++i){
		cout << (*i).first;
		cout << i->second.first;
		cout << i->second.second << endl;
	}  */

	return allocation;
}


pair<string,int> SystemManager:: getPRIDbyProcess(int processId)
{
	pair<string,int> prid;
	for (multimap<int, pair<string,int> >::iterator it = allocation.begin(); it != allocation.end(); ++it){
		if ((*it).first == processId)
			prid = it->second;
	}
	return prid;
}


void SystemManager:: searchProcessor(pair<string,int> PRID, ProcessingUnit* &p)
{
	int n=VBB.size();
	for (int i=0; i<n; i++){
		if (VBB[i].getName() == PRID.first && VBB[i].getId() == PRID.second)
			p = &VP[i];
	}
}

void SystemManager:: searchProcess(int processId, Process* &ps)
{
	int n=VPS.size();
	for (int i=0; i<n; i++){
		if (VPS[i].getId() == processId)
			ps = &VPS[i];
	}
}


void SystemManager:: Increase(int processId)
{
	Process* ps;
	ProcessingUnit* p;
	pair<string,int> PRID = getPRIDbyProcess(processId);
	searchProcessor(PRID, p);
	int CC4S = p->getCC4S();
	float frequency = p->getFrequency();
	sc_time value((CC4S/(frequency*1000)), SC_MS); // Per avere i millisecondi per statement: MIPSC * (1/(freq*1000)) == MIPSC/(freq*1000) !!! // (CC4S/(frequency*1000)
	searchProcess(processId,  ps);
	ps->processTime = ps->processTime + value;
}


void SystemManager:: Profiling(int processId)
{

	Process* ps;
	searchProcess(processId, ps);
	ps->profiling = ps->profiling + 1;

}

sc_time SystemManager:: upSimTime(int processId)
{
	ProcessingUnit* p;
	pair<string,int> PRID = getPRIDbyProcess(processId);
	searchProcessor(PRID, p);
	int CC4S = p->getCC4S();
	float frequency = p->getFrequency();
	sc_time value((CC4S/(frequency*1000)), SC_MS);
	return value;
}

vector<Channel> SystemManager:: generateChannelInstances()
{
	vector<Channel> vch;

	// parsing xml file

	xml_document myDoc;
	xml_parse_result myResult = myDoc.load_file("./XML/application.xml");
	xml_node instancesLL = myDoc.child("instancesLL");

	//channel parameters

	xml_node_iterator seqChannel_it;

	for (seqChannel_it=instancesLL.begin(); seqChannel_it!=instancesLL.end(); ++seqChannel_it){

		xml_node_iterator channel_node_it = seqChannel_it->begin();

		Channel ch;

		char* temp;

		string name = channel_node_it->child_value();
		ch.setName(name);

		channel_node_it++;
		temp = (char*) channel_node_it->child_value();
		int  id = atoi(temp);
		ch.setId(id);

		channel_node_it++;
		temp = (char*) channel_node_it->child_value();
		int  w_id = atoi(temp);
		ch.setW_id(w_id);

		channel_node_it++;
		temp = (char*) channel_node_it->child_value();
		int  r_id = atoi(temp);
		ch.setR_id(r_id);

		channel_node_it++;
		temp = (char*) channel_node_it->child_value();
		int  width = atoi(temp);
		ch.setWidth(width);

		vch.push_back(ch);

	}

	return vch;
}

Link SystemManager:: generateLinkInstance()
{
	Link l;

	// parsing xml file

	xml_document myDoc;
	xml_parse_result myResult = myDoc.load_file("./XML/instancesTL.xml");
	xml_node instancesPL = myDoc.child("instancesPL");

	//link parameters

	xml_node_iterator seqLink_it;

	for (seqLink_it=instancesPL.begin(); seqLink_it!=instancesPL.end(); ++seqLink_it){

		xml_node_iterator link_node_it = seqLink_it->begin();

		char* temp;

		string name = link_node_it->child_value();
		l.setName(name);

		link_node_it++;
		temp = (char*) link_node_it->child_value();
		unsigned int id = atoi(temp);
		l.setId(id);

		link_node_it++;
		temp = (char*) link_node_it->child_value();
		unsigned int physical_width = atoi(temp);
		l.setPhysicalWidth(physical_width);

		link_node_it++;
		temp = (char*) link_node_it->child_value();
		float tc = (float) atof(temp);
		sc_time tcomm(tc, SC_MS);
		l.setTcomm(tcomm);

		link_node_it++;
		temp = (char*) link_node_it->child_value();
		float tac = (float) atof(temp);
		sc_time tacomm(tac, SC_MS);
		l.setTAcomm(tacomm);

		link_node_it++;
		temp = (char*) link_node_it->child_value();
		unsigned int bandwidth = atoi(temp);
		l.setBandwidth(bandwidth);
	}

	return l;
}


multimap<string, pair<string,int> > SystemManager::mappingLC()
{
	multimap<string, pair<string,int> > allocationLC;

			// parsing xml file

			/* xml_document myDoc;
			xml_parse_result myResult = myDoc.load_file("./XML/mapping.xml");
			xml_node mappingLC = myDoc.child("mappingLC");

			//mapping parameters

			xml_node_iterator mappingLC_it;

			for (mappingLC_it=mappingLC.begin(); mappingLC_it!=mappingLC.end(); ++mappingLC_it){

				xml_node_iterator child_mappingLC_it = mappingLC_it->begin();

				string channelName = child_mappingLC_it->attribute("CHname").as_string();
				string linkName = child_mappingLC_it->attribute("Lname").as_string();
				int linkId = child_mappingLC_it->attribute("value").as_int();
				pair<string,int> pairLID = make_pair(linkName, linkId);
				allocationLC.insert(make_pair(channelName, pairLID));
			}  */
			/*for (multimap<string, pair<string,int> >::iterator i = allocationLC.begin(); i != allocationLC.end(); ++i){
			cout << (*i).first;
			cout << i->second.first;
			cout << i->second.second << endl;
		}*/

	 return allocationLC;
}

vector<Channel> SystemManager:: getChannels()
{
	return this->VCH;
}

Link SystemManager:: getLink()
{
	return this->L;
}

multimap<string, pair<string,int> > SystemManager:: getAllocationLC()
{
	return this->allocationLC;
}


bool SystemManager::checkSPP(int processId)
{
	ProcessingUnit *p;
	bool checkSPP = false;
	for(multimap<int, pair<string,int> >::iterator i = allocation.begin(); i != allocation.end(); i++)
	{
		searchProcessor(i->second, p);
		if (((*i).first == processId)&&(p->getProcessorType() == "SPP"))
		{
			checkSPP = true;
			break;
		}
	}
	return checkSPP;
}

sc_time SystemManager:: getFRT(){
	return this->FRT;
}

void SystemManager:: setFRT(sc_time x){
	FRT = x;
}

float* SystemManager:: getFRL(){
	return this->FRL;
}

float* SystemManager:: loadEst(sc_time FRT_n){
	for(unsigned i =2; i<VPS.size(); i++){
			FRL[i] = ((VPS[i].processTime/VPS[i].profiling)/(FRT_n/VPS[i].profiling));
	}
	return FRL;
}

void SystemManager::deleteConcXml()
{
	int i;
	char* temp;
	int Id;

	multimap<int, pair<string,int> > Map = getAllocation();
	pugi::xml_document myDoc33;
	pugi::xml_parse_result myResult = myDoc33.load_file("./XML/application.xml");
	cout << endl << "XML Delete result: " << myResult.description() << endl;

	//method 2: use object/node structure
	pugi::xml_node instancesPS2 = myDoc33.child("instancesPS");
	xml_node processes = instancesPS2.child("process");

	for(i = 0; i < NPS; i++){
		// Process id
		temp = (char*) processes.child_value("id");
		Id = atoi(temp);
		//cout<<"ProcessId: "<< Id <<endl;

		// Process Load

		// cout << "Load: " << endl;
		xml_node load = processes.child("load");
		for (pugi::xml_node processorId = load.child("processorId"); processorId; processorId = processorId.next_sibling()) {
			unsigned int processor_id_n = processorId.attribute("id").as_int();
			float process_load_value = processorId.attribute("value").as_float();
			//cout << "Basic Block Id: " << processor_id_n << " Load: " << process_load_value << endl;
			for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
			{
				if( (*i).first ==Id  && processor_id_n == (*i).second.second)
				{
					//cout<<"ProcessorId: "<< atoi(processorId.attribute("id").value()) << " allocation: " << (*i).second.second << endl;
					load.remove_child(processorId);
				}
			}
		}

		// cout << "Load: " << endl;
		xml_node wcet = processes.child("WCET");
		for (pugi::xml_node processorId = wcet.child("processorId"); processorId; processorId = processorId.next_sibling()) {
			unsigned int processor_id_n = processorId.attribute("id").as_int();
			float process_load_value = processorId.attribute("value").as_float();
			//cout << "Basic Block Id: " << processor_id_n << " Load: " << process_load_value << endl;
			for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
			{
				if( (*i).first ==Id  && processor_id_n == (*i).second.second)
				{
					//cout<<"ProcessorId: "<< atoi(processorId.attribute("id").value()) << " allocation: " << (*i).second.second << endl;
					wcet.remove_child(processorId);
				}
			}
		}

		//////////// PERIOD /////////////////

		xml_node period = processes.child("Period");
		for (pugi::xml_node processorId = period.child("processorId"); processorId; processorId = processorId.next_sibling()) {
			unsigned int processor_id_n = processorId.attribute("id").as_int();
			float process_load_value = processorId.attribute("value").as_float();
			//cout << "Basic Block Id: " << processor_id_n << " Load: " << process_load_value << endl;
			for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
			{
				if( (*i).first ==Id  && processor_id_n == (*i).second.second)
				{
					//cout<<"ProcessorId: "<< atoi(processorId.attribute("id").value()) << " allocation: " << (*i).second.second << endl;
					period.remove_child(processorId);
				}
			}
		}

		//////////// DEADLINE /////////////////

		xml_node deadline = processes.child("Deadline");
		for (pugi::xml_node processorId = deadline.child("processorId"); processorId; processorId = processorId.next_sibling()) {
			unsigned int processor_id_n = processorId.attribute("id").as_int();
			float process_load_value = processorId.attribute("value").as_float();
			//cout << "Basic Block Id: " << processor_id_n << " Load: " << process_load_value << endl;
			for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
			{
				if( (*i).first ==Id  && processor_id_n == (*i).second.second)
				{
					//cout<<"ProcessorId: "<< atoi(processorId.attribute("id").value()) << " allocation: " << (*i).second.second << endl;
					deadline.remove_child(processorId);
				}
			}
		}

		processes = processes.next_sibling();

	}

	myDoc33.save_file("./XML/application.xml");
	cout<<endl;
}

void SystemManager:: updateXml()
{
	multimap<int, pair<string,int> > Map = getAllocation();
	pugi::xml_document myDoc;
	pugi::xml_parse_result myResult = myDoc.load_file("./XML/application.xml");
	cout << "XML result: " << myResult.description() << endl;

	///////////////////// LOAD ////////////////////////////

	//method 2: use object/node structure
	xml_node instancesPS = myDoc.child("instancesPS");

	for (xml_node_iterator seqProcess_it=instancesPS.begin(); seqProcess_it!=instancesPS.end(); ++seqProcess_it){
		int Id = atoi(seqProcess_it->child_value("id"));
		//cout<< "Process: " << Id;

		if(seqProcess_it->child("load")){
			xml_node load = seqProcess_it->child("load");

			xml_node processId = load.append_child("processorId");

			for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
			{
				if((*i).first == Id)
				{
					processId.append_attribute("id")=(*i).second.second;
					//cout<<" Processor: "<<(*i).second.second;
					processId.append_attribute("value")=FRL[Id];
					//cout<< " Value: " << FRL[Id];
				}
			}
		}else{
			xml_node load = seqProcess_it->append_child("load");

			xml_node processId = load.append_child("processorId");

			for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
			{
				if((*i).first == Id)
				{
					processId.append_attribute("id")=(*i).second.second;
					//cout<<" Processor: "<<(*i).second.second;
					processId.append_attribute("value")=FRL[Id];
					//cout<< " Value: " << FRL[Id];
				}
			}
		}
	}

	/////////////////////// WCET //////////////////////////////

	//method 2: use object/node structure
	xml_node instancesPS2 = myDoc.child("instancesPS");

	for (xml_node_iterator seqProcess_it=instancesPS2.begin(); seqProcess_it!=instancesPS2.end(); ++seqProcess_it){
		int Id = atoi(seqProcess_it->child_value("id"));
		//cout<< "Process: " << Id;

		if(seqProcess_it->child("WCET")){
			xml_node wcet = seqProcess_it->child("WCET");

			xml_node processId = wcet.append_child("processorId");

			for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
			{
				if((*i).first == Id)
				{
					processId.append_attribute("id")=(*i).second.second;
					//cout<<" Processor: "<<(*i).second.second;
					double wcet_task = (VPS[Id].processTime.to_seconds());
					processId.append_attribute("value")= (wcet_task/VPS[Id].profiling)*1000000.0;
					//cout<< " Value: " << FRL[Id];
				}
			}
		}else{
			xml_node wcet = seqProcess_it->append_child("WCET");

			xml_node processId = wcet.append_child("processorId");

			for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
			{
				if((*i).first == Id)
				{
					processId.append_attribute("id")=(*i).second.second;
					//cout<<" Processor: "<<(*i).second.second;
					double wcet_task = (VPS[Id].processTime.to_seconds());
					processId.append_attribute("value")= (wcet_task/VPS[Id].profiling)*1000000.0;
					//cout<< " Value: " << FRL[Id];
				}
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////

	/////////////////////// PERIOD //////////////////////////////

	//method 2: use object/node structure
	xml_node instancesPS3 = myDoc.child("instancesPS");

	for (xml_node_iterator seqProcess_it=instancesPS3.begin(); seqProcess_it!=instancesPS2.end(); ++seqProcess_it){
		int Id = atoi(seqProcess_it->child_value("id"));
		//cout<< "Process: " << Id;

		if(seqProcess_it->child("Period")){
			xml_node period = seqProcess_it->child("Period");

			xml_node processId = period.append_child("processorId");

			for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
			{
				if((*i).first == Id)
				{
					processId.append_attribute("id")=(*i).second.second;
					//cout<<" Processor: "<<(*i).second.second;
					double period_value = (FRT.to_seconds());
					processId.append_attribute("value")= (period_value/VPS[Id].profiling)*1000000.0;
					//cout<< " Value: " << FRL[Id];
				}
			}
		}else{
			xml_node period = seqProcess_it->append_child("Period");

			xml_node processId = period.append_child("processorId");

			for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
			{
				if((*i).first == Id)
				{
					processId.append_attribute("id")=(*i).second.second;
					//cout<<" Processor: "<<(*i).second.second;
					double period_value = (FRT.to_seconds());
					processId.append_attribute("value")= (period_value/VPS[Id].profiling)*1000000.0;
					//cout<< " Value: " << FRL[Id];
				}
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////

	/////////////////////// DEADLINE //////////////////////////////

	//method 2: use object/node structure
	xml_node instancesPS4 = myDoc.child("instancesPS");

	for (xml_node_iterator seqProcess_it=instancesPS4.begin(); seqProcess_it!=instancesPS2.end(); ++seqProcess_it){
		int Id = atoi(seqProcess_it->child_value("id"));
		//cout<< "Process: " << Id;

		if(seqProcess_it->child("Deadline")){
			xml_node deadline = seqProcess_it->child("Deadline");

			xml_node processId = deadline.append_child("processorId");

			for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
			{
				if((*i).first == Id)
				{
					processId.append_attribute("id")=(*i).second.second;
					//cout<<" Processor: "<<(*i).second.second;
					double deadline_value = (FRT.to_seconds());
					processId.append_attribute("value")= (deadline_value/VPS[Id].profiling)*1000000.0;
					//cout<< " Value: " << FRL[Id];
				}
			}
		}else{
			xml_node deadline = seqProcess_it->append_child("Deadline");

			xml_node processId = deadline.append_child("processorId");

			for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
			{
				if((*i).first == Id)
				{
					processId.append_attribute("id")=(*i).second.second;
					//cout<<" Processor: "<<(*i).second.second;
					double deadline_value = (FRT.to_seconds());
					processId.append_attribute("value")= (deadline_value/VPS[Id].profiling)*1000000.0;
					//cout<< " Value: " << FRL[Id];
				}
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////

	cout << "Saving result: " << myDoc.save_file("./XML/application.xml") << endl;
	myDoc.reset();
	cout<<endl;

	pugi::xml_document myDoc2;
	pugi::xml_parse_result myResult2 = myDoc2.load_file("./XML/instancesTL.xml");
	cout << "XML result: " << myResult2.description() << endl;

	//method 2: use object/node structure
	xml_node instancesBB = myDoc2.child("instancesBB");

	for (xml_node_iterator seqBB_it=instancesBB.begin(); seqBB_it!=instancesBB.end(); ++seqBB_it){

		int Id = atoi(seqBB_it->child_value("id"));

		if(seqBB_it->child("loadEstimation")){

			xml_node frl = seqBB_it->child("loadEstimation");

			xml_node frl_node = frl.child("FreeRunningTime");

			if(PAlloc == Id)
			{
				//xml_node frl_node = processorId.append_child("FreeRunningTime");
				double xx = FRT.to_seconds();
				//char *temp = (char *)xx;
				//cout<<"FRL: " << temp << endl;
				//cout<<"FRL: " << xx << endl;
				//cout<<"FRL: " << to_string(xx) << endl;
				//cout<<" Processor: "<<PAlloc;
				frl_node.attribute("value")=xx;
			}
		}
	}

	cout << "Saving result: " << myDoc2.save_file("./XML/instancesTL.xml") << endl;
	myDoc2.reset();
	cout<<endl;

}

int SystemManager:: getmaxBBSoftware(){
	return this->maxBBSoftware;
}
