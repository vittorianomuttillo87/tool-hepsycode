//============================================================================
// Name        : cpp.cpp
// Author      : Vittoriano Muttillo, Luigi Pomante
// Version     :
// Copyright   : Your copyright notice
// Description : Define
//============================================================================

#ifndef __DEFINE__
#define __DEFINE__

// Valori costanti di riferimento usati all'interno del programma

#define NPS 10
#define NCH 15
//#define MAPPING "./XML/mapping_MPU8051_0.xml"
//#define MAPPING "./XML/mapping_MPU8051_1.xml"
//#define MAPPING "./XML/mapping_PIC24_2.xml"
//#define MAPPING "./XML/mapping_PIC24_3.xml"
//#define MAPPING "./XML/mapping_MPULEON3_4.xml"
#define MAPPING "./XML/mapping_MPULEON3_5.xml"

#endif
