
#include "mainsystem.h"

//S(gcde)
void mainsystem::gcd_evaluation()
{
	// datatype for channels
	gcde_parameters gcde_p;
	gcde_results gcde_r;

	// local variables
	sc_uint<8>  sample1;
	sc_uint<8>  sample2;

	while(1)
	{

		// read parameters from channel
		S(9) gcde_p=gcde_parameters_channel->read();

		// fill local variables
		S(9) sample1=gcde_p.sample1;
		S(9) sample2=gcde_p.sample2;

		S(9) while(sample1!=sample2)
		{S(9)
			S(9) if (sample1>sample2)
			{S(9)
				S(9) sample1=sample1-sample2;
			S(9)}
			else
			{S(9)
				S(9) sample2= sample2-sample1;
			S(9)}
		S(9)}

		// fill datatype
		S(9) gcde_r.result=sample1;

		// send results to channel
		S(9) gcde_results_channel->write(gcde_r);

		P(9)
	}
}

/*****************************************************************************/




