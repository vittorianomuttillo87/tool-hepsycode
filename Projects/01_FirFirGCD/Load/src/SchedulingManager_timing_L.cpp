#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>

#include "SchedulingManager_timing_L.h"
#include "SystemManager_timing_L.h"

extern SystemManager *pSystemManager;

// Round Robin

void SchedulingManager::scheduler_8051_0()
{
	//scheduler_xy_FP("MPU8051", 0);
	//scheduler_xy_RR("MPU8051", 0);
	scheduler_xy_NO_OH("MPU8051", 0);
}

void SchedulingManager::scheduler_8051_1()
{
	//scheduler_xy_FP("MPU8051", 0);
	//scheduler_xy_RR("MPU8051", 0);
	scheduler_xy_NO_OH("MPU8051", 1);
}

void SchedulingManager::scheduler_dspic_0()
{
	//scheduler_xy_FP("PIC24", 2);
	//scheduler_xy_RR("PIC24", 2);
	scheduler_xy_NO_OH("PIC24", 2);
}

void SchedulingManager::scheduler_dspic_1()
{
	//scheduler_xy_FP("PIC24", 2);
	//scheduler_xy_RR("PIC24", 2);
	scheduler_xy_NO_OH("PIC24", 3);
}

void SchedulingManager::scheduler_leon3_0()
{
	//scheduler_xy_FP("PIC24", 2);
	//scheduler_xy_RR("PIC24", 2);
	scheduler_xy_NO_OH("MPULEON3", 4);
}

void SchedulingManager::scheduler_leon3_1()
{
	//scheduler_xy_FP("PIC24", 2);
	//scheduler_xy_RR("PIC24", 2);
	scheduler_xy_NO_OH("MPULEON3", 5);
}

////////////////////////////////////////////////////////////////

//Fixed Priority

void SchedulingManager::scheduler_xy_FP(string pname, int pinstance)
{
	vector<Process> vps = pSystemManager->getVPS();
	multimap<int, pair<string,int> > Map = pSystemManager->getAllocation();
	Process ps;
	ProcessingUnit p;
	unsigned int old_id = 0;
	unsigned int pri_max = 0;
	unsigned int ps_max= 0;

	// no scheduling needed for the selected processor instance
	bool exit=true;
	for(unsigned int j=2; j<vps.size(); j++)
	{
		for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i!=Map.end(); ++i)
		{
			if (((*i).first == vps[j].id)&&(i->second.first == pname)&&(i->second.second == pinstance))
				exit=false;	// per controllare se il processore esiste e ha allocato dei processi, lo scheduler viene attivato
		}
	}


	if (exit==true)
	{
		cout << "exit0: " << pname << "-" << pinstance << endl;
		return;
	}

	unsigned int j=2;

	do
	{
		//bool quit = false;
		pri_max = 0;
		ps_max= 0;
		//while(quit==false)
		//{
			for(j=2; j<pSystemManager->getVPS().size(); j++)
			{
				for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
				{
					if(((*i).first == vps[j].id) && ((i->second.first == pname) && (i->second.second == pinstance)) && ready[vps[j].id]==true &&  vps[j].priority > pri_max)
					{
						// Identifico quello a priorita' piu' alta tra i ready
						ps = vps[j];
						pri_max = ps.priority;
						ps_max = ps.id;
						//quit = true;
						break;
					}
				}
			}
			//j++;
			//if (j>=vps.size()) j=2;
		//}

		pair<string,int> PRID = pSystemManager->getPRIDbyProcess(ps.id);
		vector<ProcessingUnit> vp = pSystemManager->getVP();
		for(unsigned int j=0; j<vp.size(); j++)
		{
			if ((PRID.first == pSystemManager->getVBB()[j].getName()) && (PRID.second == pSystemManager->getVBB()[j].getId()))
			{
				p=vp[j];
				break;
			}
		}

		//wait(0.001*sc_time(p.getOverheadCS(), SC_MS));
		//sched_oh[pinstance]+=(0.001*(sc_time(p.getOverheadCS(), SC_MS)));

		wait(0.001*p.getOverheadCS());
		sched_oh[pinstance]+=(0.001*p.getOverheadCS());

		if (ps_max!=0)
		//if (ready[ps_max]==true && ps_max!=0)
		{
			if (ps_max!=old_id)
			{
				old_id=ps_max;
				//wait(0.999*sc_time(p.getOverheadCS(), SC_MS));
				//sched_oh[pinstance]+=(0.999*(sc_time(p.getOverheadCS(), SC_MS)));
				wait(0.999*p.getOverheadCS());
				sched_oh[pinstance]+=(0.999*p.getOverheadCS());
			}
			schedule[ps_max].notify(SC_ZERO_TIME);
			wait(release[ps_max]);
		}
		pri_max=0;
		ps_max=0;
	}while(1);
}

//Round Robin

void SchedulingManager::scheduler_xy_RR(string pname, int pinstance)
{
	vector<Process> vps = pSystemManager->getVPS();
	multimap<int, pair<string,int> > Map = pSystemManager->getAllocation();
	Process ps;
	ProcessingUnit p;
	BasicBlock b;
	unsigned int old_id=0;

	// no scheduling needed for the selected processor instance
	bool exit=true;
	for(unsigned int j=2; j<vps.size(); j++)
	{
		for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i!=Map.end(); ++i)
		{
			if (((*i).first == vps[j].id)&&(i->second.first == pname)&&(i->second.second == pinstance))
				exit=false;	// per controllare se il processore esiste e ha allocato dei processi, lo scheduler viene attivato
		}
	}


	if (exit==true)
	{
		cout << "exit0: " << pname << "-" << pinstance << endl;
		return;
	}

	unsigned int j=2;

	do
	{
		bool quit = false;
		while(quit==false)
		{
			for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
			{
				if(((*i).first == vps[j].id) && ((i->second.first == pname) && (i->second.second == pinstance)))
				{
					ps = vps[j];
					quit = true;
					break;
				}
			}
			j++;
			if (j>=vps.size()) j=2;
		}

		pair<string,int> PRID = pSystemManager->getPRIDbyProcess(ps.id);
		vector<ProcessingUnit> vp = pSystemManager->getVP();
		for(unsigned int j=0; j<vp.size(); j++) // vp.size()
		{
			if ((PRID.first == pSystemManager->getVBB()[j].getName()) && (PRID.second == pSystemManager->getVBB()[j].getId()))
			{
				p=vp[j];
				break;
			}
		}

		wait(0.001*p.getOverheadCS());
		sched_oh[pinstance]+=(0.001*p.getOverheadCS());

		if (ready[ps.id]==true)
		{
			if (ps.id!=old_id)
			{
				old_id=ps.id;
				wait(0.999*p.getOverheadCS());
				sched_oh[pinstance]+=(0.999*p.getOverheadCS());
			}
			schedule[ps.id].notify(SC_ZERO_TIME);
			wait(release[ps.id]);
		}

	}while(1);
}

// NO Overhead

void SchedulingManager::scheduler_xy_NO_OH(string pname, int pinstance)
{
	vector<Process> vps = pSystemManager->getVPS();
	multimap<int, pair<string,int> > Map = pSystemManager->getAllocation();
	Process ps;
	ProcessingUnit p;
	//unsigned int old_id=0;

	// no scheduling needed for the selected processor instance
	bool exit=true;
	for(unsigned int j=2; j<vps.size(); j++)
	{
		for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i!=Map.end(); ++i)
		{
			if (((*i).first == vps[j].id)&&(i->second.first == pname)&&(i->second.second == pinstance))
				exit=false;	// per controllare se il processore esiste e ha allocato dei processi, lo scheduler viene attivato
		}
	}


	if (exit==true)
	{
		cout << "exit0: " << pname << "-" << pinstance << endl;
		return;
	}

	unsigned int j=2;

	do
	{
		bool quit = false;
		while(quit==false)
		{
			for(multimap<int, pair<string,int> >::iterator i = Map.begin(); i != Map.end(); i++)
			{
				if(((*i).first == vps[j].id) && ((i->second.first == pname) && (i->second.second == pinstance)))
				{
					ps = vps[j];
					quit = true;
					break;
				}
			}
			j++;
			if (j>=vps.size()) j=2;
		}

		pair<string,int> PRID = pSystemManager->getPRIDbyProcess(ps.id);
		vector<ProcessingUnit> vp = pSystemManager->getVP();
		for(unsigned int j=0; j<vp.size(); j++)
		{
			if ((PRID.first == pSystemManager->getVBB()[j].getName()) && (PRID.second == pSystemManager->getVBB()[j].getId()))
			{
				p=vp[j];
				break;
			}
		}

		wait(0.001*p.getOverheadCS());
		sched_oh[pinstance]+=(0.001*p.getOverheadCS());

		//wait(SC_ZERO_TIME);

		//wait(0.001*sc_time(p.getOverheadCS(), SC_MS));
		//sched_oh[pinstance]+=(0.001*(sc_time(p.getOverheadCS(), SC_MS)));

		if (ready[ps.id]==true)
		{
			//if (ps.id!=old_id)
			//{
			//	old_id=ps.id;
			// LP
			//	wait(0.999*sc_time(p.getOverheadCS(), SC_MS));
			//	sched_oh[pinstance]+=(0.999*p.getOverheadCS());
			//}
			schedule[ps.id].notify(SC_ZERO_TIME);
			wait(release[ps.id]);
		}

	}while(1);
}
