/*
 * tl.h
 *
 *  Created on: 26 set 2016
 *      Author: daniele
 */

#ifndef TL_H_
#define TL_H_

#include <systemc.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <map>
#include <set>

using namespace std;

class ProcessingUnit
{
    public:

		string name;
		unsigned int id;
        string processorType;
        float cost;
        string ISA;
        float frequency; // in MHz
        unsigned int CC4S;
        sc_time overheadCS;

    public:
        unsigned int getId();
        void setId(unsigned int c);

        string getName();
        void setName(string x);

        string getProcessorType();
        void setProcessorType(string x);

        float getCost();
        void setCost(float x);

        string getISA();
        void setISA(string x);

        float getFrequency();
        void setFrequency(float x);

        int getCC4S();
        void setCC4S(int x);

        sc_time getOverheadCS();
        void setOverheadCS(sc_time x);

};

class Memory
{
	private:
		unsigned int codeSize;
		unsigned int dataSize;

	public:
		//Memory();
		Memory(unsigned int code, unsigned int data);

		unsigned int getCodeSize();
		void setCodeSize(unsigned int c);

		unsigned int getDataSize();
		void setDataSize(unsigned int c);
};

class Process
{
	public:
        string name;
        unsigned int id;
        unsigned int priority;
        unsigned int criticality;
        float eqGate;
     	map <string, unsigned int> codeSizeMap;
     	map <string, unsigned int> dataSizeMap;
        //Memory mem;
        map <string,float> affinity;
        map <int,float> concurrency;
        map <int,float> load;
        unsigned int profiling;
        sc_time processTime;

	public:
        Process();

        string getName();
        void setName(string x);

        int getId();
        void setId(int x);

        int getPriority();
        void setPriority(int x);

        int getCriticality();
        void setCriticality(int x);

        float getEqGate();
        void setEqGate(float g);

        void setMemSize(Memory m);
        void setCodeSize(string procName, unsigned int value);
        void setDataSize(string procName, unsigned int value);

        float getAffinityByName(string processorName);
        void setAffinity(string processorName, float affinity);

        void setConcurrency(int procId, float conc);

        void setLoad (int procId, float value);

        int getProfiling();
        sc_time getProcessTime();
};

class BasicBlock{

private:
	string name;
	unsigned int id;
	string type;
	int idPU;
	ProcessingUnit processingUnit;
 	//Memory memSize;
 	//communicationUnit
	unsigned int codeSize;
	unsigned int dataSize;
	unsigned int eqG;
 	float FRT;

public:
 	//BasicBlock();

	//BasicBlock (unsigned int id, string type, ProcessingUnit pu, float frt);

	string getName();
	void setName(string x);

	int getId();
	void setId(int x);

	string getType();
	void setType(string x);

	int getIdPU();
	void setIdPU(int x);

	ProcessingUnit getProcessor();
	void setProcessor(ProcessingUnit pu);

	//Memory getMemSize();
	//void setMem(Memory m);

    unsigned int getCodeSize();
    void setCodeSize(unsigned int c);

    unsigned int getDataSize();
    void setDataSize(unsigned int c);


    unsigned int getEqG();
    void setEqG(unsigned int c);

	float getFRT();
	void setFRT(float x);


};

class Link
{
	public:
		string name;
		unsigned int id;
		unsigned int physical_width; // bit
		sc_time tcomm; // LP: (bandwidth / phisycal_widht = 1/sec=hz (inverto)) ( per 1000)  (non sforare i 5 ms)
		sc_time tacomm; // LP: tcomm * K (es:K=1)
		unsigned int bandwidth; // bandwidth in bit/s

	public:
		string getName();
		void setName(string x);

		int getId();
		void setId(int x);

		int getPhysicalWidth();
		void setPhysicalWidth(int x);

		sc_time getTcomm();
		void setTcomm(sc_time x);

		sc_time getTAcomm();
		void setTAcomm(sc_time x);

		int getBandwidth();
		void setBandwidth(int x);

};

class Channel
{
	public:
		string name;
		unsigned int id;
		unsigned int w_id;
		unsigned int r_id;
		unsigned int width;
		unsigned int num;

	public:
		string getName();
		void setName(string x);

		int getId();
		void setId(int x);

		int getW_id();
		void setW_id(int x);

		int getR_id();
		void setR_id(int x);

		int getWidth();
		void setWidth(int x);

		int getNum();
		void setNum(int x);

};


#endif /* TL_H_ */
