/*
 * tl.cpp
 *
 *  Created on: 26 set 2016
 *      Author: daniele
 */

#include <systemc.h>
#include "tl.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>

using namespace std;

/*
 *  Processors methods
 */

unsigned int ProcessingUnit::getId(){
	return this->id;
}

void ProcessingUnit::setId(unsigned int c){
	this->id = c;
}

string ProcessingUnit:: getName()
{
	return this->name;
}

void ProcessingUnit:: setName(string x)
{
	name = x;
}

string ProcessingUnit:: getProcessorType()
{
	return this->processorType;
}

void ProcessingUnit:: setProcessorType(string x)
{
	processorType = x;
}

float ProcessingUnit:: getCost()
{
	return this->cost;
}

void ProcessingUnit:: setCost(float x)
{
	cost = x;
}

string ProcessingUnit:: getISA()
{
	return this->ISA;
}

void ProcessingUnit:: setISA(string x)
{
	ISA = x;
}

float ProcessingUnit:: getFrequency()
{
	return this->frequency;
}

void ProcessingUnit:: setFrequency(float x)
{
	frequency = x;
}

int ProcessingUnit:: getCC4S()
{
	return this->CC4S;
}

void ProcessingUnit:: setCC4S(int x)
{
	CC4S = x;
}

sc_time ProcessingUnit:: getOverheadCS()
{
	return this->overheadCS;
}

void ProcessingUnit:: setOverheadCS(sc_time x)
{
	overheadCS = x;
}

// BasicBlock methods

/*BasicBlock::BasicBlock(unsigned int id, string type, ProcessingUnit pu, float frt){
	this->id = id;
	this->type = type;
	this->processingUnit = pu;
	this->FRT = frt;
}*/

string BasicBlock:: getName()
{
	return this->name;
}

void BasicBlock:: setName(string x)
{
	name = x;
}

int BasicBlock:: getId()
{
	return this->id;
}

void BasicBlock:: setId(int x)
{
	id = x;
}

string BasicBlock:: getType()
{
	return type;
}


void BasicBlock:: setType(string x)
{
	type = x;
}

int BasicBlock:: getIdPU()
{
	return this->idPU;
}

void BasicBlock:: setIdPU(int x)
{
	idPU = x;
}

void BasicBlock:: setProcessor(ProcessingUnit p)
{
	processingUnit = p;
}

/*void BasicBlock:: setMem(Memory m)
{
	memSize = m;
}*/

unsigned int BasicBlock::getCodeSize(){
	return this->codeSize;
}

void BasicBlock::setCodeSize(unsigned int c){
	this->codeSize = c;
}

unsigned int BasicBlock::getDataSize(){
	return this->dataSize;
}

void BasicBlock::setDataSize(unsigned int d){
	this->dataSize = d;
}

unsigned int BasicBlock::getEqG(){
	return this->eqG;
}

void BasicBlock::setEqG(unsigned int e){
	this->eqG = e;
}

void BasicBlock:: setFRT(float x){
	FRT = x;
}

float BasicBlock:: getFRT(){
	return FRT;
}

// process methods

Process::Process(){
	id = 0;
	eqGate = 0;
	criticality = 0;
	profiling = 0;
	processTime = sc_time(0, SC_MS);
	priority = 0;
}

string Process:: getName()
{
	return this->name;
}

void Process:: setName(string x)
{
	name = x;
}

int Process:: getId()
{
	return this->id;
}

void Process:: setId(int x)
{
	id = x;
}

int Process:: getPriority()
{
	return this->priority;
}

void Process:: setPriority(int x)
{
	priority = x;
}

int Process:: getCriticality()
{
	return this->criticality;
}

void Process:: setCriticality(int x)
{
	criticality = x;
}

float Process:: getEqGate()
{
	return this->eqGate;
}

void Process:: setEqGate(float g)
{
	eqGate = g;
}

void Process::setCodeSize(string p, unsigned int mem){
	map <string, unsigned int>::iterator end = this->codeSizeMap.end();
	pair<string, unsigned int> item (p, mem);
	this->codeSizeMap.insert(end, item);
}

void Process::setDataSize(string p, unsigned int mem){
	map <string, unsigned int>::iterator end = this->dataSizeMap.end();
	pair<string, unsigned int> item (p, mem);
	this->dataSizeMap.insert(end, item);
}

/*void Process::setMemSize(Memory m){
	this->mem = m;
}*/

float Process::getAffinityByName (string processorName){
	map<string, float>::iterator it = this->affinity.find(processorName);
	if ( it != this->affinity.end() )
		return it->second;
	else
		return 0;
}

void Process:: setAffinity(string x, float y)
{
	map <string, float>::iterator end = this->affinity.end();
	pair<string, float> item (x, y);
	this->affinity.insert(end, item);
}

void Process:: setConcurrency(int p, float c)
{
	map <int, float>::iterator end = this->concurrency.end();
	pair<int, float> item (p, c);
	this->concurrency.insert(end, item);
}

void Process::setLoad(int p, float value){
	map <int, float>::iterator end = this->load.end();
	pair<int, float> item (p, value);
	this->load.insert(end, item);
}


int Process:: getProfiling()
{
	return this->profiling;
}

sc_time Process:: getProcessTime()
{
	return this->processTime;
}


// memory methods

Memory::Memory(unsigned int c, unsigned int d){
	this->codeSize = c;
	this->dataSize = d;
}


unsigned int Memory::getCodeSize(){
	return this->codeSize;
}

void Memory::setCodeSize(unsigned int c){
	this->codeSize = c;
}

unsigned int Memory::getDataSize(){
	return this->dataSize;
}

void Memory::setDataSize(unsigned int d){
	this->dataSize = d;
}

// link methods

int Link:: getId()
{
	return this->id;
}

string Link:: getName()
{
	return this->name;
}

int Link:: getPhysicalWidth()
{
	return this->physical_width;
}

sc_time Link:: getTcomm()
{
	return this->tcomm;
}

sc_time Link:: getTAcomm()
{
	return this->tacomm;
}

int Link:: getBandwidth()
{
	return this->bandwidth;
}

void Link:: setId(int x)
{
	id = x;
}

void Link:: setName(string x)
{
	name = x;
}

void Link:: setPhysicalWidth(int x)
{
	physical_width = x;
}

void Link:: setTcomm(sc_time x)
{
	tcomm = x;
}

void Link:: setTAcomm(sc_time x)
{
	tacomm = x;
}

void Link:: setBandwidth(int x)
{
	bandwidth = x;
}

// channel methods

string Channel:: getName()
{
	return this->name;
}

int Channel:: getId()
{
	return this->id;
}

int Channel:: getWidth()
{
	return this->width;
}

int Channel:: getW_id()
{
	return this->w_id;
}

int Channel:: getR_id()
{
	return this->r_id;
}

int Channel:: getNum()
{
	return this->num;
}

void Channel:: setName(string x)
{
	name = x;
}

void Channel:: setId(int x)
{
	id = x;
}

void Channel:: setWidth(int x)
{
	width = x;
}

void Channel:: setW_id(int x)
{
	w_id = x;
}

void Channel:: setR_id(int x)
{
	r_id = x;
}

void Channel:: setNum(int x)
{
	num = x;
}


