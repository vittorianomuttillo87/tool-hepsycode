#include <systemc.h>
#include "stim_gen.h"

void stim_gen::main()
{
	sc_uint<8> value1=1;
	sc_uint<8> value2=1;
	unsigned int i=1;

	while(1)
	{
		// Sends out first value A
		wait(1, SC_MS);
		stim1_channel_port->write(value1);
		cout << "Stimulus1-"<<i<<": \t" << value1 << "\t at time \t" << sc_time_stamp() << endl;

		// Sends out value B
		wait(1, SC_MS);
		stim2_channel_port->write(value2);
		cout << "Stimulus2-"<<i<<": \t" << value2 << "\t at time \t" << sc_time_stamp() << endl;

		// Change next out values
		// Change next out values
		value1=value1+value2;
		value2=(value2*value1)+1;

		// Check for stop
		if(i >= 10) return;
		else i++;
	}

}
