/*****************************************************************************/

#include "mainsystem.h"

//S(gcdm)
void mainsystem::gcd_main()
{

	// datatype for channels
	gcde_parameters gcde_p;
	gcde_results gcde_r;

	// local variables
	sc_uint<8> sample1;
	sc_uint<8> sample2;
	sc_uint<8> result;
	bool err;

	// init
	S(8) err=false;

	S(8) while(1)
	{S(8)

		// main functionality

		// Main input from channel

		S(8) sample1=result8_channel->read();
		S(8) if (sample1==0) err=true;

		S(8) sample2=result16_channel->read();
		S(8) if (sample2==0) err=true;

		//cout<<"sample1: "<<sample1_tmp<<" sample2: "<<sample2_tmp<<endl;

		S(8) if (err==false)
		{S(8)  
			// fill datatype
			S(8) gcde_p.sample1=sample1;
			S(8) gcde_p.sample2=sample2;

			// send parameters and receive results
			S(8) gcde_parameters_channel->write(gcde_p);
			S(8) gcde_r=gcde_results_channel->read();

			// fill local variables
			S(8) result=gcde_r.result;
		S(8)}	
		else
		{S(8) 
			S(8) result=0;
			S(8) err=false;
		S(8)}

		S(8) result_channel_port->write(result);

		//cout << "GCD: \t\t" << result << "\t at time \t" << sc_time_stamp() << endl;

		P(8)
	}
}
