//============================================================================
// Name        : cpp.cpp
// Author      : Vittoriano Muttillo, Luigi Pomante
// Version     :
// Copyright   : Your copyright notice
// Description : Define
//============================================================================

#ifndef __DEFINE__
#define __DEFINE__

// Valori costanti di riferimento usati all'interno del programma

// FirFirGCD

#define NPS 10
#define NCH 15
#define MAPPING "./XML/mapping.xml"

// HepsyRT

//#define NPS 6
//#define NCH 7
//#define MAPPING "./XML/mapping_PIC24_2.xml"

#endif
