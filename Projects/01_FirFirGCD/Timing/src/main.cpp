/*
 * main.cpp
 *
 *  Created on: 30 set 2016
 *      Author: daniele
 */

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <set>

#include <systemc.h>
#include "sc_csp_channel_ifs.h"
#include "datatype.h"
#include "stim_gen.h"
#include "mainsystem.h"
#include "display.h"
#include <string.h>

#include "sc_csp_channel_timing.h"
#include "SchedulingManager_timing.h"
#include "SystemManager_timing.h"
#include "tl.h"

using namespace std;

SystemManager		*pSystemManager = new SystemManager();
SchedulingManager	*pSchedulingManager = new SchedulingManager("SchedulingManager");;

int sc_main(int a, char* b[])
{
	/////////////////////////////////////////////////////////////////////////////////////////
	// Testbech and System
	/////////////////////////////////////////////////////////////////////////////////////////

	// Channels for the connection to the main system

	sc_csp_channel< sc_uint<8> >   stim1_channel(8, 0, 2, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);
	sc_csp_channel< sc_uint<8> >   stim2_channel(8, 0, 5, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);
	sc_csp_channel< sc_uint<8> >   result_channel(8, 8, 1, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);

	// Instantiation and connection of testbench and system

	stim_gen mystimgen("mystimgen");
	mystimgen.stim1_channel_port(stim1_channel);
	mystimgen.stim2_channel_port(stim2_channel);

	mainsystem mysystem("mysystem");
	mysystem.stim1_channel_port(stim1_channel);
	mysystem.stim2_channel_port(stim2_channel);
	mysystem.result_channel_port(result_channel);

	display mydisplay("mydisplay");
	mydisplay.result_channel_port(result_channel);

	/////////////////////////////////////////////////////////////////////////////////////////
	// Simulation management
	/////////////////////////////////////////////////////////////////////////////////////////

	// Time info
	sc_time end, tot=sc_time(0, SC_MS);

	// Start simulation
	sc_start();

	//  Total simulated time
	end=sc_time_stamp();

	#if _WIN32
		system("pause");
	#endif

	/////////////////////////////////////////////////////////////////////////////////////////
	// Report
	/////////////////////////////////////////////////////////////////////////////////////////

	//// TIMING DATA

	cout << endl << "FINAL SIMULATED TIME: " << end.to_seconds() << endl;

	cout << endl << "PROCESS TIME PROFILING" << endl;

	cout << endl << "Average NET TIME for each process:" << endl << endl;

	for(unsigned i =2; i<pSystemManager->getVPS().size(); i++){
		cout << pSystemManager->getVPS()[i].id << "-" << pSystemManager->getVPS()[i].name << " " << (pSystemManager->getVPS()[i].processTime/pSystemManager->getVPS()[i].profiling).to_seconds() << endl;
		cout << endl;
		tot+=pSystemManager->getVPS()[i].processTime;
	}
	cout << "Total NET TIME for all the processes:" << tot.to_seconds() << endl;

	/*cout << endl << "Schedulers Overhead" << endl;
	cout << pSchedulingManager->sched_oh[0].to_seconds() << endl;
	cout << pSchedulingManager->sched_oh[1].to_seconds() << endl;
	cout << pSchedulingManager->sched_oh[2].to_seconds() << endl;  */

	cout << endl << "Schedulers Overhead" << endl;
	for(int i = 0; i< pSystemManager->getmaxBBSoftware(); i++){
		cout << pSchedulingManager->sched_oh[i] << endl;
	}

	// Communications

	cout << endl << "COMMUNICATION PROFILING" << endl << endl;

	cout << "WRITER\t\t\tREADER\t\t\tCOM\t\t\tBIT\t\t\tNUM*BIT\t\t\tTIME (sec)" << endl <<endl;
	stim1_channel.report_profiling();
	stim2_channel.report_profiling();
	result_channel.report_profiling();
	mysystem.result8_channel->report_profiling();
	mysystem.result16_channel->report_profiling();
	mysystem.fir8e_parameters_channel->report_profiling();
	mysystem.fir8e_results_channel->report_profiling();
	mysystem.fir8s_parameters_channel->report_profiling();
	mysystem.fir8s_results_channel->report_profiling();
	mysystem.fir16e_parameters_channel->report_profiling();
	mysystem.fir16e_results_channel->report_profiling();
	mysystem.fir16s_parameters_channel->report_profiling();
	mysystem.fir16s_results_channel->report_profiling();
	mysystem.gcde_parameters_channel->report_profiling();
	mysystem.gcde_results_channel->report_profiling();
	cout << endl;

	#if _WIN32
		system("pause");
	#endif

	return 0;

}


