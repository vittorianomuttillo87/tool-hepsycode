#ifndef __DISPLAY__

#define __DISPLAY__

#include "sc_csp_channel_ifs.h"
#include "sc_csp_channel_timing.h"

SC_MODULE(display)
{
	sc_port< sc_csp_channel_in_if< sc_uint<8> > > result_channel_port;


	SC_CTOR(display)
    {
      SC_THREAD(main);
    }

  void main();
};

#endif
