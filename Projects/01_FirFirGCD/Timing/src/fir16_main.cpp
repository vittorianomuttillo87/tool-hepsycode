/*****************************************************************************/

#include "mainsystem.h"

#define TAP16 16


//f16m
void mainsystem::fir16_main()
{
	// datatype for channels
	fir16e_parameters fir16e_p;
	fir16e_results fir16e_r;
	fir16s_parameters fir16s_p;
	fir16s_results fir16s_r;

	// local variables
	sc_uint<8>  sample_tmp;
	sc_uint<19> acc;
	sc_uint<9> coef[16];
	sc_uint<8> shift[16];

	// init
	S(5) coef[0] = 6;
	S(5) coef[1] = 4;
	S(5) coef[2] = 13;
	S(5) coef[3] = 16;
	S(5) coef[4] = 18;
	S(5) coef[5] = 41;
	S(5) coef[6] = 23;
	S(5) coef[7] = 154;
	S(5) coef[8] = 222;
	S(5) coef[9] = 154;
	S(5) coef[10] = 23;
	S(5) coef[11] = 41;
	S(5) coef[12] = 18;
	S(5) coef[13] = 16;
	S(5) coef[14] = 13;
	S(5) coef[15] = 4;

	S(5) for (unsigned int i=0; i<TAP16; i++)
	{S(5)
		S(5) shift[i] = 0;
	S(5)}

	// main functionality

	S(5) while(1)
	{S(5)
		// Main input from channel
		S(5) sample_tmp=stim2_channel_port->read();
		
		//fir16e

		// fill datatype
		S(5) fir16e_p.acc=acc;
		S(5) for( unsigned j=0; j<TAP16; j++) fir16e_p.coef[j]=coef[j];
		S(5) fir16e_p.sample_tmp=sample_tmp;
		S(5) for( unsigned j=0; j<TAP16; j++) fir16e_p.shift[j]=shift[j];

		// send parameters and receive results
		S(5) fir16e_parameters_channel->write(fir16e_p);
		S(5) fir16e_r=fir16e_results_channel->read();

		// fill local variables
		S(5) acc=fir16e_r.acc;

		//fir16s

		// fill datatype
		S(5) fir16s_p.sample_tmp=sample_tmp;
		S(5) for( unsigned j=0; j<TAP16; j++) fir16s_p.shift[j]=shift[j];

		// send parameters and receive results
		S(5) fir16s_parameters_channel->write(fir16s_p);
		S(5) fir16s_r=fir16s_results_channel->read();

		// fill local variables
		S(5) for( unsigned j=0; j<TAP16; j++) shift[j]=fir16s_r.shift[j];
		
		// write output values on channel
		S(5) result16_channel->write(acc);
		
		// debug
		// cout << "FIR16: \t\t" << acc << "\t at time \t" << sc_time_stamp() << endl;
		
		P(5)
	}
}
