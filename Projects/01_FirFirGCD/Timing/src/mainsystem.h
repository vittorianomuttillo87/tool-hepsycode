#ifndef __MYSYSTEM__

#define __MYSYSTEM__

#include "sc_csp_channel_ifs.h"
#include "sc_csp_channel_timing.h"
#include "datatype.h"
#include "SystemManager_timing.h"

SC_MODULE(mainsystem)
{
	// Ports for testbench connections
	sc_port< sc_csp_channel_in_if< sc_uint<8> > > stim1_channel_port;
	sc_port< sc_csp_channel_in_if< sc_uint<8> > > stim2_channel_port;
	sc_port< sc_csp_channel_out_if< sc_uint<8> > > result_channel_port;

	// PROCESSES

	// Process
	void fir8_main();
	// Fake processes
	void fir8_evaluation();
	void fir8_shifting();

	// Process
	void fir16_main();
	// Fake processes
	void fir16_evaluation();
	void fir16_shifting();

	// Process
	void gcd_main();
	// Fake process
	void gcd_evaluation();

	// CHANNELS

	// fir8
	sc_csp_channel< fir8e_parameters > *fir8e_parameters_channel;
	sc_csp_channel< fir8e_results > *fir8e_results_channel;
	sc_csp_channel< fir8s_parameters > *fir8s_parameters_channel;
	sc_csp_channel< fir8s_results > *fir8s_results_channel;

	// fir16
	sc_csp_channel< fir16e_parameters > *fir16e_parameters_channel;
	sc_csp_channel< fir16e_results > *fir16e_results_channel;
	sc_csp_channel< fir16s_parameters > *fir16s_parameters_channel;
	sc_csp_channel< fir16s_results > *fir16s_results_channel;

	// gcd
	sc_csp_channel< gcde_parameters > *gcde_parameters_channel;
	sc_csp_channel< gcde_results > *gcde_results_channel;

	// Inter-process connections
	sc_csp_channel< sc_uint<8> > *result8_channel;  	    
	sc_csp_channel< sc_uint<8> >   *result16_channel;  	    

	SC_CTOR(mainsystem)
	{
		fir8e_parameters_channel= new sc_csp_channel< fir8e_parameters >(163, 2, 3, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);
		fir8e_results_channel= new sc_csp_channel< fir8e_results>(19, 3, 2, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);
		fir8s_parameters_channel= new sc_csp_channel< fir8s_parameters>(72, 2, 4, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);
		fir8s_results_channel= new sc_csp_channel< fir8s_results>(64, 4, 2, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);

		fir16e_parameters_channel= new sc_csp_channel< fir16e_parameters >(299, 5, 6, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);
		fir16e_results_channel= new sc_csp_channel< fir16e_results>(19, 6, 5, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);
		fir16s_parameters_channel= new sc_csp_channel< fir16s_parameters>(136, 5, 7, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);
		fir16s_results_channel= new sc_csp_channel< fir16s_results>(128, 7, 5, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);

		gcde_parameters_channel = new sc_csp_channel< gcde_parameters >(16, 8, 9, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);
		gcde_results_channel = new sc_csp_channel< gcde_results >(8, 9, 8, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);

		result8_channel= new sc_csp_channel< sc_uint<8> >(8, 2, 8, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);
		result16_channel= new sc_csp_channel< sc_uint<8> >(8, 5, 8, pSystemManager->getLink().physical_width , pSystemManager->getLink().tcomm , pSystemManager->getLink().tacomm);

		SC_THREAD(fir8_main);
		SC_THREAD(fir8_evaluation);
		SC_THREAD(fir8_shifting);

		SC_THREAD(fir16_main);
		SC_THREAD(fir16_evaluation);
		SC_THREAD(fir16_shifting);

		SC_THREAD(gcd_main);
		SC_THREAD(gcd_evaluation);

	}


};


#endif
