
#include "mainsystem.h"

#define TAP16 16

//f16s
void mainsystem::fir16_shifting()
{
	// datatype for channels
	fir16s_parameters fir16s_p;
	fir16s_results fir16s_r;

	// local variables
	sc_uint<8> sample_tmp;
	sc_uint<8> shift[16];

	S(7) while(1)
	{S(7)

		// read parameters from channel
		S(7) fir16s_p=fir16s_parameters_channel->read();

		// fill local variables
		S(7) sample_tmp=fir16s_p.sample_tmp;
		S(7) for( unsigned j=0; j<TAP16; j++) shift[j]=fir16s_p.shift[j];

		// process

		S(7) for(int i=TAP16-2; i>=0; i--)
		{S(7)
			S(7) shift[i+1] = shift[i];
		S(7)}

		S(7) shift[0]=sample_tmp;

		// fill datatype
		S(7) for( unsigned j=0; j<TAP16; j++) fir16s_p.shift[j]=shift[j];

		// send results by channel
		S(7) fir16s_results_channel->write(fir16s_r);

		P(7)
	}
}
/*****************************************************************************/




