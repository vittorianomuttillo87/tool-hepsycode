
#include "mainsystem.h"

#define TAP16 16

//f16e
void mainsystem::fir16_evaluation()
{
	// datatype for channels
	fir16e_parameters fir16e_p;
	fir16e_results fir16e_r;

	// local variables
	sc_int<17> pro;
	sc_uint<19> acc;
	sc_uint<9> coef[16];
	sc_uint<8>  sample_tmp;
	sc_uint<8> shift[16];

	S(6) while(1)
	{S(6)

		// read parameters from channel
		S(6) fir16e_p=fir16e_parameters_channel->read();

		// fill local variables
		S(6) sample_tmp=fir16e_p.sample_tmp;
		S(6) for( unsigned j=0; j<TAP16; j++) coef[j]=fir16e_p.coef[j];
		S(6) for( unsigned j=0; j<TAP16; j++) shift[j]=fir16e_p.shift[j];

		// process
		S(6) acc=sample_tmp*coef[0];

		S(6) for(int i=TAP16-2; i>=0; i--)
		{S(6)
			S(6) pro=shift[i]*coef[i+1];
			S(6) acc += pro;
		S(6)}

		// fill datatype
		S(6) fir16e_r.acc=acc;

		// send results by channel
		S(6) fir16e_results_channel->write(fir16e_r);

		P(6)
	}
}




