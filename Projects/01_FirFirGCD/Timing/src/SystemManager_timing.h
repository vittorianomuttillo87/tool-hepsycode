#ifndef __SYSTEMMANAGER__
#define __SYSTEMMANAGER__

#include <systemc.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <set>

#include "tl.h"

#include "define.h"

using namespace std;

class SystemManager{

	private:
		vector<ProcessingUnit> VP;
		vector<Process> VPS;
		vector<BasicBlock> VBB;
		multimap<int, pair<string,int> > allocation; // multimap permette chiavi duplicate
		Link L;
		vector<Channel> VCH;
		multimap<string, pair<string,int> > allocationLC;
		int maxBBSoftware;

	public:
		SystemManager();
		vector<ProcessingUnit> getVP();
		vector<Process> getVPS();
		vector<BasicBlock> getVBB();
		multimap<int, pair<string,int> > getAllocation();
		vector<ProcessingUnit> generateProcessorInstances();
		vector<BasicBlock> generateBBInstances();
		vector<Process> generateProcessInstances();
		multimap<int, pair<string,int> > mapping();
		pair<string,int> getPRIDbyProcess(int processId);
		void searchProcessor(pair<string,int>, ProcessingUnit* &p);
		void searchProcess(int processId, Process* &ps);
		void Profiling(int processId); // incremento profiling al termine di ogni ciclo while (macro P)
		void Increase(int processId); // incremento processTime per ogni statement (macro I)
		sc_time upSimTime(int processId);
		Link getLink();
		vector<Channel> getChannels();
		multimap<string, pair<string,int> > getAllocationLC();
		Link generateLinkInstance();  // prevedere di far tornare un vettore di link
		vector<Channel> generateChannelInstances();
		multimap<string, pair<string,int> > mappingLC();
		bool checkSPP(int processId);
		int getmaxBBSoftware();
};


// Macro for instrumentation

#define P(X) pSystemManager->Profiling(X);

#define I(X) pSystemManager->Increase(X);   \
	wait(pSystemManager->upSimTime(X));


#endif
