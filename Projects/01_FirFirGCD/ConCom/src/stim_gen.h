#ifndef __STIMGEN__

#define __STIMGEN__

#include "sc_csp_channel_ifs.h"
#include "sc_csp_channel_functional_CC.h"

SC_MODULE(stim_gen)
{
	sc_port < sc_csp_channel_out_if < sc_uint<8> > > stim1_channel_port;  	    
	sc_port < sc_csp_channel_out_if < sc_uint<8> > > stim2_channel_port;  	    

	SC_CTOR(stim_gen)
	{
		SC_THREAD(main);
	}  

	void main();
};

#endif
