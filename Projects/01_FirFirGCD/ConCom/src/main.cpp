#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <set>

#include <systemc.h>
#include "sc_csp_channel_ifs.h"
#include "datatype.h"
#include "stim_gen.h"
#include "mainsystem.h"
#include "display.h"
#include <string.h>
#include <iomanip>
#include <locale>

#include "sc_csp_channel_functional_CC.h"
#include "SystemManager_CC.h"
#include "tl.h"

/////////////////////////////////////////////////////////////////////////////////////////
// Testbench
/////////////////////////////////////////////////////////////////////////////////////////

using namespace std;

SystemManager	*pSystemManager = new SystemManager();

/////////////////////////////////////////////////////////////////////////////////////////

int sc_main(int a, char* b[])
{

	sc_csp_channel< sc_uint<8> >   stim1_channel (12,8,0,2);
	sc_csp_channel< sc_uint<8> >   stim2_channel (13,8,0,5);
	sc_csp_channel< sc_uint<8> >   result_channel (14,8,8,1);

	// Instantiation and connection of testbench and system

	stim_gen mystimgen("mystimgen");
	mystimgen.stim1_channel_port(stim1_channel); 
	mystimgen.stim2_channel_port(stim2_channel); 

	mainsystem mysystem("mysystem");
	mysystem.stim1_channel_port(stim1_channel);
	mysystem.stim2_channel_port(stim2_channel);
	mysystem.result_channel_port(result_channel);

	display mydisplay("mydisplay");
	mydisplay.result_channel_port(result_channel); 

	sc_start();

	#if _WIN32
	    system("pause");
	#endif

	cout << endl << "PROCESSES CONCURRENCY" << endl;
	cout<< setw(2) << "   ";
	for (int j=0; j<NPS; j++){
		cout<< setw(2) << j <<" ";
	}
	cout<< endl;

	for (int i=0; i<NPS; i++){
		cout<< setw(2) << i << " " ;
		for (int j=0; j<NPS; j++){
			cout<< setw(2) << pSystemManager->matrix[i][j]<<",";
		}
		cout<<endl;
	}

	/*cout << endl << "PROCESSES CONCURRENCY total controls" << endl;
	for (int i=0; i<NPS; i++){
		for (int j=0; j<NPS; j++){
			cout<< pSystemManager->countPS[i][j]<<",";
		}
		cout<<endl;
	}*/

	cout << endl << "CHANNELS CONCURRENCY" << endl;

	cout<< setw(2) << "   ";
	for (int j=0; j<NCH; j++){
		cout<< setw(2) << j <<" ";
	}
	cout<< endl;

	for (int i=0; i<NCH; i++){
		cout<< setw(2) << i << " " ;
		for (int j=0; j<NCH; j++){
			cout<< setw(2) << pSystemManager->matrixCH[i][j]<<",";
		}
		cout<<endl;
	}

	/*cout << endl << "CHANNELS CONCURRENCY total controls" << endl;
	for (int i=0; i<NCH; i++){
		for (int j=0; j<NCH; j++){
			cout<< pSystemManager->countCH[i][j]<<",";
		}
		cout<<endl;
	}*/


	int maxPs = pSystemManager->findMaxPs(pSystemManager->matrix);
	cout << endl << "PROCESSES NORMALIZED CONCURRENCY" << endl;
	cout<< setw(2) << "   ";
	for (int j=0; j<NPS; j++){
		cout<< setw(8) << j <<" ";
	}
	cout<< endl;

	for (int i=0; i<NPS; i++){
		cout<< setw(2) << i << " " ;
		for (int j=0; j<NPS; j++){
			pSystemManager->matrixNM[i][j] = pSystemManager->matrix[i][j]/maxPs;
			cout<< setw(8) << pSystemManager->matrixNM[i][j]<<",";
		}
		cout<<endl;
	}


	int maxCh = pSystemManager->findMaxCh(pSystemManager->matrixCH);
	cout << endl << "CHANNELS NORMALIZED CONCURRENCY" << endl;
	cout<< setw(2) << "   ";
	for (int j=0; j<NCH; j++){
		cout<< setw(8) << j <<" ";
	}
	cout<< endl;

	for (int i=0; i<NCH; i++){
		cout<< setw(2) << i << " " ;
		for (int j=0; j<NCH; j++){
			pSystemManager->matrixCHNM[i][j] = pSystemManager->matrixCH[i][j]/maxCh;
			cout<< setw(8) << pSystemManager->matrixCHNM[i][j]<<",";
		}
		cout<<endl;
	}

	// Communication
	cout << endl << "PROCESS COMMUNICATION" << endl;
	for (int i=0; i<NPS; i++){
		for (int j=0; j<NPS; j++){
			pSystemManager->matrixCOM[i][j] = 0;
			for(int k=0; k<NCH; k++){
				if(pSystemManager->VCH[k].getW_id() == i && pSystemManager->VCH[k].getR_id() == j){
					pSystemManager->matrixCOM[i][j] += pSystemManager->VCH[k].getWidth()*pSystemManager->VCH[k].getNum();
				}
			}
			cout<< pSystemManager->matrixCOM[i][j]<<",";
		}
		cout<<endl;
	}

	cout << endl << "COMMUNICATION PROFILING" << endl << endl;

	cout << "WRITER\t\t\tREADER\t\t\tCOM\t\t\tBIT\t\t\tNUM*BIT\t\t\tTIME (sec)" << endl <<endl;
	stim1_channel.report_profiling();
	stim2_channel.report_profiling();
	result_channel.report_profiling();
	mysystem.result8_channel->report_profiling();
	mysystem.result16_channel->report_profiling();
	mysystem.fir8e_parameters_channel->report_profiling();
	mysystem.fir8e_results_channel->report_profiling();
	mysystem.fir8s_parameters_channel->report_profiling();
	mysystem.fir8s_results_channel->report_profiling();
	mysystem.fir16e_parameters_channel->report_profiling();
	mysystem.fir16e_results_channel->report_profiling();
	mysystem.fir16s_parameters_channel->report_profiling();
	mysystem.fir16s_results_channel->report_profiling();
	mysystem.gcde_parameters_channel->report_profiling();
	mysystem.gcde_results_channel->report_profiling();
	cout << endl;

	pSystemManager->deleteConcXml();
	pSystemManager->updateXml();

	return 0;


}
