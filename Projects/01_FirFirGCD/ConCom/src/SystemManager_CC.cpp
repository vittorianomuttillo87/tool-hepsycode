/*
 * SystemManager.cpp
 *
 *  Created on: 07 ott 2016
 *      Author: daniele
 */

#include <systemc.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <set>
#include <string.h>
#include <iostream>
#include <sstream>

#include "tl.h"
#include "pugixml.hpp"

#include "SystemManager_CC.h"

using namespace std;
using namespace pugi;

SystemManager::SystemManager(){
	VPS = generateProcessInstances();
	VCH = generateChannelInstances();
	for(unsigned int j=0; j<VPS.size(); j++) ready[j]=true;
	for(unsigned int j=0; j<VCH.size(); j++) readyCH[j]=1;
}

SystemManager::SystemManager(int ps, int ch){
	VPS = generateProcessInstances();
	VCH = generateChannelInstances();
	for(unsigned int j=0; j<VPS.size(); j++) ready[j]=true;
	for(unsigned int j=0; j<VCH.size(); j++) readyCH[j]=1;
}

vector<Process> SystemManager:: getVPS()
{
	return this->VPS;
}

vector<Process> SystemManager:: generateProcessInstances()
{

	/*****************************
	 *   LOAD PROCESSES
	*****************************/

	vector<Process> vps;

	// parsing xml file

	xml_document myDoc;
	xml_parse_result myResult = myDoc.load_file("./XML/application.xml");
	std::cout << "Load process result: " << myResult.description() << endl;

	xml_node instancesPS = myDoc.child("instancesPS");

	cout<<endl<<"****** Processes ******"<<endl;

	xml_node_iterator seqProcess_it;

	for (seqProcess_it=instancesPS.begin(); seqProcess_it!=instancesPS.end(); ++seqProcess_it){

		xml_node_iterator process_node_it = seqProcess_it->begin();

		Process pi;

		char* temp;

		string name = process_node_it->child_value();
		pi.setName(name);
		cout << "name: " << pi.getName() << endl;

		process_node_it++;
		temp = (char*) process_node_it->child_value();
		unsigned int id = atoi(temp);
		pi.setId(id);
		cout << "Id: " << pi.getId() << endl;

		process_node_it++;
		temp = (char*) process_node_it->child_value();
		unsigned int priority = atoi(temp);
		pi.setPriority(priority);
		cout << "Priority: " << pi.getPriority() << endl;

		process_node_it++;
		temp = (char*) process_node_it->child_value();
		unsigned int criticality = atoi(temp);
		pi.setCriticality(criticality);
		cout << "Criticality: " << pi.getCriticality() << endl<<endl;

		vps.push_back(pi);

	}

	return vps;
}

void SystemManager:: searchProcess(int processId, Process* &ps)
{
	int n=VPS.size();
	for (int i=0; i<n; i++){
		if (VPS[i].getId() == processId)
			ps = &VPS[i];
	}
}

bool* SystemManager:: getStatesProcesses()
{
	return this->ready;
}

void SystemManager:: checkStatesProcesses()
{

	for (unsigned int i=2; i<NPS; i++){
		if (ready[i]==1){
			for(unsigned int k=i+1; k<NPS; k++){
				//countPS[i][k]++;
				if(ready[k]==1)
					matrix[i][k]++;
			}
		}
	}

}

int SystemManager:: findMaxPs(float matrix[NPS][NPS])
{
	int max=matrix[0][0];
	for(int i=0;i<NPS;i++)
	{
	    for(int j=0;j<NPS;j++)
	    {
	        if(matrix[i][j]>max)
	            max=matrix[i][j];
	    }
	}
	return max;
}

vector<Channel> SystemManager:: generateChannelInstances()
{
	vector<Channel> vch;
	char* temp;

	// parsing xml file

	xml_document myDoc;
	xml_parse_result myResult = myDoc.load_file("./XML/application.xml");
	std::cout << "Channels load result: " << myResult.description() << endl;

	xml_node instancesLL = myDoc.child("instancesLL");

	//channel parameters

	cout<<endl<<"****** Channels ******"<<endl;

	xml_node_iterator seqChannel_it;

	for (seqChannel_it=instancesLL.begin(); seqChannel_it!=instancesLL.end(); ++seqChannel_it){

		xml_node_iterator channel_node_it = seqChannel_it->begin();

		Channel ch;

		string name = channel_node_it->child_value();
		ch.setName(name);
		cout << "Name: " << ch.getName() << endl;

		channel_node_it++;
		temp = (char*) channel_node_it->child_value();
		int  id = atoi(temp);
		ch.setId(id);
		cout << "Id: " << ch.getId() << endl;

		channel_node_it++;
		temp = (char*) channel_node_it->child_value();
		int  w_id = atoi(temp);
		ch.setW_id(w_id);
		cout << "Writer Id: " << ch.getW_id() << endl;

		channel_node_it++;
		temp = (char*) channel_node_it->child_value();
		int  r_id = atoi(temp);
		ch.setR_id(r_id);
		cout << "Reader Id: " << ch.getR_id() << endl;

		channel_node_it++;
		temp = (char*) channel_node_it->child_value();
		int  width = atoi(temp);
		ch.setWidth(width);
		cout << "Width: " << ch.getWidth() << endl<<endl;

		ch.setNum(0);

		vch.push_back(ch);

	}

	return vch;
}

vector<Channel> SystemManager:: getChannels()
{
	return this->VCH;
}

void SystemManager:: checkStatesChannels()
{

	for (unsigned int i=0; i<NCH; i++){
		if (readyCH[i]==0){
			for(unsigned int k=i+1; k<NCH; k++){
				if(readyCH[k]==0)
					matrixCH[i][k]++;
			}
		}
	}
}

int SystemManager:: findMaxCh(float matrixCH[NCH][NCH])
{
	int max=matrixCH[0][0];
	for(int i=0;i<NCH;i++)
	{
	    for(int j=0;j<NCH;j++)
	    {
	        if(matrixCH[i][j]>max)
	            max=matrixCH[i][j];
	    }
	}
	return max;
}

void SystemManager::deleteConcXml()
{

	pugi::xml_document myDoc;
	pugi::xml_parse_result myResult = myDoc.load_file("./XML/application.xml");
	cout << "XML Delete result: " << myResult.description() << endl;

	//method 2: use object/node structure
	pugi::xml_node instancesPS = myDoc.child("instancesPS");
	xml_node processes = instancesPS.child("process");

	for(int i = 0; i < NPS; i++){

		xml_node concom = processes.child("concurrency");
		for (pugi::xml_node processorId = concom.child("processId"); processorId; processorId = processorId.next_sibling()) {
			concom.remove_child(processorId);
		}

		xml_node concom2 = processes.child("comunication");
		for (pugi::xml_node processorId = concom2.child("rec"); processorId; processorId = processorId.next_sibling()) {
			concom2.remove_child(processorId);
		}

		processes = processes.next_sibling();

	}

	xml_node instancesCH = myDoc.child("instancesLL");
	xml_node processesCH = instancesCH.child("logical_link");

	for(int i = 0; i < NCH; i++){

		xml_node concom3 = processesCH.child("concurrency");
		for (pugi::xml_node processorId = concom3.child("channelId"); processorId; processorId = processorId.next_sibling()) {
			concom3.remove_child(processorId);
		}

		processesCH = processesCH.next_sibling();

	}

	myDoc.save_file("./XML/application.xml");
	cout<<endl;
}

void SystemManager:: updateXml()
{

	pugi::xml_document myDoc;
	pugi::xml_parse_result myResult = myDoc.load_file("./XML/application.xml");
	cout << "XML result: " << myResult.description() << endl;

	//method 2: use object/node structure
	pugi::xml_node instancesPS = myDoc.child("instancesPS");

	for (xml_node_iterator seqProcess_it=instancesPS.begin(); seqProcess_it!=instancesPS.end(); ++seqProcess_it){
		int Id = atoi(seqProcess_it->child_value("id"));

		if(seqProcess_it->child("concurrency")){
			pugi::xml_node concurrency = seqProcess_it->child("concurrency");
			for (int i=0; i<NPS; i++){
				if(i!=Id){
					pugi::xml_node conc_it = concurrency.append_child("processId");
					conc_it.append_attribute("id").set_value(i);
					conc_it.append_attribute("value").set_value(matrixNM[Id][i]);
				}
			}
		}else{
			pugi::xml_node concurrency = seqProcess_it->append_child("concurrency");
			for (int i=0; i<NPS; i++){
				if(i!=Id){
					pugi::xml_node conc_it = concurrency.append_child("processId");
					conc_it.append_attribute("id").set_value(i);
					conc_it.append_attribute("value").set_value(matrixNM[Id][i]);
				}
			}
		}
	}

	//method 2: use object/node structure
	pugi::xml_node instancesCOM = myDoc.child("instancesPS");

	for (pugi::xml_node_iterator seqProcess_it=instancesCOM.begin(); seqProcess_it!=instancesCOM.end(); ++seqProcess_it){
		int Id = atoi(seqProcess_it->child_value("id"));

		if(seqProcess_it->child("comunication")){
			pugi::xml_node comunication = seqProcess_it->child("comunication");
			for (int i=0; i<NPS; i++){
				if(i!=Id){
					pugi::xml_node com_it = comunication.append_child("rec");
					com_it.append_attribute("value").set_value(matrixCOM[Id][i]);
					com_it.append_attribute("idRec").set_value(i);
				}
			}
		}else{
			pugi::xml_node comunication = seqProcess_it->append_child("comunication");
			for (int i=0; i<NPS; i++){
				if(i!=Id){
					pugi::xml_node com_it = comunication.append_child("rec");
					com_it.append_attribute("value").set_value(matrixCOM[Id][i]);
					com_it.append_attribute("idRec").set_value(i);
				}
			}
		}

	}

	pugi::xml_node instancesLL = myDoc.child("instancesLL");

	for (xml_node_iterator seqLink_it=instancesLL.begin(); seqLink_it!=instancesLL.end(); ++seqLink_it){
		int Id = atoi(seqLink_it->child_value("id"));

		if(seqLink_it->child("concurrency")){
			pugi::xml_node concurrencyL = seqLink_it->child("concurrency");
			for (int i=0; i<NCH; i++){
				if(i!=Id){
					pugi::xml_node concL_it = concurrencyL.append_child("channelId");
					concL_it.append_attribute("id").set_value(i);
					concL_it.append_attribute("value").set_value(matrixCHNM[Id][i]);
				}
			}
		}else{
			pugi::xml_node concurrencyL = seqLink_it->append_child("concurrency");
			for (int i=0; i<NCH; i++){
				if(i!=Id){
					pugi::xml_node concL_it = concurrencyL.append_child("channelId");
					concL_it.append_attribute("id").set_value(i);
					concL_it.append_attribute("value").set_value(matrixCHNM[Id][i]);
				}
			}
		}
	}

	cout << "Saving result: " << myDoc.save_file("./XML/application.xml") << endl;
	myDoc.reset();
	cout<<endl;
}
