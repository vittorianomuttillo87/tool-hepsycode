#ifndef __DATATYPE__
#define __DATATYPE__

//////////////////////////////

#include <systemc.h>

////////////////////////////
//FIR16

struct fir16e_parameters
{
	sc_uint<19> acc;
	sc_uint<9> coef[16];
	sc_uint<8> shift[16];
	sc_uint<8> sample_tmp;
};

struct fir16e_results
{
	sc_uint<19> acc;
};

struct fir16s_parameters
{
	sc_uint<8> shift[16];
	sc_uint<8> sample_tmp;
};

struct fir16s_results
{
sc_uint<8> shift[16];
};

////////////////////////////
//FIR8

struct fir8e_parameters
{
	sc_uint<19> acc;
	sc_uint<9> coef[8];
	sc_uint<8> shift[8];
	sc_uint<8> sample_tmp;
};

struct fir8e_results
{
	sc_uint<19> acc;
};

struct fir8s_parameters
{
	sc_uint<8> shift[8];
	sc_uint<8> sample_tmp;
};

struct fir8s_results
{
	sc_uint<8> shift[8];
};

////////////////////////////
//GCD

struct gcde_parameters
{
	sc_uint<8> sample1;
	sc_uint<8> sample2;
};

struct gcde_results
{
	sc_uint<8> result;
};

//////////////////////////////

#endif
