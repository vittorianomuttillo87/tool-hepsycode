/*****************************************************************************

  sc_csp_channel.h -- The sc_csp_channel<T> primitive channel class.

 *****************************************************************************/

#ifndef SC_CSP_CHANNEL_H
#define SC_CSP_CHANNEL_H

#include "sysc/communication/sc_communication_ids.h"
#include "sysc/communication/sc_prim_channel.h"
#include "sysc/kernel/sc_event.h"
#include "sysc/communication/sc_communication_ids.h"
#include "sysc/kernel/sc_simcontext.h"
#include "sysc/tracing/sc_trace.h"
#include <typeinfo>
#include <string.h>
#include <iostream>
#include <sstream>

#include "sc_csp_channel_ifs.h"
#include "SystemManager_CC.h"

extern SystemManager *pSystemManager;

using namespace sc_core;
using namespace std;

// ----------------------------------------------------------------------------
//  CLASS : sc_csp_channel<T>
//
//  The sc_csp_channel<T> primitive channel class.
// ----------------------------------------------------------------------------

template <class T>
class sc_csp_channel
: public sc_csp_channel_in_if<T>,
  public sc_csp_channel_out_if<T>,
  public sc_prim_channel
{
	public:

		// constructors

		explicit sc_csp_channel(
				unsigned int channel_id=0,
				unsigned int w=0,
				unsigned int writer_id=0,
				unsigned int reader_id=0,
				unsigned int s=1,
				float phy_w=0,
				float tc=0,
				float tac=0):sc_prim_channel( sc_gen_unique_name( "csp_channel" ) ),
				ready_to_read_event((std::string(SC_KERNEL_EVENT_PREFIX)+"_read_event").c_str()),
				ready_to_write_event((std::string(SC_KERNEL_EVENT_PREFIX)+"_write_event").c_str())
		{ 
			id=channel_id;
			width=w;
			w_id=writer_id;
			r_id=reader_id;
			state=s;
			tcomm=tc;
			tacomm=tac;
			physical_width=phy_w;
			init();
			//string a = "time_scheduling"+to_string(r_id)+".txt";
			//myfile.open(a);

		}

		// interface methods
	    virtual void register_port( sc_port_base&, const char* );

		// blocking read
		virtual void read( T& );
		virtual T read();
 
		// blocking write
		virtual void write( const T& );

		// other methods

		operator T ()
		{ return read(); }

		sc_csp_channel<T>& operator = ( const T& a )
		{ write( a ); return *this; }

		void trace( sc_trace_file* tf ) const;

		virtual const char* kind() const
		{ return "sc_csp_channel"; }

		void report_profiling()
		{
			cout<<w_id<<"\t\t\t"<<r_id<<"\t\t\t"<<num<<"\t\t\t"<<width<<"\t\t\t"<<num*width<<"\t\t\t"<<working_time <<endl;
		}

		void report_comm()
		{
			cout<<w_id<<"\t"<<r_id<<"\t"<<num<<"\t"<<endl;
		}

	protected:

		void init();

		sc_event ready_to_read_event;
		sc_event ready_to_write_event;

		bool ready_to_read;
		bool ready_to_write;

		T  csp_buf; // the buffer

		// For analysis and profiling
		unsigned int width;
		unsigned int num;

	public:
		bool IPC; //interprocessor communication
		sc_time working_time;
		unsigned int w_id;
		unsigned int r_id;
		float tcomm;
		float tacomm;
		float physical_width;
		sc_event release[NPS];
		unsigned int id;
		unsigned int state;
		//ofstream myfile;

	protected:
		sc_port_base* m_reader;	// used for static design rule checking
		sc_port_base* m_writer;	// used for static design rule checking

};

// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII

// register port

template <class T>
inline
void
sc_csp_channel<T>::register_port( sc_port_base& port_,
			    const char* if_typename_ )
{
    std::string nm( if_typename_ );
    if( nm == typeid( sc_csp_channel_in_if<T> ).name())
	{
		// only one reader can be connected
		if( m_reader != 0 ) {
			SC_REPORT_ERROR( SC_ID_MORE_THAN_ONE_FIFO_READER_, 0 );
	}
	m_reader = &port_;
    } else if( nm == typeid( sc_csp_channel_out_if<T> ).name()) {
	// only one writer can be connected
	if( m_writer != 0 ) {
	    SC_REPORT_ERROR( SC_ID_MORE_THAN_ONE_FIFO_WRITER_, 0 );
	}
	m_writer = &port_;
    }
    else
    {
        SC_REPORT_ERROR( SC_ID_BIND_IF_TO_PORT_, 
	                 "sc_csp_channel<T> port not recognized" );
    }
}

// blocking read

template <class T>
inline
void
sc_csp_channel<T>::read( T& val_ )
{
	if (r_id >=2 && r_id<pSystemManager->getVPS().size()) pSystemManager->ready[r_id]=false;
	C

	if(ready_to_write==true)
	{
		pSystemManager->readyCH[id]=0; CH
		ready_to_read=true;
		C ready_to_read_event.notify(SC_ZERO_TIME);
		sc_core::wait(ready_to_write_event);

		val_=csp_buf;

		ready_to_read=false;
		C ready_to_read_event.notify(SC_ZERO_TIME);

		CH pSystemManager->readyCH[id]=1;
	}
	else
	{
		ready_to_read=true;
		C ready_to_read_event.notify(SC_ZERO_TIME);
		sc_core::wait(ready_to_write_event);

		val_=csp_buf;

		ready_to_read=false;
		C ready_to_read_event.notify(SC_ZERO_TIME);

		sc_core::wait(ready_to_write_event);
	}

	C
	if (r_id>=2 && r_id<pSystemManager->getVPS().size()) pSystemManager->ready[r_id]=true;
}

template <class T>
inline
T
sc_csp_channel<T>::read()
{
    T tmp;
    read( tmp );
    return tmp;
}

// blocking write

template <class T>
inline
void
sc_csp_channel<T>::write( const T& val_ )
{
	if (w_id>=2 && w_id<pSystemManager->getVPS().size()) pSystemManager->ready[w_id]=false;
	C

	if( ready_to_read==true)
	{
		pSystemManager->readyCH[id]=0; CH
		csp_buf=val_;

		ready_to_write=true;
		C ready_to_write_event.notify(SC_ZERO_TIME);
		sc_core::wait(ready_to_read_event);

		ready_to_write=false;
		C ready_to_write_event.notify(SC_ZERO_TIME);

		CH pSystemManager->readyCH[id]=1;
	}
	else
	{
		ready_to_write=true;
		C ready_to_write_event.notify(SC_ZERO_TIME);
		sc_core::wait(ready_to_read_event);

		csp_buf=val_;

		ready_to_write=false;
		C ready_to_write_event.notify(SC_ZERO_TIME);

		sc_core::wait(ready_to_read_event);
	}										

	// profiling
	num++;
	pSystemManager->VCH[id].setNum(num);

	C
	if (w_id>=2 && w_id<pSystemManager->getVPS().size()) pSystemManager->ready[w_id]=true;
}

template <class T>
inline
void
sc_csp_channel<T>::trace( sc_trace_file* tf ) const
{
#if defined(DEBUG_SYSTEMC)
    char buf[32];
    std::string nm = name();
    for( int i = 0; i < m_size; ++ i ) {
	std::sprintf( buf, "_%d", i );
	sc_trace( tf, m_buf[i], nm + buf );
    }
#endif
}

template <class T>
inline
void
sc_csp_channel<T>::init()
{
    m_reader = 0;
    m_writer = 0;

	num=0;
	ready_to_read=false;
	ready_to_write=false;
	working_time=sc_time(0, SC_MS);

}

#endif
