#ifndef __SYSTEMMANAGER__
#define __SYSTEMMANAGER__

#include <systemc.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <set>

#include "tl.h"

#include "define.h"

//#define NPS 10
//#define NCH 15

using namespace std;

class SystemManager{

	public:
		vector<Process> VPS;
		vector<Channel> VCH;
		unsigned int num;

		bool  ready[NPS];
		float matrix[NPS][NPS];
		float matrixNM[NPS][NPS];
		int   readyCH[NCH];
		float matrixCH[NCH][NCH];
		float matrixCHNM[NCH][NCH];
		float matrixCOM[NPS][NPS];

	public:
		SystemManager();
		SystemManager(int ps, int ch);
		vector<Process> getVPS();
		vector<Process> generateProcessInstances();
		void searchProcess(int processId, Process* &ps);
		bool* getStatesProcesses();
		void checkStatesProcesses();
		int findMaxPs(float matrix[NPS][NPS]);

		vector<Channel> getChannels();
		vector<Channel> generateChannelInstances();
		void checkStatesChannels();
		int findMaxCh(float matrixCH[NCH][NCH]);
		void deleteConcXml();
		void updateXml();
};

// Macro for instrumentation

#define C pSystemManager->checkStatesProcesses();

#define CH pSystemManager->checkStatesChannels();

#endif
