//============================================================================
// Name        : cpp.cpp
// Author      : Vittoriano Muttillo, Luigi Pomante
// Version     :
// Copyright   : Your copyright notice
// Description : Define
//============================================================================

#ifndef __DEFINE__
#define __DEFINE__

// Valori costanti di riferimento usati all'interno del programma

#define MAXBB	   30 // Numero massimo di instanze di Basic Blocs
#define MAXCLUSTER 3 // Numero massimo di famiglie di esecutori (3=GPP, DSP, ASP)
#define MAXEIL 30 // Numero massimo di famiglie di esecutori (3=GPP, DSP, ASP)

#endif
