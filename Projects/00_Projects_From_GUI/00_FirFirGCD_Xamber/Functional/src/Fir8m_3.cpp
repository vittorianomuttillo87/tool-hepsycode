#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir8m_3_main()
{
	// datatype for channels

	Fir8m_2_Fir8m_3_param Fir8m_2_Fir8m_3_param_var;

	Fir8m_3_Fir8m_4_param Fir8m_3_Fir8m_4_param_var;

	Fir8e_2_Fir8m_3_param Fir8e_2_Fir8m_3_param_var;

	Fir8m_3_Fir8s_2_param Fir8m_3_Fir8s_2_param_var;

//implementation
	while(1){
		
// Read from Fir8m_2
		Fir8m_2_Fir8m_3_param_var = Fir8m_2_Fir8m_3_channel->read();
	
// Write to Fir8m_4
		Fir8m_3_Fir8m_4_channel->write(Fir8m_3_Fir8m_4_param_var);
	
// Read from Fir8e_2
		Fir8e_2_Fir8m_3_param_var = Fir8e_2_Fir8m_3_channel->read();
	
// Write to Fir8s_2
		Fir8m_3_Fir8s_2_channel->write(Fir8m_3_Fir8s_2_param_var);
	
	}
}
//END
