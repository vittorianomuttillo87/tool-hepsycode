#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir16m_2_main()
{
	// datatype for channels

	Fir16m_1_Fir16m_2_param Fir16m_1_Fir16m_2_param_var;

	Fir16m_2_Fir16m_3_param Fir16m_2_Fir16m_3_param_var;

	Fir16e_1_Fir16m_2_param Fir16e_1_Fir16m_2_param_var;

//implementation
	while(1){
		
// Read from Fir16m_1
		Fir16m_1_Fir16m_2_param_var = Fir16m_1_Fir16m_2_channel->read();
	
// Write to Fir16m_3
		Fir16m_2_Fir16m_3_channel->write(Fir16m_2_Fir16m_3_param_var);
	
// Read from Fir16e_1
		Fir16e_1_Fir16m_2_param_var = Fir16e_1_Fir16m_2_channel->read();
	
	}
}
//END
