#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::gcde_1_main()
{
	// datatype for channels

	gcde_1_gcdm_2_param gcde_1_gcdm_2_param_var;

	gcde_1_gcde_2_param gcde_1_gcde_2_param_var;

//implementation
	while(1){
		
// Write to gcdm_2
		gcde_1_gcdm_2_channel->write(gcde_1_gcdm_2_param_var);
	
// Write to gcde_2
		gcde_1_gcde_2_channel->write(gcde_1_gcde_2_param_var);
	
	}
}
//END
