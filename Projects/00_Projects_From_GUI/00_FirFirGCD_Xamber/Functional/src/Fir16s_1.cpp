#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir16s_1_main()
{
	// datatype for channels

	Fir16s_1_Fir16m_4_param Fir16s_1_Fir16m_4_param_var;

	Fir16s_1_Fir16s_2_param Fir16s_1_Fir16s_2_param_var;

//implementation
	while(1){
		
// Write to Fir16m_4
		Fir16s_1_Fir16m_4_channel->write(Fir16s_1_Fir16m_4_param_var);
	
// Write to Fir16s_2
		Fir16s_1_Fir16s_2_channel->write(Fir16s_1_Fir16s_2_param_var);
	
	}
}
//END
