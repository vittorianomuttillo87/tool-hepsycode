#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::gcdm_2_main()
{
	// datatype for channels

	gcdm_1_gcdm_2_param gcdm_1_gcdm_2_param_var;

	gcdm_2_gcdm_3_param gcdm_2_gcdm_3_param_var;

	gcde_1_gcdm_2_param gcde_1_gcdm_2_param_var;

//implementation
	while(1){
		
// Read from gcdm_1
		gcdm_1_gcdm_2_param_var = gcdm_1_gcdm_2_channel->read();
	
// Write to gcdm_3
		gcdm_2_gcdm_3_channel->write(gcdm_2_gcdm_3_param_var);
	
// Read from gcde_1
		gcde_1_gcdm_2_param_var = gcde_1_gcdm_2_channel->read();
	
	}
}
//END
