#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir8e_1_main()
{
	// datatype for channels

	Fir8e_1_Fir8m_2_param Fir8e_1_Fir8m_2_param_var;

	Fir8e_1_Fir8e_2_param Fir8e_1_Fir8e_2_param_var;

//implementation
	while(1){
		
// Write to Fir8m_2
		Fir8e_1_Fir8m_2_channel->write(Fir8e_1_Fir8m_2_param_var);
	
// Write to Fir8e_2
		Fir8e_1_Fir8e_2_channel->write(Fir8e_1_Fir8e_2_param_var);
	
	}
}
//END
