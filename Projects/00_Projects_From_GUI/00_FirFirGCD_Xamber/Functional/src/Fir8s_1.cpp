#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir8s_1_main()
{
	// datatype for channels

	Fir8s_1_Fir8m_4_param Fir8s_1_Fir8m_4_param_var;

	Fir8s_1_Fir8s_2_param Fir8s_1_Fir8s_2_param_var;

//implementation
	while(1){
		
// Write to Fir8m_4
		Fir8s_1_Fir8m_4_channel->write(Fir8s_1_Fir8m_4_param_var);
	
// Write to Fir8s_2
		Fir8s_1_Fir8s_2_channel->write(Fir8s_1_Fir8s_2_param_var);
	
	}
}
//END
