#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir16m_4_main()
{
	// datatype for channels

	Fir16m_3_Fir16m_4_param Fir16m_3_Fir16m_4_param_var;

	Fir16m_4_Fir16m_5_param Fir16m_4_Fir16m_5_param_var;

	Fir16s_1_Fir16m_4_param Fir16s_1_Fir16m_4_param_var;

//implementation
	while(1){
		
// Read from Fir16m_3
		Fir16m_3_Fir16m_4_param_var = Fir16m_3_Fir16m_4_channel->read();
	
// Write to Fir16m_5
		Fir16m_4_Fir16m_5_channel->write(Fir16m_4_Fir16m_5_param_var);
	
// Read from Fir16s_1
		Fir16s_1_Fir16m_4_param_var = Fir16s_1_Fir16m_4_channel->read();
	
	}
}
//END
