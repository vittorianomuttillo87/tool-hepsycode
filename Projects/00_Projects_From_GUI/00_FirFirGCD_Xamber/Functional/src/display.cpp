#include <systemc.h>
#include "display.h"

void display::main()
{
	int i = 1;

	gcdm_3_display_param gcdm_3_display_param_var;

//implementation
	while(i <= 10)
	{

		gcdm_3_display_param_var = result_channel_port->read();
		cout << "Display-" << i <<": \t" << gcdm_3_display_param_var.example << " " << "\t at time \t" << sc_time_stamp().to_seconds() << endl;										
		i++;
	}
	sc_stop();
}
