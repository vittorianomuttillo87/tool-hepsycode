#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir16m_5_main()
{
	// datatype for channels

	Fir16m_4_Fir16m_5_param Fir16m_4_Fir16m_5_param_var;

	Fir16m_5_gcdm_1_param Fir16m_5_gcdm_1_param_var;

	Fir16s_2_Fir16m_5_param Fir16s_2_Fir16m_5_param_var;

//implementation
	while(1){
		
// Read from Fir16m_4
		Fir16m_4_Fir16m_5_param_var = Fir16m_4_Fir16m_5_channel->read();
	
// Write to gcdm_1
		Fir16m_5_gcdm_1_channel->write(Fir16m_5_gcdm_1_param_var);
	
// Read from Fir16s_2
		Fir16s_2_Fir16m_5_param_var = Fir16s_2_Fir16m_5_channel->read();
	
	}
}
//END
