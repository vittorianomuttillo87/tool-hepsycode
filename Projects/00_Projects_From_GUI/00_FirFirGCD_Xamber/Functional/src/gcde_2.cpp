#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::gcde_2_main()
{
	// datatype for channels

	gcde_1_gcde_2_param gcde_1_gcde_2_param_var;

	gcde_2_gcdm_3_param gcde_2_gcdm_3_param_var;

	gcdm_1_gcde_2_param gcdm_1_gcde_2_param_var;

//implementation
	while(1){
		
// Read from gcde_1
		gcde_1_gcde_2_param_var = gcde_1_gcde_2_channel->read();
	
// Write to gcdm_3
		gcde_2_gcdm_3_channel->write(gcde_2_gcdm_3_param_var);
	
// Read from gcdm_1
		gcdm_1_gcde_2_param_var = gcdm_1_gcde_2_channel->read();
	
	}
}
//END
