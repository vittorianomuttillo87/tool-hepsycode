#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir16m_3_main()
{
	// datatype for channels

	Fir16m_2_Fir16m_3_param Fir16m_2_Fir16m_3_param_var;

	Fir16m_3_Fir16m_4_param Fir16m_3_Fir16m_4_param_var;

	Fir16e_2_Fir16m_3_param Fir16e_2_Fir16m_3_param_var;

	Fir16m_3_Fir16s_2_param Fir16m_3_Fir16s_2_param_var;

//implementation
	while(1){
		
// Read from Fir16m_2
		Fir16m_2_Fir16m_3_param_var = Fir16m_2_Fir16m_3_channel->read();
	
// Write to Fir16m_4
		Fir16m_3_Fir16m_4_channel->write(Fir16m_3_Fir16m_4_param_var);
	
// Read from Fir16e_2
		Fir16e_2_Fir16m_3_param_var = Fir16e_2_Fir16m_3_channel->read();
	
// Write to Fir16s_2
		Fir16m_3_Fir16s_2_channel->write(Fir16m_3_Fir16s_2_param_var);
	
	}
}
//END
