#ifndef __MYSYSTEM__
#define __MYSYSTEM__

#include "sc_csp_channel_ifs.h"
#include "sc_csp_channel.h"
#include "datatype.h"

SC_MODULE(mainsystem)
{
	
// Ports for testbench connections
sc_port< sc_csp_channel_in_if< Stim1_Fir8m_1_param > > stim1_channel_port;
	
// Ports for testbench connections
sc_port< sc_csp_channel_in_if< Stim2_Fir16m_1_param > > stim2_channel_port;
	
// Ports for testbench connections
sc_port< sc_csp_channel_out_if< gcdm_3_display_param > > result_channel_port;


// Process Fir8m_1
void Fir8m_1_main();

// Process Fir8m_2
void Fir8m_2_main();

// Process Fir8m_3
void Fir8m_3_main();

// Process Fir8m_4
void Fir8m_4_main();

// Process Fir8m_5
void Fir8m_5_main();

// Process Fir8e_1
void Fir8e_1_main();

// Process Fir8e_2
void Fir8e_2_main();

// Process Fir8s_1
void Fir8s_1_main();

// Process Fir8s_2
void Fir8s_2_main();

// Process Fir16m_5
void Fir16m_5_main();

// Process Fir16m_1
void Fir16m_1_main();

// Process Fir16m_2
void Fir16m_2_main();

// Process Fir16m_3
void Fir16m_3_main();

// Process Fir16m_4
void Fir16m_4_main();

// Process Fir16e_1
void Fir16e_1_main();

// Process Fir16e_2
void Fir16e_2_main();

// Process gcdm_1
void gcdm_1_main();

// Process Fir16s_1
void Fir16s_1_main();

// Process Fir16s_2
void Fir16s_2_main();

// Process gcdm_2
void gcdm_2_main();

// Process gcdm_3
void gcdm_3_main();

// Process gcde_1
void gcde_1_main();

// Process gcde_2
void gcde_2_main();

// channel Fir8m_1_Fir8m_2_channel
sc_csp_channel< Fir8m_1_Fir8m_2_param > *Fir8m_1_Fir8m_2_channel;

// channel Fir8m_2_Fir8m_3_channel
sc_csp_channel< Fir8m_2_Fir8m_3_param > *Fir8m_2_Fir8m_3_channel;

// channel Fir8m_1_Fir8e_2_channel
sc_csp_channel< Fir8m_1_Fir8e_2_param > *Fir8m_1_Fir8e_2_channel;

// channel Fir8e_1_Fir8e_2_channel
sc_csp_channel< Fir8e_1_Fir8e_2_param > *Fir8e_1_Fir8e_2_channel;

// channel Fir8e_1_Fir8m_2_channel
sc_csp_channel< Fir8e_1_Fir8m_2_param > *Fir8e_1_Fir8m_2_channel;

// channel Fir8e_2_Fir8m_3_channel
sc_csp_channel< Fir8e_2_Fir8m_3_param > *Fir8e_2_Fir8m_3_channel;

// channel Fir8m_3_Fir8m_4_channel
sc_csp_channel< Fir8m_3_Fir8m_4_param > *Fir8m_3_Fir8m_4_channel;

// channel Fir8s_1_Fir8s_2_channel
sc_csp_channel< Fir8s_1_Fir8s_2_param > *Fir8s_1_Fir8s_2_channel;

// channel Fir8s_1_Fir8m_4_channel
sc_csp_channel< Fir8s_1_Fir8m_4_param > *Fir8s_1_Fir8m_4_channel;

// channel Fir8m_3_Fir8s_2_channel
sc_csp_channel< Fir8m_3_Fir8s_2_param > *Fir8m_3_Fir8s_2_channel;

// channel Fir8m_4_Fir8m_5_channel
sc_csp_channel< Fir8m_4_Fir8m_5_param > *Fir8m_4_Fir8m_5_channel;

// channel Fir8s_2_Fir8m_5_channel
sc_csp_channel< Fir8s_2_Fir8m_5_param > *Fir8s_2_Fir8m_5_channel;

// channel Fir16m_1_Fir16e_2_channel
sc_csp_channel< Fir16m_1_Fir16e_2_param > *Fir16m_1_Fir16e_2_channel;

// channel Fir16e_1_Fir16e_2_channel
sc_csp_channel< Fir16e_1_Fir16e_2_param > *Fir16e_1_Fir16e_2_channel;

// channel Fir16m_1_Fir16m_2_channel
sc_csp_channel< Fir16m_1_Fir16m_2_param > *Fir16m_1_Fir16m_2_channel;

// channel Fir16m_2_Fir16m_3_channel
sc_csp_channel< Fir16m_2_Fir16m_3_param > *Fir16m_2_Fir16m_3_channel;

// channel Fir16m_3_Fir16m_4_channel
sc_csp_channel< Fir16m_3_Fir16m_4_param > *Fir16m_3_Fir16m_4_channel;

// channel Fir16m_4_Fir16m_5_channel
sc_csp_channel< Fir16m_4_Fir16m_5_param > *Fir16m_4_Fir16m_5_channel;

// channel Fir8m_5_gcdm_1_channel
sc_csp_channel< Fir8m_5_gcdm_1_param > *Fir8m_5_gcdm_1_channel;

// channel Fir16e_1_Fir16m_2_channel
sc_csp_channel< Fir16e_1_Fir16m_2_param > *Fir16e_1_Fir16m_2_channel;

// channel Fir16e_2_Fir16m_3_channel
sc_csp_channel< Fir16e_2_Fir16m_3_param > *Fir16e_2_Fir16m_3_channel;

// channel Fir16s_1_Fir16m_4_channel
sc_csp_channel< Fir16s_1_Fir16m_4_param > *Fir16s_1_Fir16m_4_channel;

// channel Fir16m_3_Fir16s_2_channel
sc_csp_channel< Fir16m_3_Fir16s_2_param > *Fir16m_3_Fir16s_2_channel;

// channel Fir16s_1_Fir16s_2_channel
sc_csp_channel< Fir16s_1_Fir16s_2_param > *Fir16s_1_Fir16s_2_channel;

// channel Fir16s_2_Fir16m_5_channel
sc_csp_channel< Fir16s_2_Fir16m_5_param > *Fir16s_2_Fir16m_5_channel;

// channel Fir16m_5_gcdm_1_channel
sc_csp_channel< Fir16m_5_gcdm_1_param > *Fir16m_5_gcdm_1_channel;

// channel gcdm_1_gcdm_2_channel
sc_csp_channel< gcdm_1_gcdm_2_param > *gcdm_1_gcdm_2_channel;

// channel gcdm_2_gcdm_3_channel
sc_csp_channel< gcdm_2_gcdm_3_param > *gcdm_2_gcdm_3_channel;

// channel gcde_1_gcdm_2_channel
sc_csp_channel< gcde_1_gcdm_2_param > *gcde_1_gcdm_2_channel;

// channel gcdm_1_gcde_2_channel
sc_csp_channel< gcdm_1_gcde_2_param > *gcdm_1_gcde_2_channel;

// channel gcde_1_gcde_2_channel
sc_csp_channel< gcde_1_gcde_2_param > *gcde_1_gcde_2_channel;

// channel gcde_2_gcdm_3_channel
sc_csp_channel< gcde_2_gcdm_3_param > *gcde_2_gcdm_3_channel;

//mainsystem impl.
SC_CTOR(mainsystem)
{
Fir8m_1_Fir8m_2_channel = new sc_csp_channel< Fir8m_1_Fir8m_2_param >;

Fir8m_2_Fir8m_3_channel = new sc_csp_channel< Fir8m_2_Fir8m_3_param >;

Fir8m_1_Fir8e_2_channel = new sc_csp_channel< Fir8m_1_Fir8e_2_param >;

Fir8e_1_Fir8e_2_channel = new sc_csp_channel< Fir8e_1_Fir8e_2_param >;

Fir8e_1_Fir8m_2_channel = new sc_csp_channel< Fir8e_1_Fir8m_2_param >;

Fir8e_2_Fir8m_3_channel = new sc_csp_channel< Fir8e_2_Fir8m_3_param >;

Fir8m_3_Fir8m_4_channel = new sc_csp_channel< Fir8m_3_Fir8m_4_param >;

Fir8s_1_Fir8s_2_channel = new sc_csp_channel< Fir8s_1_Fir8s_2_param >;

Fir8s_1_Fir8m_4_channel = new sc_csp_channel< Fir8s_1_Fir8m_4_param >;

Fir8m_3_Fir8s_2_channel = new sc_csp_channel< Fir8m_3_Fir8s_2_param >;

Fir8m_4_Fir8m_5_channel = new sc_csp_channel< Fir8m_4_Fir8m_5_param >;

Fir8s_2_Fir8m_5_channel = new sc_csp_channel< Fir8s_2_Fir8m_5_param >;

Fir16m_1_Fir16e_2_channel = new sc_csp_channel< Fir16m_1_Fir16e_2_param >;

Fir16e_1_Fir16e_2_channel = new sc_csp_channel< Fir16e_1_Fir16e_2_param >;

Fir16m_1_Fir16m_2_channel = new sc_csp_channel< Fir16m_1_Fir16m_2_param >;

Fir16m_2_Fir16m_3_channel = new sc_csp_channel< Fir16m_2_Fir16m_3_param >;

Fir16m_3_Fir16m_4_channel = new sc_csp_channel< Fir16m_3_Fir16m_4_param >;

Fir16m_4_Fir16m_5_channel = new sc_csp_channel< Fir16m_4_Fir16m_5_param >;

Fir8m_5_gcdm_1_channel = new sc_csp_channel< Fir8m_5_gcdm_1_param >;

Fir16e_1_Fir16m_2_channel = new sc_csp_channel< Fir16e_1_Fir16m_2_param >;

Fir16e_2_Fir16m_3_channel = new sc_csp_channel< Fir16e_2_Fir16m_3_param >;

Fir16s_1_Fir16m_4_channel = new sc_csp_channel< Fir16s_1_Fir16m_4_param >;

Fir16m_3_Fir16s_2_channel = new sc_csp_channel< Fir16m_3_Fir16s_2_param >;

Fir16s_1_Fir16s_2_channel = new sc_csp_channel< Fir16s_1_Fir16s_2_param >;

Fir16s_2_Fir16m_5_channel = new sc_csp_channel< Fir16s_2_Fir16m_5_param >;

Fir16m_5_gcdm_1_channel = new sc_csp_channel< Fir16m_5_gcdm_1_param >;

gcdm_1_gcdm_2_channel = new sc_csp_channel< gcdm_1_gcdm_2_param >;

gcdm_2_gcdm_3_channel = new sc_csp_channel< gcdm_2_gcdm_3_param >;

gcde_1_gcdm_2_channel = new sc_csp_channel< gcde_1_gcdm_2_param >;

gcdm_1_gcde_2_channel = new sc_csp_channel< gcdm_1_gcde_2_param >;

gcde_1_gcde_2_channel = new sc_csp_channel< gcde_1_gcde_2_param >;

gcde_2_gcdm_3_channel = new sc_csp_channel< gcde_2_gcdm_3_param >;

SC_THREAD(Fir8m_1_main);
SC_THREAD(Fir8m_2_main);
SC_THREAD(Fir8m_3_main);
SC_THREAD(Fir8m_4_main);
SC_THREAD(Fir8m_5_main);
SC_THREAD(Fir8e_1_main);
SC_THREAD(Fir8e_2_main);
SC_THREAD(Fir8s_1_main);
SC_THREAD(Fir8s_2_main);
SC_THREAD(Fir16m_5_main);
SC_THREAD(Fir16m_1_main);
SC_THREAD(Fir16m_2_main);
SC_THREAD(Fir16m_3_main);
SC_THREAD(Fir16m_4_main);
SC_THREAD(Fir16e_1_main);
SC_THREAD(Fir16e_2_main);
SC_THREAD(gcdm_1_main);
SC_THREAD(Fir16s_1_main);
SC_THREAD(Fir16s_2_main);
SC_THREAD(gcdm_2_main);
SC_THREAD(gcdm_3_main);
SC_THREAD(gcde_1_main);
SC_THREAD(gcde_2_main);

	}
};	
#endif
