#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir8m_5_main()
{
	// datatype for channels

	Fir8m_4_Fir8m_5_param Fir8m_4_Fir8m_5_param_var;

	Fir8m_5_gcdm_1_param Fir8m_5_gcdm_1_param_var;

	Fir8s_2_Fir8m_5_param Fir8s_2_Fir8m_5_param_var;

//implementation
	while(1){
		
// Read from Fir8m_4
		Fir8m_4_Fir8m_5_param_var = Fir8m_4_Fir8m_5_channel->read();
	
// Write to gcdm_1
		Fir8m_5_gcdm_1_channel->write(Fir8m_5_gcdm_1_param_var);
	
// Read from Fir8s_2
		Fir8s_2_Fir8m_5_param_var = Fir8s_2_Fir8m_5_channel->read();
	
	}
}
//END
