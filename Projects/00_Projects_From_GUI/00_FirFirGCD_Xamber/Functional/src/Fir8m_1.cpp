#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir8m_1_main()
{
	// datatype for channels

	Stim1_Fir8m_1_param Stim1_Fir8m_1_param_var;

	Fir8m_1_Fir8m_2_param Fir8m_1_Fir8m_2_param_var;

	Fir8m_1_Fir8e_2_param Fir8m_1_Fir8e_2_param_var;

//implementation
	while(1){
		
// Read from Stimulus
		Stim1_Fir8m_1_param_var = stim1_channel_port->read();
	
// Write to Fir8m_2
		Fir8m_1_Fir8m_2_channel->write(Fir8m_1_Fir8m_2_param_var);
	
// Write to Fir8e_2
		Fir8m_1_Fir8e_2_channel->write(Fir8m_1_Fir8e_2_param_var);
	
	}
}
//END
