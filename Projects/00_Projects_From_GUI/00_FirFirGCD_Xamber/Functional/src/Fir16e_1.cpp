#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir16e_1_main()
{
	// datatype for channels

	Fir16e_1_Fir16m_2_param Fir16e_1_Fir16m_2_param_var;

	Fir16e_1_Fir16e_2_param Fir16e_1_Fir16e_2_param_var;

//implementation
	while(1){
		
// Write to Fir16m_2
		Fir16e_1_Fir16m_2_channel->write(Fir16e_1_Fir16m_2_param_var);
	
// Write to Fir16e_2
		Fir16e_1_Fir16e_2_channel->write(Fir16e_1_Fir16e_2_param_var);
	
	}
}
//END
