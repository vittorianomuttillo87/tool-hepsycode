#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::gcdm_3_main()
{
	// datatype for channels

	gcdm_2_gcdm_3_param gcdm_2_gcdm_3_param_var;

	gcdm_3_display_param gcdm_3_display_param_var;

	gcde_2_gcdm_3_param gcde_2_gcdm_3_param_var;

//implementation
	while(1){
		
// Read from gcdm_2
		gcdm_2_gcdm_3_param_var = gcdm_2_gcdm_3_channel->read();
	
// Read from gcde_2
		gcde_2_gcdm_3_param_var = gcde_2_gcdm_3_channel->read();
	
// Write to gcdm_3
		result_channel_port->write(gcdm_3_display_param_var);
	
	}
}
//END
