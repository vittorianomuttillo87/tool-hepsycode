#ifndef __DATATYPE__
#define __DATATYPE__

#include <systemc.h>

struct Stim1_Fir8m_1_param
{
	sc_uint<8> example;
};

struct Fir8m_1_Fir8m_2_param
{
	sc_uint<8> example;
};

struct Fir8m_2_Fir8m_3_param
{
	sc_uint<8> example;
};

struct Fir8m_1_Fir8e_2_param
{
	sc_uint<8> example;
};

struct Fir8e_1_Fir8e_2_param
{
	sc_uint<8> example;
};

struct Fir8e_1_Fir8m_2_param
{
	sc_uint<8> example;
};

struct Fir8e_2_Fir8m_3_param
{
	sc_uint<8> example;
};

struct Fir8m_3_Fir8m_4_param
{
	sc_uint<8> example;
};

struct Fir8s_1_Fir8s_2_param
{
	sc_uint<8> example;
};

struct Fir8s_1_Fir8m_4_param
{
	sc_uint<8> example;
};

struct Fir8m_3_Fir8s_2_param
{
	sc_uint<8> example;
};

struct Fir8m_4_Fir8m_5_param
{
	sc_uint<8> example;
};

struct Fir8s_2_Fir8m_5_param
{
	sc_uint<8> example;
};

struct Stim2_Fir16m_1_param
{
	sc_uint<8> example;
};

struct Fir16m_1_Fir16e_2_param
{
	sc_uint<8> example;
};

struct Fir16e_1_Fir16e_2_param
{
	sc_uint<8> example;
};

struct Fir16m_1_Fir16m_2_param
{
	sc_uint<8> example;
};

struct Fir16m_2_Fir16m_3_param
{
	sc_uint<8> example;
};

struct Fir16m_3_Fir16m_4_param
{
	sc_uint<8> example;
};

struct Fir16m_4_Fir16m_5_param
{
	sc_uint<8> example;
};

struct Fir8m_5_gcdm_1_param
{
	sc_uint<8> example;
};

struct Fir16e_1_Fir16m_2_param
{
	sc_uint<8> example;
};

struct Fir16e_2_Fir16m_3_param
{
	sc_uint<8> example;
};

struct Fir16s_1_Fir16m_4_param
{
	sc_uint<8> example;
};

struct Fir16m_3_Fir16s_2_param
{
	sc_uint<8> example;
};

struct Fir16s_1_Fir16s_2_param
{
	sc_uint<8> example;
};

struct Fir16s_2_Fir16m_5_param
{
	sc_uint<8> example;
};

struct Fir16m_5_gcdm_1_param
{
	sc_uint<8> example;
};

struct gcdm_1_gcdm_2_param
{
	sc_uint<8> example;
};

struct gcdm_2_gcdm_3_param
{
	sc_uint<8> example;
};

struct gcde_1_gcdm_2_param
{
	sc_uint<8> example;
};

struct gcdm_3_display_param
{
	sc_uint<8> example;
};

struct gcdm_1_gcde_2_param
{
	sc_uint<8> example;
};

struct gcde_1_gcde_2_param
{
	sc_uint<8> example;
};

struct gcde_2_gcdm_3_param
{
	sc_uint<8> example;
};

#endif
