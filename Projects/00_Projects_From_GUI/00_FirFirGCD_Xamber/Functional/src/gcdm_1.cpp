#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::gcdm_1_main()
{
	// datatype for channels

	Fir8m_5_gcdm_1_param Fir8m_5_gcdm_1_param_var;

	gcdm_1_gcdm_2_param gcdm_1_gcdm_2_param_var;

	Fir16m_5_gcdm_1_param Fir16m_5_gcdm_1_param_var;

	gcdm_1_gcde_2_param gcdm_1_gcde_2_param_var;

//implementation
	while(1){
		
// Read from Fir8m_5
		Fir8m_5_gcdm_1_param_var = Fir8m_5_gcdm_1_channel->read();
	
// Write to gcdm_2
		gcdm_1_gcdm_2_channel->write(gcdm_1_gcdm_2_param_var);
	
// Read from Fir16m_5
		Fir16m_5_gcdm_1_param_var = Fir16m_5_gcdm_1_channel->read();
	
// Write to gcde_2
		gcdm_1_gcde_2_channel->write(gcdm_1_gcde_2_param_var);
	
	}
}
//END
