#include <systemc.h>
#include "stim_gen.h"

void stim_gen::main()
{
	// init CODE
	unsigned int i=1;

	Stim1_Fir8m_1_param Stim1_Fir8m_1_param_var;
	Stim1_Fir8m_1_param_var.example = 1;

	Stim2_Fir16m_1_param Stim2_Fir16m_1_param_var;
	Stim2_Fir16m_1_param_var.example = 1;

//implementation
	while(1){
		
// content		
		wait(1, SC_MS);
		stim1_channel_port->write(Stim1_Fir8m_1_param_var);
		cout << "Stimulus-"<<i<<": \t" << Stim1_Fir8m_1_param_var.example << "\t at time \t" << sc_time_stamp() << endl;	
											
// content		
		wait(1, SC_MS);
		stim2_channel_port->write(Stim2_Fir16m_1_param_var);
		cout << "Stimulus-"<<i<<": \t" << Stim2_Fir16m_1_param_var.example << "\t at time \t" << sc_time_stamp() << endl;	
											
// Check for stop
		if(i >= 10) return;
		else i++;
	}
}
//END
