#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir8m_4_main()
{
	// datatype for channels

	Fir8m_3_Fir8m_4_param Fir8m_3_Fir8m_4_param_var;

	Fir8m_4_Fir8m_5_param Fir8m_4_Fir8m_5_param_var;

	Fir8s_1_Fir8m_4_param Fir8s_1_Fir8m_4_param_var;

//implementation
	while(1){
		
// Read from Fir8m_3
		Fir8m_3_Fir8m_4_param_var = Fir8m_3_Fir8m_4_channel->read();
	
// Write to Fir8m_5
		Fir8m_4_Fir8m_5_channel->write(Fir8m_4_Fir8m_5_param_var);
	
// Read from Fir8s_1
		Fir8s_1_Fir8m_4_param_var = Fir8s_1_Fir8m_4_channel->read();
	
	}
}
//END
