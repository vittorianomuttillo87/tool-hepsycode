#ifndef __DATATYPE__
#define __DATATYPE__

#include <systemc.h>

struct Stim1_Fir8_main_parameters
{
	sc_uint<8> example;
};

struct Stim2_Fir16_main_parameters
{
	sc_uint<8> example;
};

struct fir8e_results
{
	sc_uint<8> example;
};

struct fir8s_parameters
{
	sc_uint<8> example;
};

struct fir8s_results
{
	sc_uint<8> example;
};

struct fir8_results
{
	sc_uint<8> example;
};

struct fir16_results
{
	sc_uint<8> example;
};

struct gcd_main_display_parameters
{
	sc_uint<8> example;
};

struct gcde_parameters
{
	sc_uint<8> example;
};

struct gcde_results
{
	sc_uint<8> example;
};

struct fir16e_parameters
{
	sc_uint<8> example;
};

struct fir16e_results
{
	sc_uint<8> example;
};

struct fir16s_parameters
{
	sc_uint<8> example;
};

struct fir16s_results
{
	sc_uint<8> example;
};

struct fir8e_parameters
{
	sc_uint<8> example;
};

#endif
