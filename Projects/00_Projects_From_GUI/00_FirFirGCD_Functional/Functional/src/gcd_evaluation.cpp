#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::gcd_evaluation_main()
{
	// datatype for channels

	gcde_parameters gcde_parameters_var;

	gcde_results gcde_results_var;

//implementation
	while(1){
		
// Read from gcd_main
		gcde_parameters_var = gcde_parameters_channel->read();
	
// Write to gcd_main
		gcde_results_channel->write(gcde_results_var);
	
	}
}
//END
