#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::gcd_main_main()
{
	// datatype for channels

	fir8_results fir8_results_var;

	gcde_parameters gcde_parameters_var;

	gcde_results gcde_results_var;

	fir16_results fir16_results_var;

	gcd_main_display_parameters gcd_main_display_parameters_var;

//implementation
	while(1){
		
// Read from Fir8_main
		fir8_results_var = result8_channel->read();
	
// Write to gcd_evaluation
		gcde_parameters_channel->write(gcde_parameters_var);
	
// Read from gcd_evaluation
		gcde_results_var = gcde_results_channel->read();
	
// Read from Fir16_main
		fir16_results_var = result16_channel->read();
	
// Write to gcd_main
		result_channel_port->write(gcd_main_display_parameters_var);
	
	}
}
//END
