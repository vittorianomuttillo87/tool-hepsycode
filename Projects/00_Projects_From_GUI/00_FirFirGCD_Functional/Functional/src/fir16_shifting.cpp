#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::fir16_shifting_main()
{
	// datatype for channels

	fir16s_parameters fir16s_parameters_var;

	fir16s_results fir16s_results_var;

//implementation
	while(1){
		
// Read from Fir16_main
		fir16s_parameters_var = fir16s_parameters_channel->read();
	
// Write to Fir16_main
		fir16s_results_channel->write(fir16s_results_var);
	
	}
}
//END
