#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir16_main_main()
{
	// datatype for channels

	Stim2_Fir16_main_parameters Stim2_Fir16_main_parameters_var;

	fir16e_parameters fir16e_parameters_var;

	fir16e_results fir16e_results_var;

	fir16s_results fir16s_results_var;

	fir16s_parameters fir16s_parameters_var;

	fir16_results fir16_results_var;

//implementation
	while(1){
		
// Read from Stimulus
		Stim2_Fir16_main_parameters_var = stim2_channel_port->read();
	
// Write to fir16_evaluation
		fir16e_parameters_channel->write(fir16e_parameters_var);
	
// Read from fir16_evaluation
		fir16e_results_var = fir16e_results_channel->read();
	
// Read from fir16_shifting
		fir16s_results_var = fir16s_results_channel->read();
	
// Write to fir16_shifting
		fir16s_parameters_channel->write(fir16s_parameters_var);
	
// Write to gcd_main
		result16_channel->write(fir16_results_var);
	
	}
}
//END
