#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::fir8_shifting_main()
{
	// datatype for channels

	fir8s_parameters fir8s_parameters_var;

	fir8s_results fir8s_results_var;

//implementation
	while(1){
		
// Read from Fir8_main
		fir8s_parameters_var = fir8s_parameters_channel->read();
	
// Write to Fir8_main
		fir8s_results_channel->write(fir8s_results_var);
	
	}
}
//END
