#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::fir8_evaluation_main()
{
	// datatype for channels

	fir8e_parameters fir8e_parameters_var;

	fir8e_results fir8e_results_var;

//implementation
	while(1){
		
// Read from Fir8_main
		fir8e_parameters_var = fir8e_parameters_channel->read();
	
// Write to Fir8_main
		fir8e_results_channel->write(fir8e_results_var);
	
	}
}
//END
