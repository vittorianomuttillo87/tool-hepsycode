#ifndef __MYSYSTEM__
#define __MYSYSTEM__

#include "sc_csp_channel_ifs.h"
#include "sc_csp_channel.h"
#include "datatype.h"

SC_MODULE(mainsystem)
{
	
// Ports for testbench connections
sc_port< sc_csp_channel_in_if< Stim1_Fir8_main_parameters > > stim1_channel_port;
	
// Ports for testbench connections
sc_port< sc_csp_channel_in_if< Stim2_Fir16_main_parameters > > stim2_channel_port;
	
// Ports for testbench connections
sc_port< sc_csp_channel_out_if< gcd_main_display_parameters > > result_channel_port;


// Process Fir8_main
void Fir8_main_main();

// Process fir8_evaluation
void fir8_evaluation_main();

// Process fir8_shifting
void fir8_shifting_main();

// Process Fir16_main
void Fir16_main_main();

// Process fir16_evaluation
void fir16_evaluation_main();

// Process fir16_shifting
void fir16_shifting_main();

// Process gcd_main
void gcd_main_main();

// Process gcd_evaluation
void gcd_evaluation_main();

// channel fir8e_results_channel
sc_csp_channel< fir8e_results > *fir8e_results_channel;

// channel fir8s_parameters_channel
sc_csp_channel< fir8s_parameters > *fir8s_parameters_channel;

// channel fir8s_results_channel
sc_csp_channel< fir8s_results > *fir8s_results_channel;

// channel result8_channel
sc_csp_channel< fir8_results > *result8_channel;

// channel result16_channel
sc_csp_channel< fir16_results > *result16_channel;

// channel gcde_parameters_channel
sc_csp_channel< gcde_parameters > *gcde_parameters_channel;

// channel gcde_results_channel
sc_csp_channel< gcde_results > *gcde_results_channel;

// channel fir16e_parameters_channel
sc_csp_channel< fir16e_parameters > *fir16e_parameters_channel;

// channel fir16e_results_channel
sc_csp_channel< fir16e_results > *fir16e_results_channel;

// channel fir16s_parameters_channel
sc_csp_channel< fir16s_parameters > *fir16s_parameters_channel;

// channel fir16s_results_channel
sc_csp_channel< fir16s_results > *fir16s_results_channel;

// channel fir8e_parameters_channel
sc_csp_channel< fir8e_parameters > *fir8e_parameters_channel;

//mainsystem impl.
SC_CTOR(mainsystem)
{
fir8e_results_channel = new sc_csp_channel< fir8e_results >;

fir8s_parameters_channel = new sc_csp_channel< fir8s_parameters >;

fir8s_results_channel = new sc_csp_channel< fir8s_results >;

result8_channel = new sc_csp_channel< fir8_results >;

result16_channel = new sc_csp_channel< fir16_results >;

gcde_parameters_channel = new sc_csp_channel< gcde_parameters >;

gcde_results_channel = new sc_csp_channel< gcde_results >;

fir16e_parameters_channel = new sc_csp_channel< fir16e_parameters >;

fir16e_results_channel = new sc_csp_channel< fir16e_results >;

fir16s_parameters_channel = new sc_csp_channel< fir16s_parameters >;

fir16s_results_channel = new sc_csp_channel< fir16s_results >;

fir8e_parameters_channel = new sc_csp_channel< fir8e_parameters >;

SC_THREAD(Fir8_main_main);
SC_THREAD(fir8_evaluation_main);
SC_THREAD(fir8_shifting_main);
SC_THREAD(Fir16_main_main);
SC_THREAD(fir16_evaluation_main);
SC_THREAD(fir16_shifting_main);
SC_THREAD(gcd_main_main);
SC_THREAD(gcd_evaluation_main);

	}
};	
#endif
