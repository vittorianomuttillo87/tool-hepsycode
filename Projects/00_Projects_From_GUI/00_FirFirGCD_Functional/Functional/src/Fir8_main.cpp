#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir8_main_main()
{
	// datatype for channels

	Stim1_Fir8_main_parameters Stim1_Fir8_main_parameters_var;

	fir8e_parameters fir8e_parameters_var;

	fir8e_results fir8e_results_var;

	fir8s_parameters fir8s_parameters_var;

	fir8s_results fir8s_results_var;

	fir8_results fir8_results_var;

//implementation
	while(1){
		
// Read from Stimulus
		Stim1_Fir8_main_parameters_var = stim1_channel_port->read();
	
// Write to fir8_evaluation
		fir8e_parameters_channel->write(fir8e_parameters_var);
	
// Read from fir8_evaluation
		fir8e_results_var = fir8e_results_channel->read();
	
// Write to fir8_shifting
		fir8s_parameters_channel->write(fir8s_parameters_var);
	
// Read from fir8_shifting
		fir8s_results_var = fir8s_results_channel->read();
	
// Write to gcd_main
		result8_channel->write(fir8_results_var);
	
	}
}
//END
