#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::fir16_evaluation_main()
{
	// datatype for channels

	fir16e_parameters fir16e_parameters_var;

	fir16e_results fir16e_results_var;

//implementation
	while(1){
		
// Read from Fir16_main
		fir16e_parameters_var = fir16e_parameters_channel->read();
	
// Write to Fir16_main
		fir16e_results_channel->write(fir16e_results_var);
	
	}
}
//END
