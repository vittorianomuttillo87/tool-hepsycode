#include <systemc.h>
#include "display.h"

void display::main()
{
	int i = 1;

	gcd_main_display_parameters gcd_main_display_parameters_var;

//implementation
	while(i <= 10)
	{

		gcd_main_display_parameters_var = result_channel_port->read();
		cout << "Display-" << i <<": \t" << gcd_main_display_parameters_var.example << " " << "\t at time \t" << sc_time_stamp().to_seconds() << endl;										
		i++;
	}
	sc_stop();
}
