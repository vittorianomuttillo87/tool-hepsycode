#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir16s_main()
{
	// datatype for channels

	Fir16m_2_fir16s_param Fir16m_2_fir16s_param_var;

	Fir16s_Fir16m_3_param Fir16s_Fir16m_3_param_var;

//implementation
	while(1){
		
// Read from Fir16m_2
		Fir16m_2_fir16s_param_var = Fir16m_2_fir16s_channel->read();
	
// Write to Fir16m_3
		Fir16s_Fir16m_3_channel->write(Fir16s_Fir16m_3_param_var);
	
	}
}
//END
