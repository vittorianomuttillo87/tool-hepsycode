#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::gcdm_2_main()
{
	// datatype for channels

	gcde_gcdm_2_param gcde_gcdm_2_param_var;

	gcdm_2_display_param gcdm_2_display_param_var;

//implementation
	while(1){
		
// Read from gcde
		gcde_gcdm_2_param_var = gcde_gcdm_2_channel->read();
	
// Write to gcdm_2
		result_channel_port->write(gcdm_2_display_param_var);
	
	}
}
//END
