#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir8e_main()
{
	// datatype for channels

	Fir8m_1_Fir8e_param Fir8m_1_Fir8e_param_var;

	Fir8e_Fir8m_2_param Fir8e_Fir8m_2_param_var;

//implementation
	while(1){
		
// Read from Fir8m_1
		Fir8m_1_Fir8e_param_var = Fir8m_1_Fir8e_channel->read();
	
// Write to Fir8m_2
		Fir8e_Fir8m_2_channel->write(Fir8e_Fir8m_2_param_var);
	
	}
}
//END
