#include <systemc.h>
#include "sc_csp_channel_ifs.h"
#include "sc_csp_channel.h"
#include "datatype.h"
#include "stim_gen.h"
#include "mainsystem.h"
#include "display.h"
#include <string.h>

/////////////////////////////////////////////////////////////////////////////////////////
// Testbench
/////////////////////////////////////////////////////////////////////////////////////////

int sc_main (int, char *[])
{

// Channels for the connection to the main system

sc_csp_channel< Stim1_Fir8m_1_param >   Stim1_Fir8m_1_channel;		
sc_csp_channel< Stim2_Fir16m_1_param >   Stim2_Fir16m_1_channel;		
sc_csp_channel< gcdm_2_display_param >   gcdm_2_display_channel;		

// Instantiation and connection of testbench and system

stim_gen mystimgen("mystimgen");
mainsystem mysystem("mysystem");
display mydisplay("mydisplay");

mystimgen.stim1_channel_port(Stim1_Fir8m_1_channel); 

mysystem.stim1_channel_port(Stim1_Fir8m_1_channel);
mystimgen.stim2_channel_port(Stim2_Fir16m_1_channel); 

mysystem.stim2_channel_port(Stim2_Fir16m_1_channel);
mysystem.result_channel_port(gcdm_2_display_channel);

mydisplay.result_channel_port(gcdm_2_display_channel); 

sc_start();
system("pause");
return 0;
}
