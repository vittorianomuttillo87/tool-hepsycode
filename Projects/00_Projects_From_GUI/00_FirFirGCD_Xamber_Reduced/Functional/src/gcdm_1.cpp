#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::gcdm_1_main()
{
	// datatype for channels

	Fir8m_3_gcdm_1_param Fir8m_3_gcdm_1_param_var;

	gcdm_1_gcde_param gcdm_1_gcde_param_var;

	Fir16m_3_gcdm_1_param Fir16m_3_gcdm_1_param_var;

//implementation
	while(1){
		
// Read from Fir8m_3
		Fir8m_3_gcdm_1_param_var = Fir8m_3_gcdm_1_channel->read();
	
// Write to gcde
		gcdm_1_gcde_channel->write(gcdm_1_gcde_param_var);
	
// Read from Fir16m_3
		Fir16m_3_gcdm_1_param_var = Fir16m_3_gcdm_1_channel->read();
	
	}
}
//END
