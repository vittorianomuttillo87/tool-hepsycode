#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir16m_1_main()
{
	// datatype for channels

	Stim2_Fir16m_1_param Stim2_Fir16m_1_param_var;

	Fir16m_1_Fir16e_param Fir16m_1_Fir16e_param_var;

	Fir16m_1_Fir16m_2_param Fir16m_1_Fir16m_2_param_var;

//implementation
	while(1){
		
// Read from Stimulus
		Stim2_Fir16m_1_param_var = stim2_channel_port->read();
	
// Write to Fir16e
		Fir16m_1_Fir16e_channel->write(Fir16m_1_Fir16e_param_var);
	
// Write to Fir16m_2
		Fir16m_1_Fir16m_2_channel->write(Fir16m_1_Fir16m_2_param_var);
	
	}
}
//END
