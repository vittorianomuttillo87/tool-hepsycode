#ifndef __DATATYPE__
#define __DATATYPE__

#include <systemc.h>

struct Stim1_Fir8m_1_param
{
	sc_uint<8> example;
};

struct Fir8m_1_Fir8e_param
{
	sc_uint<8> example;
};

struct Fir8e_Fir8m_2_param
{
	sc_uint<8> example;
};

struct Fir8m_2_Fir8s_
{
	sc_uint<8> example;
};

struct Fir8s_Fir8m_3_param
{
	sc_uint<8> example;
};

struct Fir8m_1_fir8m_2_param
{
	sc_uint<8> example;
};

struct Fir8m_2_Fir8m_3_param
{
	sc_uint<8> example;
};

struct Fir16m_1_Fir16m_2_param
{
	sc_uint<8> example;
};

struct fir16m_2_Fir16m_3_param
{
	sc_uint<8> example;
};

struct Fir16m_1_Fir16e_param
{
	sc_uint<8> example;
};

struct Fir16e_Fir16m_2_param
{
	sc_uint<8> example;
};

struct Fir16m_2_fir16s_param
{
	sc_uint<8> example;
};

struct Fir16s_Fir16m_3_param
{
	sc_uint<8> example;
};

struct Stim2_Fir16m_1_param
{
	sc_uint<8> example;
};

struct Fir8m_3_gcdm_1_param
{
	sc_uint<8> example;
};

struct Fir16m_3_gcdm_1_param
{
	sc_uint<8> example;
};

struct gcdm_1_gcde_param
{
	sc_uint<8> example;
};

struct gcde_gcdm_2_param
{
	sc_uint<8> example;
};

struct gcdm_2_display_param
{
	sc_uint<8> example;
};

#endif
