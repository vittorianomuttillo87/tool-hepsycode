#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::gcde_main()
{
	// datatype for channels

	gcdm_1_gcde_param gcdm_1_gcde_param_var;

	gcde_gcdm_2_param gcde_gcdm_2_param_var;

//implementation
	while(1){
		
// Read from gcdm_1
		gcdm_1_gcde_param_var = gcdm_1_gcde_channel->read();
	
// Write to gcdm_2
		gcde_gcdm_2_channel->write(gcde_gcdm_2_param_var);
	
	}
}
//END
