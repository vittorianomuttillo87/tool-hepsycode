#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir8s_main()
{
	// datatype for channels

	Fir8m_2_Fir8s_ Fir8m_2_Fir8s__var;

	Fir8s_Fir8m_3_param Fir8s_Fir8m_3_param_var;

//implementation
	while(1){
		
// Read from Fir8m_2
		Fir8m_2_Fir8s__var = Fir8m_2_Fir8s_channel->read();
	
// Write to Fir8m_3
		Fir8s_Fir8m_3_channel->write(Fir8s_Fir8m_3_param_var);
	
	}
}
//END
