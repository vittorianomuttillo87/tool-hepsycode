#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir8m_2_main()
{
	// datatype for channels

	Fir8e_Fir8m_2_param Fir8e_Fir8m_2_param_var;

	Fir8m_2_Fir8s_ Fir8m_2_Fir8s__var;

	Fir8m_1_fir8m_2_param Fir8m_1_fir8m_2_param_var;

	Fir8m_2_Fir8m_3_param Fir8m_2_Fir8m_3_param_var;

//implementation
	while(1){
		
// Read from Fir8e
		Fir8e_Fir8m_2_param_var = Fir8e_Fir8m_2_channel->read();
	
// Write to Fir8s
		Fir8m_2_Fir8s_channel->write(Fir8m_2_Fir8s__var);
	
// Read from Fir8m_1
		Fir8m_1_fir8m_2_param_var = Fir8m_1_fir8m_2_channel->read();
	
// Write to Fir8m_3
		Fir8m_2_Fir8m_3_channel->write(Fir8m_2_Fir8m_3_param_var);
	
	}
}
//END
