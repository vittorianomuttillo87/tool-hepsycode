#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir16e_main()
{
	// datatype for channels

	Fir16m_1_Fir16e_param Fir16m_1_Fir16e_param_var;

	Fir16e_Fir16m_2_param Fir16e_Fir16m_2_param_var;

//implementation
	while(1){
		
// Read from Fir16m_1
		Fir16m_1_Fir16e_param_var = Fir16m_1_Fir16e_channel->read();
	
// Write to Fir16m_2
		Fir16e_Fir16m_2_channel->write(Fir16e_Fir16m_2_param_var);
	
	}
}
//END
