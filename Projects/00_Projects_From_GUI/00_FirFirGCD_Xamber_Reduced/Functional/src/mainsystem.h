#ifndef __MYSYSTEM__
#define __MYSYSTEM__

#include "sc_csp_channel_ifs.h"
#include "sc_csp_channel.h"
#include "datatype.h"

SC_MODULE(mainsystem)
{
	
// Ports for testbench connections
sc_port< sc_csp_channel_in_if< Stim1_Fir8m_1_param > > stim1_channel_port;
	
// Ports for testbench connections
sc_port< sc_csp_channel_in_if< Stim2_Fir16m_1_param > > stim2_channel_port;
	
// Ports for testbench connections
sc_port< sc_csp_channel_out_if< gcdm_2_display_param > > result_channel_port;


// Process Fir8m_1
void Fir8m_1_main();

// Process Fir8e
void Fir8e_main();

// Process Fir8m_2
void Fir8m_2_main();

// Process Fir8s
void Fir8s_main();

// Process Fir8m_3
void Fir8m_3_main();

// Process Fir16m_3
void Fir16m_3_main();

// Process Fir16m_1
void Fir16m_1_main();

// Process Fir16m_2
void Fir16m_2_main();

// Process Fir16e
void Fir16e_main();

// Process Fir16s
void Fir16s_main();

// Process gcdm_1
void gcdm_1_main();

// Process gcde
void gcde_main();

// Process gcdm_2
void gcdm_2_main();

// channel Fir8m_1_Fir8e_channel
sc_csp_channel< Fir8m_1_Fir8e_param > *Fir8m_1_Fir8e_channel;

// channel Fir8e_Fir8m_2_channel
sc_csp_channel< Fir8e_Fir8m_2_param > *Fir8e_Fir8m_2_channel;

// channel Fir8m_2_Fir8s_channel
sc_csp_channel< Fir8m_2_Fir8s_ > *Fir8m_2_Fir8s_channel;

// channel Fir8s_Fir8m_3_channel
sc_csp_channel< Fir8s_Fir8m_3_param > *Fir8s_Fir8m_3_channel;

// channel Fir8m_1_fir8m_2_channel
sc_csp_channel< Fir8m_1_fir8m_2_param > *Fir8m_1_fir8m_2_channel;

// channel Fir8m_2_Fir8m_3_channel
sc_csp_channel< Fir8m_2_Fir8m_3_param > *Fir8m_2_Fir8m_3_channel;

// channel Fir16m_1_Fir16m_2_channel
sc_csp_channel< Fir16m_1_Fir16m_2_param > *Fir16m_1_Fir16m_2_channel;

// channel fir16m_2_Fir16m_3_channel
sc_csp_channel< fir16m_2_Fir16m_3_param > *fir16m_2_Fir16m_3_channel;

// channel Fir16m_1_Fir16e_channel
sc_csp_channel< Fir16m_1_Fir16e_param > *Fir16m_1_Fir16e_channel;

// channel Fir16e_Fir16m_2_channel
sc_csp_channel< Fir16e_Fir16m_2_param > *Fir16e_Fir16m_2_channel;

// channel Fir16m_2_fir16s_channel
sc_csp_channel< Fir16m_2_fir16s_param > *Fir16m_2_fir16s_channel;

// channel Fir16s_Fir16m_3_channel
sc_csp_channel< Fir16s_Fir16m_3_param > *Fir16s_Fir16m_3_channel;

// channel Fir8m_3_gcdm_1_channel
sc_csp_channel< Fir8m_3_gcdm_1_param > *Fir8m_3_gcdm_1_channel;

// channel Fir16m_3_gcdm_1_channel
sc_csp_channel< Fir16m_3_gcdm_1_param > *Fir16m_3_gcdm_1_channel;

// channel gcdm_1_gcde_channel
sc_csp_channel< gcdm_1_gcde_param > *gcdm_1_gcde_channel;

// channel gcde_gcdm_2_channel
sc_csp_channel< gcde_gcdm_2_param > *gcde_gcdm_2_channel;

//mainsystem impl.
SC_CTOR(mainsystem)
{
Fir8m_1_Fir8e_channel = new sc_csp_channel< Fir8m_1_Fir8e_param >;

Fir8e_Fir8m_2_channel = new sc_csp_channel< Fir8e_Fir8m_2_param >;

Fir8m_2_Fir8s_channel = new sc_csp_channel< Fir8m_2_Fir8s_ >;

Fir8s_Fir8m_3_channel = new sc_csp_channel< Fir8s_Fir8m_3_param >;

Fir8m_1_fir8m_2_channel = new sc_csp_channel< Fir8m_1_fir8m_2_param >;

Fir8m_2_Fir8m_3_channel = new sc_csp_channel< Fir8m_2_Fir8m_3_param >;

Fir16m_1_Fir16m_2_channel = new sc_csp_channel< Fir16m_1_Fir16m_2_param >;

fir16m_2_Fir16m_3_channel = new sc_csp_channel< fir16m_2_Fir16m_3_param >;

Fir16m_1_Fir16e_channel = new sc_csp_channel< Fir16m_1_Fir16e_param >;

Fir16e_Fir16m_2_channel = new sc_csp_channel< Fir16e_Fir16m_2_param >;

Fir16m_2_fir16s_channel = new sc_csp_channel< Fir16m_2_fir16s_param >;

Fir16s_Fir16m_3_channel = new sc_csp_channel< Fir16s_Fir16m_3_param >;

Fir8m_3_gcdm_1_channel = new sc_csp_channel< Fir8m_3_gcdm_1_param >;

Fir16m_3_gcdm_1_channel = new sc_csp_channel< Fir16m_3_gcdm_1_param >;

gcdm_1_gcde_channel = new sc_csp_channel< gcdm_1_gcde_param >;

gcde_gcdm_2_channel = new sc_csp_channel< gcde_gcdm_2_param >;

SC_THREAD(Fir8m_1_main);
SC_THREAD(Fir8e_main);
SC_THREAD(Fir8m_2_main);
SC_THREAD(Fir8s_main);
SC_THREAD(Fir8m_3_main);
SC_THREAD(Fir16m_3_main);
SC_THREAD(Fir16m_1_main);
SC_THREAD(Fir16m_2_main);
SC_THREAD(Fir16e_main);
SC_THREAD(Fir16s_main);
SC_THREAD(gcdm_1_main);
SC_THREAD(gcde_main);
SC_THREAD(gcdm_2_main);

	}
};	
#endif
