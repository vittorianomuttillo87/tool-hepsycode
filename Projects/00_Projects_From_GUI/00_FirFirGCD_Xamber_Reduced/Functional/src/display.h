#ifndef __DISPLAY__
#define __DISPLAY__

#include <systemc.h>
#include "sc_csp_channel_ifs.h"
#include "sc_csp_channel.h"
#include "datatype.h"


SC_MODULE(display)
{

    sc_port < sc_csp_channel_in_if < gcdm_2_display_param > > result_channel_port;
    
	SC_CTOR(display)
    {
      SC_THREAD(main);
    }

    void main();
};

#endif

