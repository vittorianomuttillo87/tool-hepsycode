#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir8m_3_main()
{
	// datatype for channels

	Fir8s_Fir8m_3_param Fir8s_Fir8m_3_param_var;

	Fir8m_3_gcdm_1_param Fir8m_3_gcdm_1_param_var;

	Fir8m_2_Fir8m_3_param Fir8m_2_Fir8m_3_param_var;

//implementation
	while(1){
		
// Read from Fir8s
		Fir8s_Fir8m_3_param_var = Fir8s_Fir8m_3_channel->read();
	
// Write to gcdm_1
		Fir8m_3_gcdm_1_channel->write(Fir8m_3_gcdm_1_param_var);
	
// Read from Fir8m_2
		Fir8m_2_Fir8m_3_param_var = Fir8m_2_Fir8m_3_channel->read();
	
	}
}
//END
