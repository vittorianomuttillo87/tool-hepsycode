#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::Fir16m_3_main()
{
	// datatype for channels

	fir16m_2_Fir16m_3_param fir16m_2_Fir16m_3_param_var;

	Fir16m_3_gcdm_1_param Fir16m_3_gcdm_1_param_var;

	Fir16s_Fir16m_3_param Fir16s_Fir16m_3_param_var;

//implementation
	while(1){
		
// Read from Fir16m_2
		fir16m_2_Fir16m_3_param_var = fir16m_2_Fir16m_3_channel->read();
	
// Write to gcdm_1
		Fir16m_3_gcdm_1_channel->write(Fir16m_3_gcdm_1_param_var);
	
// Read from Fir16s
		Fir16s_Fir16m_3_param_var = Fir16s_Fir16m_3_channel->read();
	
	}
}
//END
