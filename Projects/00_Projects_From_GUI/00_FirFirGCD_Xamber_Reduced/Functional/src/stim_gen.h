#ifndef __STIMGEN__
#define __STIMGEN__

#include <systemc.h>
#include "sc_csp_channel_ifs.h"
#include "sc_csp_channel.h"
#include "datatype.h"

SC_MODULE(stim_gen)
{

    sc_port < sc_csp_channel_out_if < Stim1_Fir8m_1_param > > stim1_channel_port;
    

    sc_port < sc_csp_channel_out_if < Stim2_Fir16m_1_param > > stim2_channel_port;
    
	SC_CTOR(stim_gen)
	{
		SC_THREAD(main);
	}  

	void main();
};

#endif
