#!/bin/bash
hepsycode=$(dirname $BASH_SOURCE)/..
hepsycode=$(cd $hepsycode && pwd)

export SYSTEMCPATHLIB=/usr/local/systemc-2.3.0a/lib-linux64
export SYSTEMCPATHINCLUDE=/usr/local/systemc-2.3.0a/include
