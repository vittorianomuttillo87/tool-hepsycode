#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::A_main()
{
	// datatype for channels

	B_A_parameters B_A_parameters_var;

	A_B_parameters A_B_parameters_var;

	display_parameters display_parameters_var;

	C_A_parameters C_A_parameters_var;

	A_C_parameters A_C_parameters_var;

//implementation
	while(1){
		
// Read from B
		B_A_parameters_var = B_A->read();
	
// Write to B
		A_B->write(A_B_parameters_var);
	
// Read from C
		C_A_parameters_var = C_A->read();
	
// Write to C
		A_C->write(A_C_parameters_var);
	
// Write to A
		result_channel_port->write(display_parameters_var);
	
	}
}
//END
