#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::C_main()
{
	// datatype for channels

	B_C_parameters B_C_parameters_var;

	C_A_parameters C_A_parameters_var;

	C_B_parameters C_B_parameters_var;

	A_C_parameters A_C_parameters_var;

//implementation
	while(1){
		
// Read from B
		B_C_parameters_var = B_C->read();
	
// Write to A
		C_A->write(C_A_parameters_var);

// Read from A
		A_C_parameters_var = A_C->read();
	
// Write to B
		C_B->write(C_B_parameters_var);
	
	}
}
//END
