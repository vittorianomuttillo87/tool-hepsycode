#ifndef __MYSYSTEM__
#define __MYSYSTEM__

#include "sc_csp_channel_ifs.h"
#include "sc_csp_channel.h"
#include "datatype.h"

SC_MODULE(mainsystem)
{
	
// Ports for testbench connections
sc_port< sc_csp_channel_in_if< stim_parameter > > stim_channel_port;
	
// Ports for testbench connections
sc_port< sc_csp_channel_out_if< display_parameters > > result_channel_port;


// Process A
void A_main();

// Process B
void B_main();

// Process C
void C_main();

// channel B_A
sc_csp_channel< B_A_parameters > *B_A;

// channel A_B
sc_csp_channel< A_B_parameters > *A_B;

// channel B_C
sc_csp_channel< B_C_parameters > *B_C;

// channel C_B
sc_csp_channel< C_B_parameters > *C_B;

// channel C_A
sc_csp_channel< C_A_parameters > *C_A;

// channel A_C
sc_csp_channel< A_C_parameters > *A_C;

//mainsystem impl.
SC_CTOR(mainsystem)
{
B_A = new sc_csp_channel< B_A_parameters >;

A_B = new sc_csp_channel< A_B_parameters >;

B_C = new sc_csp_channel< B_C_parameters >;

C_B = new sc_csp_channel< C_B_parameters >;

C_A = new sc_csp_channel< C_A_parameters >;

A_C = new sc_csp_channel< A_C_parameters >;

SC_THREAD(A_main);
SC_THREAD(B_main);
SC_THREAD(C_main);

	}
};	
#endif
