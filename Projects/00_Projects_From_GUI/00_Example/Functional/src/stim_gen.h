#ifndef __STIMGEN__
#define __STIMGEN__

#include <systemc.h>
#include "sc_csp_channel_ifs.h"
#include "sc_csp_channel.h"
#include "datatype.h"

SC_MODULE(stim_gen)
{

    sc_port < sc_csp_channel_out_if < stim_parameter > > stim_channel_port;
    
	SC_CTOR(stim_gen)
	{
		SC_THREAD(main);
	}  

	void main();
};

#endif
