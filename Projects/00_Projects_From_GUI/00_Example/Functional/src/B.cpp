#include <systemc.h>
#include "mainsystem.h"
#include <math.h>

void mainsystem::B_main()
{
	// datatype for channels

	A_B_parameters A_B_parameters_var;

	B_A_parameters B_A_parameters_var;

	stim_parameter stim_parameter_var;

	B_C_parameters B_C_parameters_var;

	C_B_parameters C_B_parameters_var;

//implementation
	while(1){
		
// Read from Stimulus
		stim_parameter_var = stim_channel_port->read();
	
// Write to A
		B_A->write(B_A_parameters_var);

// Read from A
		A_B_parameters_var = A_B->read();
	
// Write to C
		B_C->write(B_C_parameters_var);
	
// Read from C
		C_B_parameters_var = C_B->read();
	
	}
}
//END
