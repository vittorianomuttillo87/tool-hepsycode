#include <systemc.h>
#include "sc_csp_channel_ifs.h"
#include "sc_csp_channel.h"
#include "datatype.h"
#include "stim_gen.h"
#include "mainsystem.h"
#include "display.h"
#include <string.h>

/////////////////////////////////////////////////////////////////////////////////////////
// Testbench
/////////////////////////////////////////////////////////////////////////////////////////

int sc_main (int, char *[])
{

// Channels for the connection to the main system

sc_csp_channel< stim_parameter >   S_B;		
sc_csp_channel< display_parameters >   A_D;		

// Instantiation and connection of testbench and system

stim_gen mystimgen("mystimgen");
mainsystem mysystem("mysystem");
display mydisplay("mydisplay");

mystimgen.stim_channel_port(S_B); 

mysystem.stim_channel_port(S_B);
mysystem.result_channel_port(A_D);

mydisplay.result_channel_port(A_D); 

sc_start();
system("pause");
return 0;
}
