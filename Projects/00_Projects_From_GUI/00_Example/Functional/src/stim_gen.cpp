#include <systemc.h>
#include "stim_gen.h"

void stim_gen::main()
{
	// init CODE
	unsigned int i=1;

	stim_parameter stim_parameter_var;
	stim_parameter_var.example = 1;

//implementation
	while(1){
		
// content		
		wait(1, SC_MS);
		stim_channel_port->write(stim_parameter_var);
		cout << "Stimulus-"<<i<<": \t" << stim_parameter_var.example << "\t at time \t" << sc_time_stamp() << endl;	
											
// Check for stop
		if(i >= 10) return;
		else i++;
	}
}
//END
