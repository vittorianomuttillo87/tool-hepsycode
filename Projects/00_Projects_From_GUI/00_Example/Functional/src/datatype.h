#ifndef __DATATYPE__
#define __DATATYPE__

#include <systemc.h>

struct stim_parameter
{
	sc_uint<8> example;
};

struct B_A_parameters
{
	sc_uint<8> example;
};

struct A_B_parameters
{
	sc_uint<8> example;
};

struct display_parameters
{
	short example;
};

struct B_C_parameters
{
	sc_uint<8> example;
};

struct C_B_parameters
{
	sc_uint<8> example;
};

struct C_A_parameters
{
	sc_uint<8> example;
};

struct A_C_parameters
{
	sc_uint<8> example;
};

#endif
